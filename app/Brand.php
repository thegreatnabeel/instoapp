<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
	use SoftDeletes;
    //
    public function Product()
    {
        return $this->hasMany(Product::class);
    }

    public function getBrandImgPathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }
}
