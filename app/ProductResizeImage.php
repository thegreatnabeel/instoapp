<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductResizeImage extends Model
{
    //

    public function ProductImage()
    {
    	return $this->belongsTo(ProductImage::class,'product_image_id','id');
    }

    public function getProductResizeImagePathAttribute($path)
    {
        if($path){
        return asset(str_replace('public/', 'storage/', $path));
        }
        else{
            return null;
        }
    }

}
