<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorSubArea extends Model
{
    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id','id')->withTrashed();
    }

    public function SubArea()
    {
        return $this->belongsTo(SubArea::class,'sub_area_id','id');
    }
}
