<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorOffereCount extends Model
{
    //
    public function giveofferemaster()
    {
    	return $this->belongsTo(GiveOffereMaster::class,'give_master_id','id');
    }
}
