<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function Customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function Product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function getProduct()
    {
        return $this->Product()->with(['ProductImage'])->withTrashed();
    }

    public function OffereDetail()
    {
        return $this->belongsTo(OffereDetail::class,'offere_id');
    }
}
