<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiveOffereDetail extends Model
{
    //
    public function GiveoffereMaster()
    {
    	return $this->belongsTo(GiveOffereMaster::class,'give_offere_master_id','id');
    }

    public function offereDetail()
    {
    	return $this->belongsTo(OffereDetail::class,'offere_id','id');
    }

    public function getofferedetail()
    {
        $currentdate = date('Y-m-d');
        return $this->OffereDetail()->where('offere_valid_date','>=',$currentdate);
    }
}
