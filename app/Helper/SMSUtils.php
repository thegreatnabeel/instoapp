<?php
namespace App\Helper;
use SoapClient;

abstract class SMSUtils
{
	public static function sendNotification($to, $body)
	{
		if(empty($to))
			return false;

	    $payload = array(
	        'to' 	=> $to,
	        'sound' => 'default',
	        'body' 	=> $body['data']['message'] ?? "Notification Recevied",
            'data'  => $body,
            'channelId' => 'message'
	    );

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL 			=> "https://exp.host/--/api/v2/push/send",
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_ENCODING 		=> "",
			CURLOPT_MAXREDIRS 		=> 10,
			CURLOPT_TIMEOUT 		=> 30,
			CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST 	=> "POST",
			CURLOPT_POSTFIELDS 		=> json_encode($payload),
			CURLOPT_HTTPHEADER 		=> array(
				"Accept: application/json",
				"Accept-Encoding: gzip, deflate",
				"Content-Type: application/json",
				"cache-control: no-cache",
				"host: exp.host"
			),
		));

		$response 	= curl_exec($curl);
		$err 		= curl_error($curl);

		curl_close($curl);

		if ($err)
		{
			return false;
		}
		return $response;
	}

	public static function tripByClientTrip($client_trip_id)
	{
		return \App\GroupClientTrip::find($client_trip_id)->clienttrip->trip;
	}

	public static function clientByClientTrip($client_trip_id)
	{
		return \App\GroupClientTrip::find($client_trip_id)->client;
	}

	public static function travelAgencyByClientTrip($client_trip_id)
	{
		return \App\GroupClientTrip::find($client_trip_id)->clienttrip->trip->travelAgency;
	}

	public  static function sendSMSNew($destinationNumber, $messageContent)

     {
    try {
    	

        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 0);

        $opts = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'ciphers' => 'RC4-SHA',
                'allow_self_signed' => true
            ),
            'http' => array(
                'user_agent' => 'PHPSoapClient'
            ),
            'https' => array(
                'user_agent' => 'PHPSoapClient'
            ),

        );

        $context = stream_context_create($opts);

        $wsdlUrl = 'http://cbs.zong.com.pk/reachcwsv2/corporatesms.svc?wsdl';

        $soapClientOptions = array(
            'trace' => 1,
            'exceptions' => 1,
            'stream_context' => $context,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'encoding' => 'UTF-8',
            'verify_peer' => false,
            'verifyhost' => false,

            /*   'location'       => $wsdlUrl,*/

        );

        $credentials = array(
            'loginId' => '923124742135',
            'loginPassword' => 'Zong@123'
        );
        libxml_disable_entity_loader(false); //adding this worked for me
        $client = new \SoapClient($wsdlUrl, $soapClientOptions);

        /*$client = new SoapClient(null, array('location' => $wsdlUrl,
            'uri'      => $wsdlUrl,
            'stream_context' => stream_context_create($opts)));*/



        $smsParameters = array(
            'obj_QuickSMS' => array(
                'loginId' => '923124742135',
                'loginPassword' => 'Zong@123',
                'Destination' => $destinationNumber,
                'Mask' => 'Instalment',
                'Message' => $messageContent,
                'UniCode' => '0',
                'ShortCodePrefered' => 'n',
            )
        );



        /*$summaryParameters = array(
            'obj_GetAccountSummary' => $credentials
        );*/


        /*   $result = $client->GetAccountSummary($summaryParameters);*/

        $result = $client->QuickSMS($smsParameters);
//        echo '<pre>';
//        print_r($result);
//        echo '</pre>';
 //       exit;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
}
