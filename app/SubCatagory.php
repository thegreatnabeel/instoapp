<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCatagory extends Model
{
	use SoftDeletes;
    public function Category()
    {
    	return $this->belongsTo(Catagory::class,'catagory_id','id');
    }

    public function CategoryId()
    {
    	return $this->Category()->select('id');
    }

    public function Product()
    {
        return $this->hasMany(Product::class);
    }

    public function KeyFeature()
    {
        return $this->hasMany(KeyFeature::class);
    }

    public function Vendor()
    {
        return $this->belongsToMany(Vendor::class, 'vendor_sub_catagories','sub_catagory_id','vendor_id');
    }

    public function getSubCatagoryImgPathAttribute($path)
    {
        if($path){
        return asset(str_replace('public/', 'storage/', $path));
        }
        else{
            return null;
        }
    }
}
