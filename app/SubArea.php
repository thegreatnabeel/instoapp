<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubArea extends Model
{
    //
    public function area()
    {
    	return $this->belongsTo(Area::class,'area_id','id');
    }

    public function AreaId()
    {
    	return $this->area()->select('id');
    }

    public function Vendor()
    {
        return $this->belongsToMany(SubArea::class, 'vendor_sub_areas','sub_area_id','vendor_id');
    }

    public function customer()
    {
        return $this->hasMany(Customer::class);
    }
}
