<?php

namespace App\Http\Controllers\API\ProductDetail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;
use App\SubCatagory;
use App\Product;
use App\Brand;
use App\KeyFeature;
use App\SubKeyFeature;
use DB;
use App\InfoGraphic;
use App\Ad;
use App\Cms;
class SubCatagoryController extends Controller
{

    public function category()
    {
        $category=Catagory::with('SubCatagory')->
                           whereHas('SubCatagory',function($q){
                           $q->havingRaw('count(sub_catagories.catagory_id) > 0');
                           })->get();

        $subcategory=SubCatagory::whereHas('Product',function($q){
                              $q->where('new_arrival',true);
                              })->get();
        return response()->json([
          'category'    => $category,
          'new_arrival' => $subcategory
           ]);
    }

    public function productdetail(Request $request)
    {
        $request->validate([
           'sub_category_id' => 'required|numeric',
        ]);
          $product=Product::with(['brand','ProductImage.ProductResizeImage'])->
                          where('sub_catagory_id',$request->sub_category_id)->
                          orderBy('new_arrival','desc')->jsonPaginate();
          return response()->json([
          'product'    => $product
           ]);
    }

    public function brandkeyfeaturedetail(Request $request)
    {
        $request->validate([
           'sub_category_id' => 'required|numeric',
        ]);
        $SubCatID=$request->sub_category_id;
        $brand= Brand::whereHas('Product',function($q) use($SubCatID){
                         $q->where('sub_catagory_id',$SubCatID);
                         })->get();

        $KeyFeatures= KeyFeature::whereHas('SubCatagory',function($q) use($SubCatID){
                         $q->where('sub_catagory_id',$SubCatID);
                         })->whereHas('SubKeyFeature',function($q){
                          $q->havingRaw('count(sub_key_features.key_feature_id) > 0');
                         })->get();
        $KeyFeatures->each(function($KeyFeature, $key) use($KeyFeatures) {
        $KeyFeatures[$key]->KeyFeatureValue        = SubKeyFeature::where(                                    'key_feature_id',$KeyFeature->id)
                                    ->distinct()->get(['sub_key_feature_value']);
      });
        return response()->json([
          'brand'       => $brand,
          'KeyFeature'  => $KeyFeatures
           ]);
    }

    public function sorting(Request $request)
    {
      if($request->type == 'popular'){
        
        $product=Product::with(['brand','ProductImage.ProductResizeImage'])
        ->where('sub_catagory_id',$request->sub_category_id)->orderBy('request_offere_number','desc')->jsonPaginate();
      }
      else{
        $product=Product::with(['brand','ProductImage.ProductResizeImage'])
        ->where('sub_catagory_id',$request->sub_category_id)->orderBy('new_arrival','desc')->orderBy('product_name','asc')->jsonPaginate();
      }
        return response()->json([
          'product'     => $product
           ]);
    }

    public function infographic()
    {
        $infographic=InfoGraphic::all();
        return response()->json([
          'infographic'    => $infographic
           ]);
    }

    public function ads()
    {
        $ads=Ad::all();
        return response()->json([
          'ads'    => $ads
           ]);
    }

    public function staticpage()
    {
        $cms=Cms::all();
        return response()->json([
          'cms'    => $cms
           ]);
    }

    public function adslink(Request $request)
    {
        $request->validate([
           'type' => 'required|string',
           'id'   => 'required|numeric'
        ]);
        if($request->type == "product"){
          $get_link_detail = Product::with(['brand','ProductImage.ProductResizeImage'])->withTrashed()->where('id',$request->id)->first();
        }
        else{
          $get_link_detail = SubCatagory::withTrashed()->where('id',$request->id)->first();
        }
        return response()->json([
          'get_link_detail'       => $get_link_detail,
           ]);
    }


}