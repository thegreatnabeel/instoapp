<?php

namespace App\Http\Controllers\API\ProductDetail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;
use App\SubCatagory;
use App\Product;
use DB;
class ProductSearchController extends Controller
{

    public function allproductsearch(Request $request)
    {
        $product=Product::with('brand')->with(['ProductImage' => function($q){
                                $q->with('ProductResizeImage');
                            }])->
                            where(DB::raw("CONCAT(`product_name`, ' ', `info`, ' ', `info1`)"), 'LIKE', "%" . $request->search . "%")->
                          orWhere('product_description','LIKE','%'.$request->search.'%')->jsonPaginate();
        return response()->json([
          'product'    => $product
           ]);
    }

    public function productsearch(Request $request)
    {
        $product=Product::with('brand')->with(['ProductImage' => function($q){
                                $q->with('ProductResizeImage');
                            }])->
                            where(DB::raw("CONCAT(`product_name`, ' ', `info`, ' ', `info1`)"), 'LIKE', "%" . $request->search . "%")->limit('5')->get();
        return response()->json([
          'product'    => $product
           ]);
    }

    public function productsearchbyfilter(Request $request)
    {
      $brand_id = json_decode($request->brand_id, true);
      $Sub_key_feature_id = json_decode($request->Key_feature_id, true);
      $Sub_key_feature_value = json_decode($request->Key_feature_value, true);

      if($brand_id==null){
        if($Sub_key_feature_id==null){
          $product  = Product::with('brand')->with(['ProductImage' => function($q){
                                $q->with('ProductResizeImage');
                            }])->
                          where('sub_catagory_id',$request->sub_category_id)->jsonPaginate();
          return response()->json([
          'product'    => $product
           ]);
        }
        else{
          $product  = Product::with('ProductImage')->
                             with(['ProductImage' =>  function($q){
                                $q->with('ProductResizeImage');
                            }])->
                          whereHas('SubKeyFeature',function($q) use($Sub_key_feature_id,$Sub_key_feature_value){
                            $q->whereIn('key_feature_id',$Sub_key_feature_id)->whereIn('sub_key_feature_value',$Sub_key_feature_value);
                          })->
                          where('sub_catagory_id',$request->sub_category_id)->jsonPaginate();
          return response()->json([
          'product'    => $product
           ]);
        }
        
      }
      else{
        if($Sub_key_feature_id==null){
          $product  = Product::with('brand')->with(['ProductImage' => function($q){
                                $q->with('ProductResizeImage');
                            }])->
                          where('sub_catagory_id',$request->sub_category_id)->
                          whereIn('brand_id',$brand_id)->
                          jsonPaginate();
          return response()->json([
          'product'    => $product
           ]);
        }
        else{
          $product  = Product::with('brand')->with(['ProductImage' => function($q){
                                $q->with('ProductResizeImage');
                            }])->
                          whereHas('SubKeyFeature',function($q) use($Sub_key_feature_id,$Sub_key_feature_value){
                            $q->whereIn('key_feature_id',$Sub_key_feature_id)->whereIn('sub_key_feature_value',$Sub_key_feature_value);
                          })->
                          whereIn('brand_id',$brand_id)->
                          where('sub_catagory_id',$request->sub_category_id)->jsonPaginate();
          return response()->json([
          'product'    => $product
           ]);
        }
      }
    }

}