<?php

namespace App\Http\Controllers\API\ProductDetail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\RequestOffere;
use App\WishList;
use App\OffereMaster;
use App\OffereDetail;
use App\Customer;
use App\GiveOffereMaster;
use App\GiveOffereDetail;
use App\VendorPackage;
use App\VendorSubArea;
use App\VendorOffereCount;
use App\Product;
use App\Order;
use App\Vendor;
use App\Notification;
use App\InstoNotification;
use Auth;
use DB;
class OffereController extends Controller
{
    public function customerrequestoffere(Request $request)
    {
      $request->validate([
        'product_id'         => 'required',
        ]);
        $status = false;
        $errMsg ='';
        //$noti =new Notification;
        //$token="c8tzC9IovGQ:APA91bFyis0wh1lQZRmGN1JpzVPh58hOtDddHDUfnOynqpXBm2kvcCvrd3wHLln9N-XvqFyAm4aglE_ZowzIBrbrOesaM1Iky45vEKeBzHw9SuYaiwwGwoeG0TVP4LuQXuUj-DoQqzm8";
        //$token=auth()->user()->where('id',3)->first();
        //$noti->toSingleDevice($token,'sdfds','fdsf',null,null);
        $CurrentDateTime = date('Y-m-d H:i:s');
        $CurrentDateTime = date('Y-m-d H:i:s', strtotime($CurrentDateTime. ' + 1 days'));
        $currentdate = date('Y-m-d');
        $status = false;
        $errMsg = '';
        $subareaid = Auth::user()->userType->sub_area_id;
        $give_offere_master_id = 0;
        $checkoffere = RequestOffere::where('customer_id',Auth::user()->userType->id)->where('product_id',$request->product_id)->first();
        if($checkoffere){
          $errMsg = 'Request Already Sent';
        }
        else{
        $requestoffere = new RequestOffere();
        $requestoffere->customer_id     = Auth::user()->userType->id;
        $requestoffere->product_id      = $request->product_id;
        $requestoffere->offere_datetime = $CurrentDateTime;
        $requestoffere->status          = "request_offere";
        $requestoffere->save();
        $get_product = Product::where('id',$request->product_id)->first();
        $get_vendor = Vendor::with('user')->whereHas('SubArea',function($q) use($subareaid){
                              $q->where('sub_area_id',$subareaid);})->get();
        foreach($get_vendor as $vendor){
          $get_package = VendorPackage::where('vendor_id',$vendor->id)->first();
          if($get_package){
          if( $get_package->end_date < $currentdate){
          }
          else{
            if($vendor->user->fcm_token!=null){
              $noti =new Notification;
              $noti->toSingleDevice($vendor->user->fcm_token,'Insto','Customer Request for '.$get_product->product_name.'',null,null);
            }

            $insto_notification = new InstoNotification();
            $insto_notification->vendor_id    = $vendor->id;
            $insto_notification->customer_id  = Auth::user()->userType->id;
            $insto_notification->product_id   = $get_product->id;
            $insto_notification->message_type = "vendor_received_request";
            $insto_notification->message      = 'Customer Request for '.$get_product->product_name.'';
            $insto_notification->status       = "new";
            $insto_notification->save();
          }
        }        
        }
        $get_product->request_offere_number = $get_product->request_offere_number == null ? 1 : $get_product->request_offere_number + 1;
        $get_product->save();
        if($requestoffere){

          $get_offere = OffereDetail::whereHas('OffereMaster.Product',function($q) use($requestoffere){
            $q->where('product_id',$requestoffere->product_id);})
            ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($subareaid){
              $q->where('sub_area_id',$subareaid);})->where('offere_valid_date','>=',$currentdate)->where('featured',true)->get();
          $get_offere->each(function($offere, $key) use($get_offere) {
                $get_offere[$key]->offeremaster  = OffereMaster::where('id',$offere->offere_master_id)->first();
          });
          foreach($get_offere as $offere){
            $get_package = VendorPackage::where('vendor_id',$offere->offeremaster->vendor_id)->first();
          if( $get_package->end_date < $currentdate){
          }
          else{
            $vendor_id = $offere->offeremaster->vendor_id;
            $start_date = $get_package->start_date; 
            $end_date = $get_package->end_date;
            $get_send_offere_count = VendorOffereCount::WhereHas('giveofferemaster',function($q) use($vendor_id){
              $q->where( 'vendor_id',$vendor_id);})->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date))->count();
            if($get_send_offere_count==null){
            $get_send_offere_count = 0;
            }
            if($get_package->no_of_post == $get_send_offere_count){
            }
            else{ 
            $checkgiveofferemaster = GiveOffereMaster::where('offere_request_id',$requestoffere->id)->where('vendor_id',$offere->offeremaster->vendor_id)->first();
            if($checkgiveofferemaster){
              $give_offere_master_id = $checkgiveofferemaster->id;
              if(Auth::user()->fcm_token!=null){
              $noti = new Notification;
              $noti->toSingleDevice(Auth::user()->fcm_token,'Insto','Offer Recevived on'.$get_product->product_name.' by '.$offere->offeremaster->vendor->first_name. ' '.$offere->offeremaster->vendor->last_name.'',null,null);
              }
              $insto_notification = new InstoNotification();
              $insto_notification->customer_id  = Auth::user()->userType->id;
              $insto_notification->vendor_id    = $offere->offeremaster->vendor_id;
              $insto_notification->product_id   = $get_product->id;
              $insto_notification->message_type = "customer_received_offer";
              $insto_notification->message      = 'Offer Recevived on'.$get_product->product_name.' by '.$offere->offeremaster->vendor->first_name. ' '.$offere->offeremaster->vendor->last_name.'';
              $insto_notification->status       = "new";
              $insto_notification->save();
            }
            else{
              $giveofferemaster = new GiveOffereMaster();
              $giveofferemaster->offere_request_id = $requestoffere->id;
              $giveofferemaster->vendor_id         = $offere->offeremaster->vendor_id;
              $giveofferemaster->status            = "offere_sent";
              $giveofferemaster->save();
              $give_offere_master_id = $giveofferemaster->id;
              if(Auth::user()->fcm_token!=null){
              $noti = new Notification;
              $noti->toSingleDevice(Auth::user()->fcm_token,'Insto','Offer Recevived on'.$get_product->product_name.' by '.$offere->offeremaster->vendor->first_name. ' '.$offere->offeremaster->vendor->last_name.'',null,null);
              }
              $insto_notification = new InstoNotification();
              $insto_notification->customer_id  = Auth::user()->userType->id;
              $insto_notification->vendor_id    = $offere->offeremaster->vendor_id;
              $insto_notification->product_id   = $get_product->id;
              $insto_notification->message_type = "customer_received_offer";
              $insto_notification->message      = 'Offer Recevived on'.$get_product->product_name.' by '.$offere->offeremaster->vendor->first_name. ' '.$offere->offeremaster->vendor->last_name.'';
              $insto_notification->status       = "new";
              $insto_notification->save();
            }
            $checkofferecount = VendorOffereCount::where('give_master_id',$give_offere_master_id)->where('status','auto')->first();
            if($checkofferecount){}
            else{
              $vendor_offere_count = new VendorOffereCount();
              $vendor_offere_count->give_master_id = $give_offere_master_id;
              $vendor_offere_count->vendor_id      = $offere->offeremaster->vendor_id;
              $vendor_offere_count->status         = "auto";
              $vendor_offere_count->save();
              $get_product = Product::where('id',$request->product_id)->first();
              $get_product->get_offere_number = $get_product->get_offere_number == null ? 1 : $get_product->get_offere_number + 1;
              $get_product->save();
            }
            $giveofferedetail = new GiveOffereDetail();
            $giveofferedetail->give_offere_master_id = $give_offere_master_id;
            $giveofferedetail->offere_id             = $offere->id;
            $giveofferedetail->cash_price            = $offere->cash_price;
            $giveofferedetail->status                = "offere_sent";
            $giveofferedetail->save(); 
          }
          }
          } 
          $status=true;
          $errMsg = 'Request Successfully Send';
        }
        else{
        $status=false;
        $errMsg = 'Request Not Sent';   
        }
      }
        return response()->json([
          'status'    => $status,
          'errMsg'    => $errMsg
        ]);
    }

    public function getcustomerrequestoffere(Request $request){
      $currentdate = date('Y-m-d');
        $request_offere   = RequestOffere::with(['Product.ProductImage','getGiveoffereMaster'])
        ->where('customer_id',Auth::user()->userType->id)->get();
        
        
        $request_offere->each(function($master_offere, $key) use($request_offere,$currentdate) {
        $request_offere[$key]->offerecount     = GiveOffereDetail::whereHas('offereDetail', function($q) use($currentdate){
                                                 $q->where('offere_valid_date','>=',$currentdate);})
                                                 ->whereHas('GiveOffereMaster', function($q) use($master_offere){
                                                 $q->where('offere_request_id',$master_offere->id);
        })->count();

        $request_offere[$key]->new_offere     = InstoNotification::where('customer_id',$master_offere->customer_id)
                                                        ->where('product_id',$master_offere->product_id)->where('message_type','customer_received_offer')
                                                        ->where('status','new')->count(); 
        }); 
          return response()->json([
          'request_offere'    => $request_offere
           ]);      
    }

    public function getcustomerwishlist(Request $request){
      $currentdate = date('Y-m-d');
        $wishlists = WishList::has('OffereDetail')->where('customer_id',Auth::user()->userType->id)->get();
        $wishlists->each(function($wishlist, $key) use($wishlists,$currentdate) {
        $wishlists[$key]->get_all_offere_detail  = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.vendor'])
                                                                         ->where('id',$wishlist->offere_id)->first();
            });
          return response()->json([
          'wishlist'    => $wishlists
           ]);      
    }

    public function offere(Request $request){
      $request->validate([
            'product_id'         => 'required',
            'Advance'            => 'required|numeric',
            'month'              => 'required|numeric',
            'installment'        => 'required|numeric',
            'total_price'        => 'required|numeric',
            'cash_price'         => 'required|numeric',
            'featured'           => 'required',
            'post_on_instalment' => 'required',
         ]);

         $status=false;
         $errMsg="";
         $currentdate = date('Y-m-d');  
         if(Auth::user()->status=="deactive"){
          $status=false;
          $errMsg="You are not verified";
         }
         else{
          $get_package = VendorPackage::where('vendor_id',Auth::user()->userType->id)->first();
          if( $get_package->end_date < $currentdate){
            $status=false;
            $errMsg="Your lincesed expire";
          }
          else{
            $check =0;
          if($request->featured == true){
          $get_package = VendorPackage::where('vendor_id',Auth::user()->userType->id)->first();
          $start_date = $get_package->start_date; 
          $end_date = $get_package->end_date;
          $vendor_id = Auth::user()->userType->id;
          
          $get_feature_count = OffereDetail::whereHas('OffereMaster',function($q) use($vendor_id){
          $q->where('vendor_id',$vendor_id);})->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date))
          ->where('featured',true)->count();
          if($get_feature_count == null){
           $get_feature_count =0;
           }
           if($get_package->no_of_feature == $get_feature_count){
            $status=false;
            $errMsg="Your feature credit finished please renew the feature credit";
            $check =1;
           }
          }
          if($check == 0){
          $expired_date = date('Y-m-d', strtotime("+".$get_package->offere_validity." days"));
         
         $offeremastercheck = OffereMaster::where('vendor_id',Auth::user()->userType->id)->where('product_id',$request->product_id)->first();

         $offeremasterid=0;

          if($offeremastercheck){

            $offeremasterid=$offeremastercheck->id;
          }
          else{

              $offeremaster = new OffereMaster();
              $offeremaster->vendor_id  = Auth::user()->userType->id;
              $offeremaster->product_id = $request->product_id;
              $offeremaster->status     = "offere_master";
              $offeremaster->save();
              $offeremasterid=$offeremaster->id;   
          }
         

         $offeredetail = new OffereDetail();
         $offeredetail->offere_master_id   = $offeremasterid;
         $offeredetail->Advance            = $request->Advance;
         $offeredetail->month              = $request->month;
         $offeredetail->installment        = $request->installment;
         $offeredetail->total_price        = $request->total_price;
         $offeredetail->cash_price         = $request->cash_price;
         $offeredetail->post_on_instalment = $request->featured == true ? true : $request->post_on_instalment;
         $offeredetail->featured           = $request->featured;
         $offeredetail->offere_valid_date  = $expired_date;
         $offeredetail->status             = "active";
         $offeredetail->save();
         if($offeredetail){
           $product_id = $request->product_id;
           $get_offere_count = OffereDetail::whereHas('OffereMaster',function($q) use($product_id){
             $q->where('product_id',$product_id);})->where('post_on_instalment',true)->count('id');
             $get_product = Product::where('id',$product_id)->first();
             if($get_offere_count < 0 ){
              $get_product->post_instalment = true;
              $get_product->save();
             }
             else{
               if($offeredetail->post_on_instalment == true){
                $get_product->post_instalment = true;
                $get_product->save();
               }
               else{
                $get_product->post_instalment = false;
              $get_product->save();
               }
             }
            $status=true;
            $errMsg="offere has been created";
         }
         else{
            $errMsg="offere was not saved";
         }
        } 
        }
        }
        return response()->json([
          'status' => $status,
          'ErrorMessage' => $errMsg
           ]);      
    }

    public function getalloffere(Request $request){
        $all_offere=[];
        $expired_offere=[];
        $active_offere=[];
        $status=false;
        $errMsg="";
        $currentdate = date('Y-m-d');
        if(Auth::user()->status=="deactive"){
          $status=false;
          $errMsg="You are not verified";
         }
         else{
          $get_package = VendorPackage::where('vendor_id',Auth::user()->userType->id)->first();
          if( $get_package->end_date < $currentdate){
            $status=false;
            $errMsg="Your lincesed expire";
          }
          else{
            $all_offere     = OffereMaster::with(['OffereDetail','Product.ProductImage'])->where('vendor_id',Auth::user()->userType->id)->get();
            $expired_offere = OffereMaster::with(['expiredoffere','Product.ProductImage'])->has('expiredoffere')->where('vendor_id',Auth::user()->userType->id)->get();
            $active_offere  = OffereMaster::with(['activeoffere','Product.ProductImage'])->where('vendor_id',Auth::user()->userType->id)->get();
            $status=true;
            $errMsg="data fetched successfully";
          }
        }
        return response()->json([
          'status'         => $status,
          'errMsg'         => $errMsg, 
          'all_offere'     => $all_offere,
          'expired_offere' => $expired_offere,
          'active_offere'  => $active_offere
           ]);      
    }

    public function updateoffere(Request $request){
     
     $request->validate([
            'offere_detail_id' => 'required',
            'Advance'            => 'required|numeric',
            'month'              => 'required|numeric',
            'installment'        => 'required|numeric',
            'total_price'        => 'required|numeric',
            'cash_price'         => 'required|numeric',
            'featured'           => 'required',
            'post_on_instalment' => 'required',
        ]);

         $status=false;
         $errMsg="";

         $offeredetail = OffereDetail::findOrFail($request->offere_detail_id);
         if($offeredetail){
          $check = 0;
           if($offeredetail->featured == false && $request->featured == true){
            $get_package = VendorPackage::where('vendor_id',Auth::user()->userType->id)->first();
            $start_date = $get_package->start_date; 
            $end_date = $get_package->end_date;
            $vendor_id = Auth::user()->userType->id;
            
            $get_feature_count = OffereDetail::whereHas('OffereMaster',function($q) use($vendor_id){
            $q->where('vendor_id',$vendor_id);})->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date))
            ->where('featured',true)->count();
            if($get_feature_count == null){
             $get_feature_count =0;
             }
             if($get_package->no_of_feature == $get_feature_count){
              $status=false;
              $errMsg="Your feature credit finished please renew the feature credit";
              $check =1;
             }
           }
           if($check == 0){
          $offeredetail->Advance            = $request->Advance;
          $offeredetail->month              = $request->month;
          $offeredetail->installment        = $request->installment;
          $offeredetail->total_price        = $request->total_price;
          $offeredetail->cash_price         = $request->cash_price;
          $offeredetail->featured           = $request->featured;
          $offeredetail->post_on_instalment = $request->featured == true ? true : $request->post_on_instalment;
          $offeredetail->status             = "active";
          $offeredetail->save();
          $get_offere_master = OffereMaster::where('id',$offeredetail->offere_master_id)->first();
          $get_offere_count = OffereDetail::whereHas('OffereMaster',function($q) use($get_offere_master){
            $q->where('product_id',$get_offere_master->product_id);})->where('post_on_instalment',true)->count('id');
            $get_product = Product::where('id',$get_offere_master->product_id)->first();
            if($get_offere_count < 0 ){
             $get_product->post_instalment = true;
             $get_product->save();
            }
            else{
              if($offeredetail->post_on_instalment == true){
                $get_product->post_instalment = true;
                $get_product->save();
               }
               else{
              $get_product->post_instalment = false;
              $get_product->save();
               }
            }
          $status=true;

          $errMsg="offere has been updated";
         }
        }
         else{
            $errMsg="offere was not update";
         } 
          return response()->json([
          'status' => $status,
          'ErrorMessage' => $errMsg
           ]);
    }

    public function deleteoffere(Request $request){
     
     $request->validate([
            'offere_detail_id' => 'required',
        ]);

         $status=false;
         $errMsg="";

         $offeredetail = OffereDetail::where('id',$request->offere_detail_id)->first();
         if($offeredetail){
          $get_offere_master = OffereMaster::where('id',$offeredetail->offere_master_id)->first();
          $get_offere_count = OffereDetail::whereHas('OffereMaster',function($q) use($get_offere_master){
            $q->where('product_id',$get_offere_master->product_id);})->where('post_on_instalment',true)->count('id');
            $get_product = Product::where('id',$get_offere_master->product_id)->first();
            if($get_offere_count < 0 ){
             $get_product->post_instalment = true;
             $get_product->save();
            }
            else{
              if($offeredetail->post_on_instalment == true){
                $get_product->post_instalment = true;
                $get_product->save();
               }
               else{
              $get_product->post_instalment = false;
              $get_product->save();
               }
            }
            $offeredetail->delete();
            $status=true;
            $errMsg="offere has been deleted";
         }
         else{

            $errMsg="offere was not delete";
         } 

          return response()->json([
          'status' => $status,
          'ErrorMessage' => $errMsg
           ]);
    }

    public function adddeletewishlist(Request $request){
     
     $request->validate([
            'offere_id' => 'required',
        ]);

        $status = 0;
        $errMsg = "";
        $getwishlistcount =0;
       $get_wish_list = WishList::where('offere_id',$request->offere_id)->where('customer_id',Auth::user()->userType->id)->first();
       if($get_wish_list){
        $get_wish_list->delete();
        $status = 1;
        $errMsg = "Ofere Remove Successfully";
        //$getwishlistcount = WishList::where('customer_id',Auth::user()->userType->id)->count();
          //if($getwishlistcount){
            // Session::put('wishlist_count',$getwishlistcount);
            //}
            //else{
            //Session::put('wishlist_count',0);
            //$getwishlistcount = 0;
          // }
       }   
       else{
           $get_offere_detail_for_cash = OffereDetail::where('id',$request->offere_id)->first();
           $wish_list = new WishList();
           $wish_list->offere_id   = $request->offere_id;
           $wish_list->customer_id = Auth::user()->userType->id;
           $wish_list->cash_price  = $get_offere_detail_for_cash->cash_price;
           $wish_list->save();
           $status = 2;
           $errMsg = "Ofere Add Successfully";
           //$getwishlistcount = WishList::where('customer_id',Auth::user()->userType->id)->count();
           //if($getwishlistcount){
            //Session::put('wishlist_count',$getwishlistcount);
           //}
           //else{
           //Session::put('wishlist_count',0);
           //$getwishlistcount = 0;
          //}
        } 

        return response()->json([
        'status' => $status,
        'ErrorMessage' => $errMsg
        ]);
    }

    public function changeofferedate(Request $request){
      $request->validate([
        'offere_id' => 'required|numeric',
      ]);
      $status=false;
      $errMsg="";
      $currentdate = date('Y-m-d');
      
      $get_package = VendorPackage::where('vendor_id',Auth::user()->userType->id)->first();
          if( $get_package->end_date < $currentdate){
            $status=false;
            $errMsg="Your lincesed expire";
          }
          else{
          $expired_date = date('Y-m-d', strtotime("+".$get_package->offere_validity." days"));
          $getoffere = OffereDetail::where('id',$request->offere_id)->first();
          if($getoffere){
            $getoffere->offere_valid_date = $expired_date;
            $getoffere->save();
            $status=true;
            $errMsg="Offere Vaild Date Change"; 
          }
          else{
            $status=false;
            $errMsg="Offfere Mot Found";
          }
        }
        return response()->json([
          'status' => $status,
          'ErrorMessage' => $errMsg
           ]); 
    }

    public function vendorreceivedoffere(){
      $remain_feature = 0;
      $remain_offer_credit = 0;
      $sub_area_ids = VendorSubArea::where('vendor_id',Auth::user()->userType->id)->pluck('sub_area_id')->toArray();
      $get_requests = RequestOffere::with(['getProduct'])->whereHas('Customer',function($q) use($sub_area_ids){
        $q->whereIn('sub_area_id',$sub_area_ids);})->orderBy('id','desc')->get();
      
      
      $get_requests->each(function($get_request, $key) use($get_requests) {
      $get_requests[$key]->customer      = Customer::with(['subarea.area.city'])->where('id',$get_request->customer_id)->first();
      $get_requests[$key]->check_offere  = VendorOffereCount::whereHas('giveofferemaster',function($q) use($get_request){$q->where('offere_request_id',$get_request->id);})->where('status','push')->first();
      });
      $get_package = VendorPackage::where('vendor_id',Auth::user()->userType->id)->first();
      if($get_package){
      $start_date = $get_package->start_date; 
      $end_date = $get_package->end_date;
      $vendor_id = Auth::user()->userType->id;
      $get_send_offere_count = $get_send_offere_count = VendorOffereCount::WhereHas('giveofferemaster',function($q) use($vendor_id){
        $q->where( 'vendor_id',$vendor_id);})->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date))->count();
      if($get_send_offere_count==null){
        $get_send_offere_count = 0;
      }
      $vendor_id = Auth::user()->userType->id;
      $get_feature_count = OffereDetail::whereHas('OffereMaster',function($q) use($vendor_id){
        $q->where('vendor_id',$vendor_id);})->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date))
        ->where('featured',true)->count();
        if($get_feature_count == null){
          $get_feature_count =0;
        }
        $remain_feature = $get_package->no_of_feature - $get_feature_count;
        $remain_offer_credit =  $get_package->no_of_post - $get_send_offere_count;
      }

        return response()->json([
        'get_requests'        => $get_requests,
        'remain_feature'      => $remain_feature,
        'remain_offer_credit' => $remain_offer_credit
         ]);
    }

    public function productwisevendoroffere(Request $request){
      $request->validate([
        'product_id'  => 'required|numeric',
      ]);
      $currentdate = date('Y-m-d');
      $product_id = $request->product_id;
      $vendor_id = Auth::user()->userType->id;
      $get_all_offere = OffereDetail::whereHas('OffereMaster',function($q) use($product_id,$vendor_id){
        $q->where('product_id',$product_id)->where('vendor_id',$vendor_id);})
        ->where('offere_valid_date','>=',$currentdate)->orderBy('id','desc')->get();
        $cash_price = OffereDetail::whereHas('OffereMaster',function($q) use($product_id,$vendor_id){
          $q->where('product_id',$product_id)->where('vendor_id',$vendor_id);})
          ->where('offere_valid_date','>=',$currentdate)->orderBy('created_at','desc')->orderBy('updated_at','desc')->first();
          if($cash_price){
            $cash_price1 = $cash_price->cash_price;
          }
          else{
            $cash_price1=0;
          }
        return response()->json([
        'get_all_offere'    => $get_all_offere,
        'cash_price'        => $cash_price1
         ]);
    }

    public function sendoffere(Request $request){

      $request->validate([
        'offere_id'    => 'required',
        //'offere_id.*'  => 'required|numeric|distinct',
        'request_id'   => 'required'
      ]);
        $status = false;
        $message ="";
        $currentdate = date('Y-m-d');
        $offere_id = json_decode($request->offere_id, true);

        $get_package = VendorPackage::where('vendor_id',Auth::user()->userType->id)->first();
        if( $get_package->end_date < $currentdate){
          $status=false;
          $errMsg="Your lincesed expire";
        }
        else{
          $start_date = $get_package->start_date; 
          $end_date = $get_package->end_date;
          $vendor_id = Auth::user()->userType->id;
      $get_send_offere_count = $get_send_offere_count = VendorOffereCount::WhereHas('giveofferemaster',function($q) use($vendor_id){
        $q->where( 'vendor_id',$vendor_id);})->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date))->count();
          if($get_send_offere_count==null){
            $get_send_offere_count = 0;
          }
          if($get_package->no_of_post == $get_send_offere_count){
           $status=false;
           $errMsg="Your offers credit finished please renew the offers credit";
          }
          else{
        foreach($offere_id as $offere){

          $checkgiveofferemaster = GiveOffereMaster::where('offere_request_id',$request->request_id)->where('vendor_id',Auth::user()->userType->id)->first();
          if($checkgiveofferemaster){
            $give_offere_master_id = $checkgiveofferemaster->id; 
          } 
          else{
            $giveofferemaster = new GiveOffereMaster();
            $giveofferemaster->offere_request_id = $request->request_id;
            $giveofferemaster->vendor_id         = Auth::user()->userType->id;
            $giveofferemaster->status            = "offere_sent";
            $giveofferemaster->save();
            $give_offere_master_id = $giveofferemaster->id;
          }
          $checkofferecount = VendorOffereCount::where('give_master_id',$give_offere_master_id)->where('status','push')->first();
            if($checkofferecount){}
            else{
              $vendor_offere_count = new VendorOffereCount();
              $vendor_offere_count->give_master_id = $give_offere_master_id;
              $vendor_offere_count->vendor_id      = Auth::user()->userType->id;
              $vendor_offere_count->status         = "push";
              $vendor_offere_count->save();
              $get_product_id = RequestOffere::where('id',$request->request_id)->first();
              $get_product = Product::where('id',$get_product_id->product_id)->first();
              $get_product->get_offere_number = $get_product->get_offere_number == null ? 1 : $get_product->get_offere_number + 1;
              $get_product->save();
            }
            $check_offere_detail = GiveOffereDetail::where('give_offere_master_id',$give_offere_master_id)->where('offere_id',$offere)->first();
            if($check_offere_detail){

            }
            else{
              $get_offere_detail_for_cash = OffereDetail::where('id',$offere)->first();
             $giveofferedetail = new GiveOffereDetail();
             $giveofferedetail->give_offere_master_id = $give_offere_master_id;
             $giveofferedetail->offere_id             = $offere;
             $giveofferedetail->cash_price            = $get_offere_detail_for_cash->cash_price;
             $giveofferedetail->status                = "offere_sent";
             $giveofferedetail->save();

             $get_request_customer = RequestOffere::with(['Customer.user','getProduct'])->where('id',$request->request_id)->first();
            if($get_request_customer->customer->user->fcm_token !=null){
              $noti = new Notification;
              $noti->toSingleDevice($get_request_customer->customer->user->fcm_token,'Insto','Offer Recevived on'.$get_request_customer->getproduct->product_name.' by '. Auth::user()->userType->first_name. ' '. Auth::user()->userType->last_name.'',null,null);
            }
            $insto_notification = new InstoNotification();
            $insto_notification->customer_id  = $get_request_customer->customer->id;
            $insto_notification->vendor_id    = Auth::user()->userType->id;
            $insto_notification->product_id   = $get_request_customer->getproduct->id;
            $insto_notification->message_type = "customer_received_offer";
            $insto_notification->message      = 'Offer Recevived on'.$get_request_customer->getProduct->product_name.' by '. Auth::user()->userType->first_name. ' '. Auth::user()->userType->last_name.'';
            $insto_notification->status       = "new";
            $insto_notification->save();
            } 

          $status=true;
          $errMsg="Offer Send Successfully";
          }
        }
       } 
      return response()->json([
      'status' => $status,
      'errMsg' => $errMsg
      ]);
    }

    public function productreport(){
      $month = date('m');
      $get_new_product_count     = Product::where('new_arrival',true)->count('id');
      $get_new_product           = Product::with('ProductImage')->where('new_arrival',true)->get();
      $get_hot_product_count     = Product::where('hot_product',true)->count('id');
      $get_hot_product           = Product::with('ProductImage')->where('hot_product',true)->get();
      $get_popular_product_count = Product::where('request_offere_number','!=',0)->count('id');
      if($get_popular_product_count > 10){
        $get_popular_product_count = 10;
      }
      $get_popular_product       = Product::with('ProductImage')->where('request_offere_number','!=',0)->orderBy('request_offere_number','desc')->limit(10)->get();
      $sub_area_ids = VendorSubArea::where('vendor_id',Auth::user()->userType->id)->pluck('sub_area_id')->toArray();
      $get_total_received_request1 = InstoNotification::where('vendor_id',Auth::user()->userType->id)->whereMonth('created_at',$month)->groupBy('product_id')->orderBy(DB::raw('count(product_id)','DESC'),'DESC')->pluck('product_id')->toArray();
      $get_total_received_request = count($get_total_received_request1);
      $get_received_request_product = Product::with('ProductImage')->withTrashed()->whereIn('id',$get_total_received_request1)->get(); 
      return response()->json([
        'get_new_product_count'        => $get_new_product_count,
        'get_new_product'              => $get_new_product,
        'get_hot_product_count'        => $get_hot_product_count,
        'get_hot_product'              => $get_hot_product,
        'get_popular_product_count'    => $get_popular_product_count,
        'get_popular_product'          => $get_popular_product,
        'get_total_received_request'   => $get_total_received_request,
        'get_received_request_product' => $get_received_request_product

        ]);
    }

    public function customerreport(){
      $sub_area_ids = VendorSubArea::where('vendor_id',Auth::user()->userType->id)->pluck('sub_area_id')->toArray();
      $get_total_customer_ids = RequestOffere::withTrashed()->whereHas('Customer',function($q) use($sub_area_ids){
        $q->whereIn('sub_area_id',$sub_area_ids);})->distinct('customer_id')->pluck('customer_id')->toArray();
      $get_total_customer_count = count($get_total_customer_ids);
      $get_total_customer = Customer::whereIn('id',$get_total_customer_ids)->orderBy('id','desc')->get();
      $get_total_engage_customer = Order::where('vendor_id',Auth::user()->userType->id)->distinct('customer_id')->count('customer_id');
      $get_engage_customer = Order::with(['getProduct','Customer','OffereDetail'])->where('vendor_id',Auth::user()->userType->id)->get();
      return response()->json([
        'get_total_customer_count'  => $get_total_customer_count,
        'get_total_customer'        => $get_total_customer, 
        'get_total_engage_customer' => $get_total_engage_customer,
        'get_engage_customer'       => $get_engage_customer
        ]);
    }

    public function offerereport(){
      $sub_area_ids = VendorSubArea::where('vendor_id',Auth::user()->userType->id)->pluck('sub_area_id')->toArray();
      $get_total_received_request1 = InstoNotification::where('vendor_id',Auth::user()->userType->id)->groupBy('product_id')->orderBy(DB::raw('count(product_id)','DESC'),'DESC')->pluck('product_id')->toArray();
      $get_total_received_request = count($get_total_received_request1);     
      $get_total_send_request = GiveOffereMaster::where('vendor_id',Auth::user()->userType->id)->count('id');
      return response()->json([
        'get_total_received_request' => $get_total_received_request,
        'get_total_send_request'     => $get_total_send_request

        ]);
    }

    public function gettotalvendorbycustomerrequest(Request $request){
      $request->validate([
        'product_id'         => 'required',
        ]);
        $subareaid = Auth::user()->userType->sub_area_id;
        $get_total_vendor = OffereMaster::where('product_id',$request->product_id)
          ->whereHas('Vendor.SubArea',function($q) use($subareaid){
            $q->where('sub_area_id',$subareaid);})->count();
            return response()->json([
              'get_total_vendor' => $get_total_vendor
            ]);
    }

    public function newofferestatuschange(Request $request){
      $request->validate([
        'product_id' => 'required',
        'id'         => 'required',
        'type'       => 'required',
        ]);
        $status = false;
        if($request->type == 'customer_received_offer'){
        $notifications = InstoNotification::where('customer_id',$request->id)
                                          ->where('product_id',$request->product_id)
                                          ->where('message_type',$request->type)
                                          ->where('status','new')->get();
          foreach($notifications as $notification){
          $notification->status = 'seen';
          $notification->save();
          $status =true;
          }
        }
        return response()->json([
          'status' => $status
        ]); 

    }
    public function vendorcredit(){
      $remain_feature = 0;
      $remain_offer_credit = 0;
      $get_package = VendorPackage::where('vendor_id',Auth::user()->userType->id)->first();
      if($get_package){
      $start_date = $get_package->start_date; 
      $end_date = $get_package->end_date;
      $vendor_id = Auth::user()->userType->id;
      $get_send_offere_count = $get_send_offere_count = VendorOffereCount::WhereHas('giveofferemaster',function($q) use($vendor_id){
        $q->where( 'vendor_id',$vendor_id);})->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date))->count();
      if($get_send_offere_count==null){
        $get_send_offere_count = 0;
      }
      $vendor_id = Auth::user()->userType->id;
      $get_feature_count = OffereDetail::whereHas('OffereMaster',function($q) use($vendor_id){
        $q->where('vendor_id',$vendor_id);})->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date))
        ->where('featured',true)->count();
        if($get_feature_count == null){
          $get_feature_count =0;
        }
        $remain_feature = $get_package->no_of_feature - $get_feature_count;
        $remain_offer_credit =  $get_package->no_of_post - $get_send_offere_count;
      }

        return response()->json([
        'remain_feature'      => $remain_feature,
        'remain_offer_credit' => $remain_offer_credit
         ]);
    }

}