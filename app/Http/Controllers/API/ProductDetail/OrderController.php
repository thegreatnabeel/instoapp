<?php

namespace App\Http\Controllers\API\ProductDetail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Order;
use App\OffereDetail;
use App\GiveOffereDetail;
use Auth;
class OrderController extends Controller
{

  public function getallorder()
    {
      $get_order_history    = Order::with(['OffereDetail.OffereMaster.Vendor','OffereDetail.OffereMaster.Product'])->where('customer_id',Auth::user()->userType->id)->get();
      return response()->json([
        'get_order_history'    => $get_order_history
         ]);
    }

  public function addorder(Request $request){
    $request->validate([
           'offere_id'       => 'required',
       ]);
      $CurrentDate=date('Y-m-d');
      $customer_id =0;
      $status = false;
      $errMsg= '';
      $check_order = Order::where('customer_id',Auth::user()->userType->id)->where('offere_id',$request->offere_id)->first();
      if($check_order){
          $status = false;
          $errMsg= 'Already order Saved';
      } 
      else{
        $get_offere_detail_for_cash = GiveOffereDetail::where('offere_id',$request->offere_id)->first();
        $get_offere_detail = OffereDetail::with(['OffereMaster','OffereMaster.Vendor'])->where('id',$request->offere_id)->first();
          $order = new  Order();
          $order->customer_id = Auth::user()->userType->id;
          $order->offere_id   = $request->offere_id;
          $order->vendor_id   = $get_offere_detail->offeremaster->vendor_id;
          $order->product_id  = $get_offere_detail->offeremaster->product_id;
          $order->cash_price  = $get_offere_detail_for_cash->cash_price;
          $order->order_date  = $CurrentDate;
          $order->place_order = 'insto';
          $order->status      = 'pending';
          $order->save();
          $status = true;
          $errMsg= 'Order Saved Successfully';
      }
         return response()->json([
         'status'    => $status,
         'message'    => $errMsg
          ]);      
   }
}