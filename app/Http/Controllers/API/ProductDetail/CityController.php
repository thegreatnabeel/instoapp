<?php

namespace App\Http\Controllers\API\ProductDetail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\City;
use App\Area;
use App\SubArea;

class CityController extends Controller
{

    public function cities()
    {
        $cities=City::all();
        return $cities;
    }

    public function area(Request $request)
    {
        $request->validate([
            'city_id'        => 'required',
        ]);
        $areas=Area::where('city_id',$request->city_id)->get();
        return $areas;
    }

    public function subarea(Request $request)
    {
        $request->validate([
            'area_id'        => 'required',
        ]);
        $sub_areas=SubArea::where('area_id',$request->area_id)->get();
        return $sub_areas;
    }

    public function subareabycity(Request $request)
    {
        $request->validate([
            'city_id'        => 'required',
        ]);
        $a=$request->city_id;
        $sub_areas=SubArea::whereHas('area',function($q) use ($a){
                            $q->where('city_id',$a);
                            })->get();
        return $sub_areas;
    }

    public function multisubarea(Request $request)
    {
        $request->validate([
            'area_id'        => 'required',
        ]);
        $area_ids = json_decode($request->area_id, true);
        $sub_areas=SubArea::whereIn('area_id',$area_ids)->get();
        return $sub_areas;
    }
}