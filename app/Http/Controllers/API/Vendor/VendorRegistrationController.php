<?php

namespace App\Http\Controllers\API\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use Validator; 
use App\Package;
use App\Vendor;
use App\VendorPackage;
use App\VendorSubArea;
use App\VendorSubCatagory;
use App\User;
use App\Area;
use App\Catagory;
use App\City;
use App\Otp;
class VendorRegistrationController extends Controller
{
  use AuthenticatesUsers;

    public function packages()
    {
        $packages=Package::all();
        return response()->json([
          'packages'    => $packages
           ]);
    }

    public function personalClientName()
    {
        return 'Personal Grant - Mobile Apps';
    }
    
    public function vendorregistration(Request $request){

      /*$to_name = 'nabeel';
      $to_email = 'mnabeel992@gmail.com';
      $data = array('name'=>"Sam Jose", "body" => "Test mail");
      Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
      $message->to($to_email, $to_name)->subject('Artisans Web Testing Mail');
      $message->from('test@abc.instalment.pk','Artisans Web');
      });*/
      $currentDate=date('Y-m-d');
      $newDateMonth= date('Y-m-d', strtotime("+30 days"));
           $request->validate([
           'first_name'          => 'required|string',
           'last_name'           => 'required|string',
           'company_name'        => 'required|string',
           'designation'         => 'required|string',
           'cnic'                => 'required|string',
           'business_address'     => 'required|string',
           'company_slug'        => 'required|string',
           'base_address'        => 'required|string',
           //'password'            => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
           'password'            => 'required|string|min:4|confirmed',
           'email'               => [
                                     'nullable',
                                     'email',
                                      Rule::unique('users')->where(function($query) {
                                      $query->whereNotNull('email');
                                       })
                                     ],
           'user_type'           => 'required|string',
           'google_id'          => 'nullable',
           'facebook_id'        => 'nullable',
           'otp_code'            => 'required|numeric',
           'sub_area_id*'        => 'required',
           'sub_catagory_id*'    => 'required',
           'phone_number'        => 'required',
           'package_id'          => 'required',
           'picture_path'        => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
           'cnic_front_img_path' => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
           'cnic_back_img_path'  => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
            ]);
            $PNumber = ltrim($request->phone_number, '0');
            $PNumber='92'.$PNumber;
            $getuser = User::where('phone_number',$PNumber)->where('user_type','vendor')->first();
            if($getuser){$getuser = User::where('phone_number',$PNumber)->where('user_type','vendor')->first();
              return response()->json([ 'message' => "User Already Register"]);
            }
            else{
           $user =new User;
           $user->name          = $request->first_name.' '.$request->last_name;
           $user->email         = $request->email == "" ? NULL : strtolower($request->email);
           $user->password      = !empty($request->password) ? bcrypt($request->password):null;
           $user->phone_number  = $PNumber;
           $user->google_id     = $request->google_id == ""? null : $request->google_id;
           $user->facebook_id   = $request->facebook_id == ""? null : $request->facebook_id;
           $user->timezone      = $request->timezone;
           $user->user_type     = 'vendor';
           $user->status        = 'deactive';
           $user->user_status   = true; 
           $userInfo            = $user->save();
           if($userInfo){
            $imageName=null;
            if($request->hasFile('picture_path'))
              {
            $imageName = $request->file('picture_path')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'vendorimages', $request->file('picture_path'), $imageName
            );
            $imageName='storage/'.$path;
            }
            else
             {
               $imageName = null;
             }

             $forntcnicimagepath=null;
            if($request->hasFile('cnic_front_img_path'))
              {
            $forntcnicimagepath = $request->file('cnic_front_img_path')->getClientOriginalName();
            $forntcnicimagepath = str_replace(" ","",$forntcnicimagepath);
            $forntcnicimagepath = time().'_'.$forntcnicimagepath;

            $path = Storage::disk('public')->putFileAs(
                'vendorimages', $request->file('cnic_front_img_path'), $forntcnicimagepath
            );
            $forntcnicimagepath='storage/'.$path;
            }
            else
             {
               $forntcnicimagepath = null;
             }

             $backcnicimagepath=null;
            if($request->hasFile('cnic_back_img_path'))
              {
            $backcnicimagepath = $request->file('cnic_back_img_path')->getClientOriginalName();
            $backcnicimagepath = str_replace(" ","",$backcnicimagepath);
            $backcnicimagepath = time().'_'.$backcnicimagepath;

            $path = Storage::disk('public')->putFileAs(
                'vendorimages', $request->file('cnic_back_img_path'), $backcnicimagepath
            );
            $backcnicimagepath='storage/'.$path;
            }
            else
             {
               $backcnicimagepath = null;
             }

             $vendor = new Vendor;
             $vendor->user_id              = $user->id;
             $vendor->first_name           = $request->first_name;   
             $vendor->last_name            = $request->last_name;
             $vendor->base_address         = $request->base_address;
             $vendor->opt_code             = $request->otp_code;
             $vendor->primary_phone_number = $PNumber;
             $vendor->company_name         = $request->company_name;
             $vendor->designation          = $request->designation;
             $vendor->cnic                 = $request->cnic;
             $vendor->business_address     = $request->business_address;
             $vendor->company_slug         = $request->company_slug;
             $vendor->company_slug         = $request->company_slug;
             $vendor->verfication_status   = 'Unverfied';
             $vendor->register_type        = 'instoapp';
             $vendor->picture_path         = $imageName;
             $vendor->cnic_front_img_path  = $forntcnicimagepath;
             $vendor->cnic_back_img_path   = $backcnicimagepath;
             $vendor->status               = true;
             $vendor->save();

             $package = Package::where('id',2)->first();
             $vendorpackage = new VendorPackage();
             $vendorpackage->package_id    = $request->package_id;
             $vendorpackage->vendor_id     = $vendor->id;
             $vendorpackage->no_of_feature = $package->no_of_feature;
             $vendorpackage->no_of_post    = $package->no_of_post;
             $vendorpackage->start_date    = $currentDate;
             $vendorpackage->end_date      = $newDateMonth;
             $vendorpackage->status        = "active";
             $vendorpackage->save();
             if($request->package_id !=2){
               /*$to_name = 'nabeel';
                $to_email = 'mnabeel992@gmail.com';
                $data = array('name'=>"Sam Jose", "body" => "Test mail");
                Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject('Artisans Web Testing Mail');
                $message->from('test@abc.instalment.pk','Artisans Web');
               });*/    
             }
             $SubAreas = json_decode($request->sub_area_id, true);

             foreach ($SubAreas as $key => $SubAreaid) {
               $vendorsubarea = new VendorSubArea();
               $vendorsubarea->vendor_id   = $vendor->id;
               $vendorsubarea->sub_area_id = $SubAreaid;
               $vendorsubarea->save();
             }
             $SubCatagories = json_decode($request->sub_catagory_id, true);

             foreach ($SubCatagories as $key => $SubCatagoryId) {
               $vendorsubcatagory = new VendorSubCatagory();
               $vendorsubcatagory->vendor_id       = $vendor->id;
               $vendorsubcatagory->sub_catagory_id = $SubCatagoryId;
               $vendorsubcatagory->save();
             }
             $getotp=Otp::where('phone_number',$PNumber)->where('type','vendor')->first();
             if($getotp){
                 $getotp->status = 'complete';
                 $getotp->save();
             }
           
           }
           \Auth::login($user);
           $token           = $user->createToken($this->personalClientName(), ['*']);
            $user->token    = $token->accessToken;
            $user->userType;
            $user->sub_area = VendorSubArea::with('SubArea.AreaId')->where('vendor_id',$user->userType->id)->get();
            $subareaids = VendorSubArea::where('vendor_id',$user->userType->id)->pluck('sub_area_id');
            $user->area = Area::whereHas('subarea', function($q) use($subareaids){
                $q->whereIn('id',$subareaids);})->get();
            $areaids = Area::whereHas('subarea', function($q) use($subareaids){
                    $q->whereIn('id',$subareaids);})->pluck('id');

            $user->city = City::whereHas('area', function($q) use($areaids){
                    $q->whereIn('id',$areaids);})->get();

            $user->vendor_package = VendorPackage::where('vendor_id',$user->userType->id)->get();
            $user->sub_category = VendorSubCatagory::with('SubCategory.CategoryId')->where('vendor_id',$user->userType->id)->get();
            $subcategoryids = VendorSubCatagory::where('vendor_id',$user->userType->id)->pluck('sub_catagory_id');
            $user->category = Catagory::whereHas('SubCatagory', function($q) use($subcategoryids){
                                            $q->whereIn('id',$subcategoryids);})->get();
            return $user;
        }
      }

      public function vendorprofileupdate(Request $request){
             $request->validate([
             'first_name'          => 'required|string',
             'last_name'           => 'required|string',
             'company_name'        => 'required|string',
             'designation'         => 'required|string',
             'cnic'                => 'required|string',
             'business_address'     => 'required|string',
             'company_slug'        => 'required|string',
             'base_address'        => 'required|string',
             'email'              => [
                                       'nullable',
                                       'email',
                                       Rule::unique('users')->where(function($query) use($request) {
                                       $query->whereNotNull('email')->where('id','!=',auth()->user()->id);
                                      })
                                     ],
             'sub_area_id*'        => 'required',
             'sub_catagory_id*'    => 'required',
             'picture_path'        => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
             'cnic_front_img_path' => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
             'cnic_back_img_path'  => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
              ]);
             auth()->user()->name          = $request->first_name.' '.$request->last_name;
             auth()->user()->email         = $request->email == "" ? NULL : strtolower($request->email);
             auth()->user()->save();

             $user = auth()->user()->userType;

             if($user){
              $imageName=null;
              if($request->hasFile('picture_path'))
                {
              $imageName = $request->file('picture_path')->getClientOriginalName();
              $imageName = str_replace(" ","",$imageName);
              $imageName = time().'_'.$imageName;
  
              $path = Storage::disk('public')->putFileAs(
                  'vendorimages', $request->file('picture_path'), $imageName
              );
              $imageName='storage/'.$path;
              }
              else
               {
                 $imageName = null;
               }
  
               $forntcnicimagepath=null;
              if($request->hasFile('cnic_front_img_path'))
                {
              $forntcnicimagepath = $request->file('cnic_front_img_path')->getClientOriginalName();
              $forntcnicimagepath = str_replace(" ","",$forntcnicimagepath);
              $forntcnicimagepath = time().'_'.$forntcnicimagepath;
  
              $path = Storage::disk('public')->putFileAs(
                  'vendorimages', $request->file('cnic_front_img_path'), $forntcnicimagepath
              );
              $forntcnicimagepath='storage/'.$path;
              }
              else
              {
                $forntcnicimagepath = null;
              }
  
              $backcnicimagepath=null;
              if($request->hasFile('cnic_back_img_path'))
                {
              $backcnicimagepath = $request->file('cnic_back_img_path')->getClientOriginalName();
              $backcnicimagepath = str_replace(" ","",$backcnicimagepath);
              $backcnicimagepath = time().'_'.$backcnicimagepath;
  
              $path = Storage::disk('public')->putFileAs(
                  'vendorimages', $request->file('cnic_back_img_path'), $backcnicimagepath
              );
              $backcnicimagepath='storage/'.$path;
              }
              else
              {
                $backcnicimagepath = null;
              }

              $user->first_name           = $request->first_name;   
              $user->last_name            = $request->last_name;
              $user->base_address         = $request->base_address;
              $user->company_name         = $request->company_name;
              $user->designation          = $request->designation;
              $user->cnic                 = $request->cnic;
              $user->business_address     = $request->business_address;
              $user->company_slug         = $request->company_slug;
              $user->company_slug         = $request->company_slug;
              $user->picture_path         = $imageName == null ? $user->picture_path : $imageName;
              $user->cnic_front_img_path  = $forntcnicimagepath == null ? $user->cnic_front_img_path : $forntcnicimagepath;
              $user->cnic_back_img_path   = $backcnicimagepath == null ? $user->cnic_back_img_path : $backcnicimagepath;
              $user->save();

              $SubAreas = json_decode($request->sub_area_id, true);
              if(count($SubAreas) > 0){
              $deletesubarea = VendorSubArea::where('vendor_id',$user->id)->get();

              foreach($deletesubarea as $area){
                $area->delete();
              }
              foreach ($SubAreas as $key => $SubAreaid) {
                $vendorsubarea = new VendorSubArea();
                $vendorsubarea->vendor_id   = $user->id;
                $vendorsubarea->sub_area_id = $SubAreaid;
                $vendorsubarea->save();
              }
            }

            $SubCatagories = json_decode($request->sub_catagory_id, true);
            if(count($SubCatagories)>0){
              $deleteSubCatagories = VendorSubCatagory::where('vendor_id',$user->id)->get();

              foreach($deleteSubCatagories as $subcategory){
                $subcategory->delete();
              }
              foreach ($SubCatagories as $key => $SubCatagoryId) {
                $vendorsubcatagory = new VendorSubCatagory();
                $vendorsubcatagory->vendor_id       = $user->id;
                $vendorsubcatagory->sub_catagory_id = $SubCatagoryId;
                $vendorsubcatagory->save();
              }
            }
              $user = Auth()->user();
              $user->sub_area = VendorSubArea::with('SubArea.AreaId')->where('vendor_id',$user->userType->id)->get();
              $subareaids = VendorSubArea::where('vendor_id',$user->userType->id)->pluck('sub_area_id');
              $user->area = Area::whereHas('subarea', function($q) use($subareaids){
                $q->whereIn('id',$subareaids);})->get();
              $areaids = Area::whereHas('subarea', function($q) use($subareaids){
                    $q->whereIn('id',$subareaids);})->pluck('id');

              $user->city = City::whereHas('area', function($q) use($areaids){
                    $q->whereIn('id',$areaids);})->get();

                $user->vendor_package = VendorPackage::where('vendor_id',$user->userType->id)->get();
                $user->sub_category = VendorSubCatagory::with('SubCategory.CategoryId')->where('vendor_id',$user->userType->id)->get();
                $subcategoryids = VendorSubCatagory::where('vendor_id',$user->userType->id)->pluck('sub_catagory_id');
                $user->category = Catagory::whereHas('SubCatagory', function($q) use($subcategoryids){
                                            $q->whereIn('id',$subcategoryids);})->get();
              return $user;
            }
            else{
              return response()->json([ 'message' => "Vendor cannot update"]);
            }
        }

        public function vendorregistrationvalidation(Request $request){
          if($request->type == 'register'){ 
          $validator = Validator::make($request->all(), [
            'first_name'         => 'required|string',
            'last_name'           => 'required|string',
            'company_name'        => 'required|string',
            'designation'         => 'required|string',
            'cnic'                => 'required|string',
           //'password'          => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'password'            => 'required|string|min:4|confirmed',
            'email'               => [
                                     'nullable',
                                     'email',
                                      Rule::unique('users')->where(function($query) {
                                      $query->whereNotNull('email');
                                       })
                                     ],
            'user_type'           => 'required|string',
            'otp_code'            => 'required|numeric',
            'phone_number'        => 'required',
             ]);

             return $validator->errors()->toJson();
          }
          else{
            $validator = Validator::make($request->all(), [
              'first_name'         => 'required|string',
              'last_name'           => 'required|string',
              'company_name'        => 'required|string',
              'designation'         => 'required|string',
              'cnic'                => 'required|string',
             //'password'          => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
              'password'            => 'required|string|min:4|confirmed',
              'email'              => [
                                        'nullable',
                                        'email',
                                        Rule::unique('users')->where(function($query) use($request) {
                                        $query->whereNotNull('email')->where('id','!=',auth()->user()->id);
                                       })
              ],
              'user_type'           => 'required|string',
              'otp_code'            => 'required|numeric',
              'phone_number'        => 'required',
               ]);
  
               return $validator->errors()->toJson();           
          }
        }

        public function packageEmail(Request $request){
          $request->validate([
                 'package_id'       => 'required',
             ]);

             /*$to_name = 'nabeel';
                $to_email = 'mnabeel992@gmail.com';
                $data = array('name'=>"Sam Jose", "body" => "Test mail");
                Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject('Artisans Web Testing Mail');
                $message->from('test@abc.instalment.pk','Artisans Web');
               });*/  

               return response()->json([
               'status'    => true,
                ]);      
         }

        
      
}