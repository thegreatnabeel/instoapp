<?php

namespace App\Http\Controllers\API\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule; 
use App\User;
use App\Customer;
use App\Otp;
use App\Helper\SMSUtils;
use \SoapClient;
use Hash;
use App\VendorSubArea;
use App\VendorPackage;
use App\VendorSubCatagory;
use App\SubArea;
use App\City;
use App\Catagory;
use App\Area;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Get the personal client name for Passport oAuth login.
     *
     * @return string
     */
    public function personalClientName()
    {
        return 'Personal Grant - Mobile Apps';
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if(!in_array($user->user_type, ['client']))
        {
            return abort(403, "You're not eligible to use this App.");
        }
        elseif($user->status != 'active')
        {
            if($user->status == 'pending')
                return abort(423, "Authentication pending yet.");
            elseif($user->status == 'deactive')
                return abort(423, "Your account is not active. Please contact administration.");
            elseif($user->status == 'suspended')
                return abort(423, "Your account is suspended. Please contact administration.");
            else
                return abort(423, "Your account is not accessible for now. Please try again shortly.");
        }

        $token          = $user->createToken($this->personalClientName(), ['*']);
        $user->token    = $token->accessToken;
        $user->userType;

        return $user;
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        \Auth::user()->fcm_token    = null;
        \Auth::user()->save();
        
        return $this->loggedOut($request);
    }

    public function resetcheck(Request $request)
    {
        $status="false";
        $request->validate([
            'email' => 'required|email'
        ]);
          $getEmail= user::where('email',$request->email)->first();
          if($getEmail){
            $status="true";
          }

        return $status;
    }


    public function displayPhoto(Request $request)
    {
        $request->validate([
            'picture'  => 'required|image|max:5120'
        ]);

        $imageName = null;
        if($request->hasFile('picture'))
        {
            $imageName = $request->file('picture')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'profile_images', $request->file('picture'), $imageName
            );
        }
        else
        {
            $imageName = null;
        }
        if($imageName!=null){
        \Auth::user()->userType->picture_path = $imageName;
        \Auth::user()->userType->save();     
        }
        return Auth()->user();
    }

    public function registerFCM(Request $request)
    {
        $request->validate([
            'fcm_token' => 'required'
        ]);

        \Auth::user()->fcm_token = $request->fcm_token;
        \Auth::user()->save();

        return;
    }


    public function sendOTP(Request $request){
         $request->validate([
           'otp_name'  => 'required|numeric|digits_between:11,12',
           'type'      => 'required|string',
           'call_type' => 'required|string'
        ]);
        $PNumber = ltrim($request->otp_name, '0');
            $PNumber='92'.$PNumber;
         if($request->call_type=='forget'){
            $getotp=Otp::where('phone_number',$PNumber)->where('type',$request->type)->where('status','complete')->first();
            if($getotp){
                 SMSUtils::sendSMSNew($PNumber, "Your verification pin code for instalment is ". $getotp->otp_code."");
                 return $getotp;
                }
                else{
                    return response()->json(['message' => "Please Register First"],404);     
                }
         }
        else if($request->call_type=='register'){
         $getotp=Otp::where('phone_number',$PNumber)->where('type',$request->type)->first();
         if($getotp){
            if($getotp->status == 'complete'){
                return $getotp;
               }
               else{
              SMSUtils::sendSMSNew($PNumber, "Your verification pin code for instalment is ". $getotp->otp_code."");
              return $getotp;
               }
         }
         else{
            $otpcode = $this->generateOTP();
         SMSUtils::sendSMSNew($PNumber, "Your verification pin code for instalment is ". $otpcode."");
         $otp = new Otp;
         $otp->type         = $request->type;
         $otp->phone_number = $PNumber;
         $otp->otp_code     = $otpcode;
         $otp->status       = "unverify";
         $otp->save();
          return $otp;
         }
        }
        else{
            return response()->json(['message' => "call type not match"],404);
        }
        }

        public function checkOTP(Request $request){
         $request->validate([
           'phone_number'  => 'required|numeric|digits_between:11,12',
           'check_otp' => 'required|numeric',
           'type'    =>'required|string',
           'call_type' => 'required|string'
        ]);
        $PNumber = ltrim($request->phone_number, '0');
        $PNumber='92'.$PNumber;
        if($request->call_type=='forget'){
        $getotp=Otp::where('phone_number',$PNumber)->where('otp_code',$request->check_otp)->where('type',$request->type)->first();
         if($getotp){
            return response()->json([
                'status'    => true,
                'getotp'    => $getotp
                 ]);
        }
        else{
            return response()->json([
                'status'    => false,
                'getotp'    => null
                 ]);
        }
       }
        else if($request->call_type=='register'){
         $getotp=Otp::where('phone_number',$PNumber)->where('otp_code',$request->check_otp)->where('type',$request->type)->first();
         if($getotp){
            $getotp->status = "verify";
            $getotp->save();
            return response()->json([
                'status'    => true,
                'getotp'    => $getotp
                 ]);
        }
        else{
            return response()->json([
                'status'    => false,
                'getotp'    => null
                 ]);
        }
        }
         else{
            return response()->json(['message' => "call type not match"],404);
         }
        }

        public function generateOTP(){
        return rand ( 1000 , 9999 );
        }

        public function customerregistration(Request $request){
           $request->validate([
           'first_name'         => 'required|string',
           'last_name'          => 'required|string',
           'base_location'      => 'required|string',
           //'password'           => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
           'password'           => 'required|string|min:4|confirmed',
           'email'              => [
                                      'nullable',
                                      'email',
                                      Rule::unique('users')->where(function($query) {
                                       $query->whereNotNull('email');
                                        })
                                    ],
           'user_type'          => 'required|string',
           'otp_code'           => 'required|numeric',
           'sub_area_id'        => 'required',
           'phone_number'       => 'required|numeric|digits_between:11,12',
           'picture_path'       => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
           'google_id'          => 'nullable',
           'facebook_id'        => 'nullable',
            ]);
            $PNumber = ltrim($request->phone_number, '0');
            $PNumber='92'.$PNumber;         
            $getuser = User::where('phone_number',$PNumber)->where('user_type','client')->first();
            if($getuser){
                return response()->json([ 'message' => "User Already Register"]);
              }
              else{
           $user =new User;
           $user->name          = $request->first_name.''.$request->last_name;
           $user->email         = $request->email =="" ? NULL : strtolower($request->email);
           $user->password      = !empty($request->password) ? bcrypt($request->password):null;
           $user->phone_number  = $PNumber;
           $user->google_id     = $request->google_id == ""? null : $request->google_id;
           $user->facebook_id   = $request->facebook_id == ""? null : $request->facebook_id; 
           $user->timezone      = $request->timezone;
           $user->user_type     = 'client';
           $user->status        = 'active'; 
           $userInfo            = $user->save();
           if($userInfo){
            $imageName=null;
            if($request->hasFile('picture_path'))
              {
            $imageName = $request->file('picture_path')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'customerimages', $request->file('picture_path'), $imageName
            );
            $imageName='storage/'.$path;
        }
        else
        {
            $imageName = null;
        }
             $customer = new Customer;
             $customer->user_id              = $user->id;
             $customer->first_name           = $request->first_name;   
             $customer->last_name            = $request->last_name;
             $customer->base_address         = $request->base_location;
             $customer->opt_code             = $request->otp_code;
             $customer->primary_phone_number = $PNumber;
             $customer->sub_area_id          = $request->sub_area_id;
             $customer->verfication_status   = 'verified';
             $customer->register_type        = 'instoapp';
             $customer->picture_path         = $imageName;
             $customer->status               = true;
             $customer->save();
             $getotp=Otp::where('phone_number',$PNumber)->where('type','client')->first();
             if($getotp){
                 $getotp->status = 'complete';
                 $getotp->save();
             }
           }
           \Auth::login($user);
           $token           = $user->createToken($this->personalClientName(), ['*']);
            $user->token    = $token->accessToken;
            $user->userType;
            $user->sub_area = SubArea::where('id',$user->userType->sub_area_id)->first();
            $user->city = City::whereHas('area.subarea',function($q) use($user){
                $q->where('id',$user->userType->sub_area_id);})->first();
           return $user;
        }
    }

        public function facebooklogin(Request $request){
            $request->validate([
           'facebook_id' => 'required',
           'type'        => 'required',
          ]);
            $finduser = User::where('facebook_id', $request->facebook_id)->
                               where('user_type',$request->type)->first();
            if($finduser){
                \Auth::login($finduser);
                $token           = $finduser->createToken($this->personalClientName(), ['*']);
                $finduser->token    = $token->accessToken;
                $finduser->userType;
                if($finduser->user_type=="vendor"){
                    $finduser->sub_area = VendorSubArea::with('SubArea.AreaId')->where('vendor_id',$finduser->userType->id)->get();
                    $subareaids = VendorSubArea::where('vendor_id',$finduser->userType->id)->pluck('sub_area_id');
                    $finduser->area = Area::whereHas('subarea', function($q) use($subareaids){
                        $q->whereIn('id',$subareaids);})->get();
                    $areaids = Area::whereHas('subarea', function($q) use($subareaids){
                            $q->whereIn('id',$subareaids);})->pluck('id');
    
                    $finduser->city = City::whereHas('area', function($q) use($areaids){
                            $q->whereIn('id',$areaids);})->get();
    
                    $finduser->vendor_package = VendorPackage::where('vendor_id',$finduser->userType->id)->get();
                    $finduser->sub_category = VendorSubCatagory::with('SubCategory.CategoryId')->where('vendor_id',$finduser->userType->id)->get();
                    $subcategoryids = VendorSubCatagory::where('vendor_id',$finduser->userType->id)->pluck('sub_catagory_id');
                    $finduser->category = Catagory::whereHas('SubCatagory', function($q) use($subcategoryids){
                                                    $q->whereIn('id',$subcategoryids);})->get();
                    return $finduser;
                    }
                    else if($finduser->user_type=="client"){
                    $finduser->sub_area = SubArea::where('id',$finduser->userType->sub_area_id)->first();
                    $finduser->city = City::whereHas('area.subarea',function($q) use($finduser){
                        $q->where('id',$finduser->userType->sub_area_id);})->first();
                    return $finduser;
                    }
                    else{
                        return $finduser;
                    }
            }
            else{
                return response()->json(['message' => "Not Found"],404);
            }
            
        }

        public function googlelogin(Request $request){
            $request->validate([
           'google_id' => 'required',
           'type'      => 'required', 
          ]);
            $finduser = User::where('google_id', $request->google_id)->
                              where('user_type',$request->type)->first();
            if($finduser){
                \Auth::login($finduser);
                $token           = $finduser->createToken($this->personalClientName(), ['*']);
                $finduser->token    = $token->accessToken;
                $finduser->userType;
                if($finduser->user_type=="vendor"){
                    $finduser->sub_area = VendorSubArea::with('SubArea.AreaId')->where('vendor_id',$finduser->userType->id)->get();
                    $subareaids = VendorSubArea::where('vendor_id',$finduser->userType->id)->pluck('sub_area_id');
                    $finduser->area = Area::whereHas('subarea', function($q) use($subareaids){
                        $q->whereIn('id',$subareaids);})->get();
                    $areaids = Area::whereHas('subarea', function($q) use($subareaids){
                            $q->whereIn('id',$subareaids);})->pluck('id');
    
                    $finduser->city = City::whereHas('area', function($q) use($areaids){
                            $q->whereIn('id',$areaids);})->get();
    
                    $finduser->vendor_package = VendorPackage::where('vendor_id',$finduser->userType->id)->get();
                    $finduser->sub_category = VendorSubCatagory::with('SubCategory.CategoryId')->where('vendor_id',$finduser->userType->id)->get();
                    $subcategoryids = VendorSubCatagory::where('vendor_id',$finduser->userType->id)->pluck('sub_catagory_id');
                    $finduser->category = Catagory::whereHas('SubCatagory', function($q) use($subcategoryids){
                                                    $q->whereIn('id',$subcategoryids);})->get();
                    return $finduser;
                    }
                    else if($finduser->user_type=="client"){
                    $finduser->sub_area = SubArea::where('id',$finduser->userType->sub_area_id)->first();
                    $finduser->city = City::whereHas('area.subarea',function($q) use($finduser){
                        $q->where('id',$finduser->userType->sub_area_id);})->first();
                    return $finduser;
                    }
                    else{
                        return $finduser;
                    }
            }
            else{
                return response()->json(['message' => "Not Found"],404);
            }
        }

        public function loginbynumber(Request $request){
            $request->validate([
           'phone_number' => 'required|numeric|digits_between:11,12',
           //'password'     => 'required|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
           'password'     => 'required|string|min:4',
           'type'         => 'required|string', 
          ]);
            $PNumber = ltrim($request->phone_number, '0');
            $PNumber='92'.$PNumber;
            $finduser = User::where('phone_number', $PNumber)->
                              where('user_type',$request->type)->first();  
            if($finduser){
                if(!Hash::check($request->password, $finduser->password)){
                    return response()->json(['message' => "Password not match"],404);
                }
                else{
                    
                \Auth::login($finduser);
                $token           = $finduser->createToken($this->personalClientName(), ['*']);
                $finduser->token    = $token->accessToken;
                $finduser->userType;
                if($finduser->user_type=="vendor"){
                $finduser->sub_area = VendorSubArea::with('SubArea.AreaId')->where('vendor_id',$finduser->userType->id)->get();
                $subareaids = VendorSubArea::where('vendor_id',$finduser->userType->id)->pluck('sub_area_id');
                $finduser->area = Area::whereHas('subarea', function($q) use($subareaids){
                    $q->whereIn('id',$subareaids);})->get();
                $areaids = Area::whereHas('subarea', function($q) use($subareaids){
                        $q->whereIn('id',$subareaids);})->pluck('id');

                $finduser->city = City::whereHas('area', function($q) use($areaids){
                        $q->whereIn('id',$areaids);})->get();

                $finduser->vendor_package = VendorPackage::where('vendor_id',$finduser->userType->id)->get();
                $finduser->sub_category = VendorSubCatagory::with('SubCategory.CategoryId')->where('vendor_id',$finduser->userType->id)->get();
                $subcategoryids = VendorSubCatagory::where('vendor_id',$finduser->userType->id)->pluck('sub_catagory_id');
                $finduser->category = Catagory::whereHas('SubCatagory', function($q) use($subcategoryids){
                                                $q->whereIn('id',$subcategoryids);})->get();
                return $finduser;
                }
                else if($finduser->user_type=="client"){
                $finduser->sub_area = SubArea::where('id',$finduser->userType->sub_area_id)->first();
                $finduser->city = City::whereHas('area.subarea',function($q) use($finduser){
                                             $q->where('id',$finduser->userType->sub_area_id);
                                        })->first();
                return $finduser;
                }
                else{
                    return $finduser;
                }
                
              }
            }
            else{
                return response()->json(['message' => "Not Found"],404);
            }
        }

        public function forgetpasswordbynumber(Request $request){
            $request->validate([
           'phone_number' => 'required|numeric|digits_between:11,12',
           //'password'     => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
           'password'     => 'required|string|min:4|confirmed',
           'type'         => 'required|string', 
          ]);
          $PNumber = ltrim($request->phone_number, '0');
          $PNumber='92'.$PNumber;
            $finduser = User::where('phone_number', $PNumber)->
                              where('user_type',$request->type)->first();
            if($finduser){
                $finduser->password = !empty($request->password) ? bcrypt($request->password):null;
                $finduser->save();
                return response()->json(['status' => true , 'message' => "Password change successfully!"]);
            }
            else{
                return response()->json(['status' => false , 'message' => "Not Found"],404);
            }
        }


        public function customerprofileupdate(Request $request){
            $request->validate([
            'first_name'         => 'required|string',
            'last_name'          => 'required|string',
            'base_location'      => 'required|string',
            'email'              => [
                                       'nullable',
                                       'email',
                                       Rule::unique('users')->where(function($query) use($request) {
                                        $query->whereNotNull('email')->where('email','==',$request->emial);
                                         })
                                     ],
            'sub_area_id'        => 'required',
            'picture_path'       => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
             ]);

             \Auth::user()->name         = $request->first_name.' '.$request->last_name;
             \Auth::user()->email        = ($request->email == "") ? auth()->user()->email : strtolower($request->email);
             \Auth::user()->password     = !empty($request->password) ? bcrypt($request->password) : \Auth::user()->password;
             \Auth::user()->save();

             $user = \Auth::user()->userType;
             $imageName=null;
             if($request->hasFile('picture_path'))
               {
             $imageName = $request->file('picture_path')->getClientOriginalName();
             $imageName = str_replace(" ","",$imageName);
             $imageName = time().'_'.$imageName;
 
             $path = Storage::disk('public')->putFileAs(
                 'customerimages', $request->file('picture_path'), $imageName
             );
             $imageName='storage/'.$path;
            }
            else
            {
             $imageName = null;
            }
              $user->first_name           = $request->first_name;   
              $user->last_name            = $request->last_name;
              $user->base_address         = $request->base_location;
              $user->sub_area_id          = $request->sub_area_id;
              $user->picture_path         = $imageName == null ? $user->picture_path : $imageName;
              $user->save();
              \Auth::user()->sub_area = SubArea::where('id',$user->sub_area_id)->first();
              \Auth::user()->city  = City::whereHas('area.subarea',function($q) use($user){
                    $q->where('id',$user->sub_area_id);})->first();
            
        return Auth()->user();
     }

     public function changepassword(Request $request){
        $request->validate([
        'old_password' => 'required|string|min:4',
        'password'     => 'required|string|min:4|confirmed',
        'type'         => 'required|string'
        ]);
        if(!Hash::check($request->old_password, \Auth::user()->password)){
            return response()->json(['status' => false,'message' => "Your old password was not correct"]);
        }
        else{
        \Auth::user()->password     = !empty($request->password) ? bcrypt($request->password) : \Auth::user()->password;
        \Auth::user()->save();
        return response()->json(['status' => true,'message' => "password change successfully"]);
        }
     }


     public function register_fcm(Request $request){
        $request->validate([
            'fcm_token' => 'required'
            ]);
            \Auth::user()->fcm_token    = $request->fcm_token;
            \Auth::user()->save();
            return response()->json(['status' => true,'message' => "Token Save Successfully"]);
     }
}