<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Helper\SMSUtils;
use \SoapClient;
Use Redirect;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
      if(in_array($user->user_type, ['client','vendor']))
      {
        Auth::logout();
        return abort(403, "You're not admin.");
      }
      else{
        return Redirect::to('/admin/home');
      }
        
    }

    public function sendOTP(Request $request){
        //dd(phpinfo());
        //$request->validate([
         //   'phone_number' => 'required|number',
        //]);
        //return $request;
        //$customer = Models\Customers::where('primary_phone', $this->input->post('phone_number'))->first();

        //if ($this->form_validation->run() === true) {
           // if (empty($customer)) {
               // $phone_number = ltrim($this->input->post('phone_number'), '+');
                $otp = $this->generateOTP();
                //return $otp; 
                SMSUtils::sendSMSNew($request->optname, "Your verification pin code for instalment is ". $otp."");


                //$user = new Application\Models\Users;
                //$customer = new Application\Models\Customers;

                //$customer->primary_phone = $this->input->post('phone_number');
               //$customer->save();

                //$user->phone_number = $this->input->post('phone_number');
               // $user->customer_id = $customer->id;
               // $user->access_level_bits = _CUSTOMER_;
               // $user->user_type = 'customer';
               // $user->otp = $otp;
                //$user->save();


                //$response = array('status' => 'success', 'phone_number' => substr($this->input->post('phone_number'),3), 'customer_id' => $customer->id, 'message' => 'Verification Code has been send on your number.');
                //header('Content-Type: application/json');
               // echo json_encode($response);

            //} else {
                //update otp in users table and send again to user
               // $phone_number = ltrim($this->input->post('phone_number'), '+');
               // sendSMSNew($phone_number, "Your verification pin code for instalment is ". $customer->user->otp ."");

              //  $response = array('status' => 'success', 'phone_number' => substr($this->input->post('phone_number'),3), 'customer_id' => $customer->id, 'message' => 'Verification Code has been send on your number.');
               // header('Content-Type: application/json');
               // echo json_encode($response);
           // }
        //}else{
           // $response = array('status' => 'fail', 'errors' => validation_errors());
            //header('Content-Type: application/json');
            //echo json_encode( $response );
            return redirect(route('login'))->with(['status' => "success", 'message' => "Airport Added Successfully!"]);
        }


    public function generateOTP(){

        return rand ( 1000 , 9999 );
    }
}
