<?php

// FacebookloginController.php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Auth;
Use Redirect;

class FacebookLoginController extends Controller
{
/**
* Create a redirect method to facebook api.
*
* @return void@@
*/
public function redirect($provider)
{
return Socialite::driver($provider)->redirect();
}
/**
* Return a callback method from facebook api.
*
* @return callback URL from facebook
*/
public function callback($provider)
{
  $userSocial = Socialite::driver($provider)->stateless()->user();
  if($provider=='facebook'){
    $finduser = User::where('facebook_id', $userSocial->id)->first();
  }
  else{
    $finduser = User::where('google_id', $userSocial->id)->first();
  }
  //return $userSocial->email;
      if($finduser){
      Auth::login($finduser);
      return Redirect::to('admin/home');
  }else{
      //dd( $userSocial->id);
        $new_user =new User;
        
        if($provider=='facebook'){
          $checkEmail=User::where('email',$userSocial->email)->first();
          if($checkEmail)
           {
             return Redirect::to('admin/login')->with([
            'status' => 'error',
            'message' => 'Email already exist.'
           ]);
          }
          else{
          $new_user->name      = $userSocial->name;
          $new_user->email       = $userSocial->email;
          $new_user->user_type   = 'Client';
          $new_user->facebook_id = $userSocial->id;
          $new_user->status      = 'active';
          $new_user->save();
         }
        }
        else{
          
          $checkEmail=User::where('email',$userSocial->email)->first();
          if($checkEmail)
           {
             return Redirect::to('admin/login')->with([
            'status' => 'error',
            'message' => 'Email already exist.'
           ]);
          }
          else{
        $new_user->name       = $userSocial->name;
        $new_user->email       = $userSocial->email;
        $new_user->user_type   = 'Client';
        $new_user->google_id   = $userSocial->id;
        $new_user->status      = 'active';
        $new_user->save();
        }
      }

        
    
    Auth::login($new_user);
    return Redirect::to('admin/home');
  }
 }
}
