<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;
use App\SubCatagory;
use App\OffereDetail;
use App\City;
use App\Product;
use App\Brand;
use App\Cms;
use App\WishList;
use App\OffereMaster;
use Session;
use DB;
use Auth;
class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offere_count =[];
        $get_all_offere_detail =[];
        $wishlist_cms = null;
        $city_id = 1;
         if(Auth::check()==true && Auth::user()->user_type == "client"){
             $sub_area_id = Auth::user()->userType->sub_area_id;
            $getcity = City::whereHas('area.subarea',function($q) use($sub_area_id){
                             $q->where('id',$sub_area_id);
                              })->first();
            $city_id = $getcity->id;
        }
        else{
            $city_id =1;
        }
        $get_category         = Catagory::has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });
        $get_wishlist        = WishList::where('customer_id',Auth::user()->userType->id)->get();
        $get_wishlist->each(function($wishlist, $key) use($get_wishlist,$city_id) {
        $get_wishlist[$key]->get_all_offere_detail  = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.vendor'])
                                                                     ->where('id',$wishlist->offere_id)->first();
        });
        $wishlist_cms = Cms::where('page_title','WishList')->first();

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.wishlist',compact('get_category','get_wishlist','wishlist_cms','get_top_product_category','get_top_product_brand'));
    }

    public function adddeletewishlist(Request $request){
        $status = 0;
        $errMsg = "";
        $getwishlistcount =0;
       $get_wish_list = WishList::where('offere_id',$request->offere_id)->where('customer_id',Auth::user()->userType->id)->first();
       if($get_wish_list){
        $get_wish_list->delete();
        $status = 1;
        $errMsg = "Ofere Remove Successfully";
        $getwishlistcount = WishList::where('customer_id',Auth::user()->userType->id)->count();
          if($getwishlistcount){
             Session::put('wishlist_count',$getwishlistcount);
            }
            else{
            Session::put('wishlist_count',0);
            $getwishlistcount = 0;
           }
       }   
       else{
        $get_offere_detail_for_cash = OffereDetail::where('id',$request->offere_id)->first();
           $wish_list = new WishList();
           $wish_list->offere_id   = $request->offere_id;
           $wish_list->customer_id = Auth::user()->userType->id;
           $wish_list->cash_price  = $get_offere_detail_for_cash->cash_price;
           $wish_list->save();
           $status = 2;
           $errMsg = "Ofere Add Successfully";
           $getwishlistcount = WishList::where('customer_id',Auth::user()->userType->id)->count();
           if($getwishlistcount){
            Session::put('wishlist_count',$getwishlistcount);
           }
           else{
           Session::put('wishlist_count',0);
           $getwishlistcount = 0;
          }
       }
       return response()->json( array('status' => $status, 'errMsg' => $errMsg,'getwishlistcount' =>$getwishlistcount));      
    }
}
