<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Validator;
use App\Cms;
use App\Catagory;
use App\SubCatagory;
use App\User;
use App\Customer;
use App\Otp;
use App\City;
use App\SubArea;
use App\Helper\SMSUtils;
use \SoapClient;
use Hash;
use App\Product;
use Session;
use Auth;
use DB;
Use Redirect;
use App\WishList;
class ForgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Session::has('forget')){
            //Session::forget('forget');
        }
        else{
            Session::put('forget',['divname' => 'phoneField', 'phone_number' => 0]);    
        }
        $forget_cms = null;
        $get_category         = Catagory::has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        
        return view('web.forget',compact('forget_cms','get_category','get_top_product_category','get_top_product_brand'));
    }
    
    public function forgetsendotp(Request $request){
        $status = 0;
        $errMsg='';
        $validator = Validator::make($request->all(), [
        'phone_number' => 'required|numeric|digits_between:11,12',
         ]);
        if($validator->fails()){
            $status =2;
            $errMsg = $validator->getMessageBag()->toArray();
        }
        else{
            $PNumber = ltrim($request->phone_number, '0');
            $PNumber='92'.$PNumber;
            $getotp=Otp::where('phone_number',$PNumber)->where('type','client')->first();
           if($getotp){
              SMSUtils::sendSMSNew($PNumber, "Your verification pin code for instalment is ". $getotp->otp_code."");
                $status =1;
          }
        }
         return response()->json( array('status' => $status, 'errMsg' => $errMsg));
    }

    public function forgetcheckotp(Request $request){
        $status =false;
        $PNumber = ltrim($request->phone_number, '0');
        $PNumber='92'.$PNumber;
        $getotp=Otp::where('phone_number',$PNumber)->where('otp_code',$request->otp)->where('type','client')->first();
        if($getotp){
           $status = true;
        }
        return response()->json( array('status' => $status));
       }
       public function customerforgetPassword(Request $request){
        $validator = Validator::make($request->all(), [
        //'password'           => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        'password'     => 'required|string|min:4|confirmed',
         ]);
         if($validator->fails()){
            Session::put('forget',['divname' => 'forgetField', 'phone_number' => $request->hiddenphone]);
            return redirect()->route('web.forget')->withErrors($validator)->withInput();
         }
         else{
            $PNumber = ltrim($request->hiddenphone, '0');
            $PNumber='92'.$PNumber; 
            Session::put('forget',['divname' => 'phoneField', 'phone_number' => 0]);
            $getuser = User::where('phone_number',$PNumber)->where('user_type','client')->first();
            if($getuser){
           $getuser->password      = !empty($request->password) ? bcrypt($request->password):null;
           $getuser->save();
           return redirect()->route('web.login')->with([
            'status' => 'success',
            'message' => 'Password Changed Successfully'
        ]);
            }
            else{
                return redirect()->route('web.forget')->withInput();
            }
                
        }
    }
}
