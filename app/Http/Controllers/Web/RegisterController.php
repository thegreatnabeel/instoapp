<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Validator;
use App\Cms;
use App\Catagory;
use App\SubCatagory;
use App\User;
use App\Customer;
use App\Otp;
use App\City;
use App\SubArea;
use App\Helper\SMSUtils;
use \SoapClient;
use Hash;
use Session;
use Auth;
Use Redirect;
use App\WishList;
use App\Product;
use DB;
class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Session::has('register')){
            //Session::forget('register');
        }
        else{
            Session::put('register',['divname' => 'phoneField', 'phone_number' => 0, 'opt' => 0]);    
        }
        $register_cms = null;
        $cities = City::all();
        $get_category         = Catagory::has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.signup',compact('register_cms','get_category','cities','get_top_product_category','get_top_product_brand'));
    }
    
    public function sendotp(Request $request){
        $status = 0;
        $errMsg='';
        $validator = Validator::make($request->all(), [
        'phone_number' => 'required|numeric|digits_between:11,12',
         ]);
        if($validator->fails()){
            $status =2;
            $errMsg = $validator->getMessageBag()->toArray();
        }
        else{
            $PNumber = ltrim($request->phone_number, '0');
            $PNumber='92'.$PNumber;
            $getotp=Otp::where('phone_number',$PNumber)->where('type','client')->first();
           if($getotp){
               if($getotp->status == 'complete'){
                $status =3;
                $errMsg = "Number Already used please try login";
               }
               else{
              SMSUtils::sendSMSNew($PNumber, "Your verification pin code for instalment is ". $getotp->otp_code."");
                $status =1;
               }
          }
          else{
             $otpcode = $this->generateOTP();
        SMSUtils::sendSMSNew($PNumber, "Your verification pin code for instalment is ". $otpcode."");
          $otp = new Otp;
          $otp->type         = 'client';
          $otp->phone_number = $PNumber;
          $otp->otp_code     = $otpcode;
          $otp->status       = "unverify";
          $otp->save();
          $status =1;
          }
        }
         return response()->json( array('status' => $status, 'errMsg' => $errMsg));
    }

    public function checkotp(Request $request){
        $status =false;
        $PNumber = ltrim($request->phone_number, '0');
        $PNumber='92'.$PNumber;
        $getotp=Otp::where('phone_number',$PNumber)->where('otp_code',$request->otp)->where('type','client')->first();
        if($getotp){
           $getotp->status = "verify";
           $getotp->save();
           $status = true;
        }
        return response()->json( array('status' => $status));
       }

       public function generateOTP(){
       return rand ( 1000 , 9999 );
       }

       public function customerregistration(Request $request){
        $validator = Validator::make($request->all(), [
        'first_name'         => 'required|string',
        'last_name'          => 'required|string',
        'base_location'      => 'required|string',
        //'password'           => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        'password'     => 'required|string|min:4|confirmed',
        'email'              => [
                                   'nullable',
                                   'email',
                                   Rule::unique('users')->where(function($query) {
                                    $query->whereNotNull('email');
                                     })
                                 ],
        'sub_area_id'        => 'required',
        'picture_path'       => 'image|mimes:png,jpg,jpeg|max:5120|required',
         ]);
         if($validator->fails()){
            Session::put('register',['divname' => 'registerField', 'phone_number' => $request->hiddenphone, 'opt' => $request->hiddenotp]);
            return redirect()->route('signup')->withErrors($validator)->withInput();
         }
         else{
            $PNumber = ltrim($request->hiddenphone, '0');
            $PNumber='92'.$PNumber; 
            Session::put('register',['divname' => 'phoneField', 'phone_number' => 0, 'opt' => 0]);
            $getuser = User::where('phone_number',$PNumber)->where('user_type','client')->first();
            if($getuser){
                return redirect()->route('web.login')->with([ 'status' => 'error','message' => "User Already Register"]);
              }
              else{
           $user =new User;
           $user->name          = $request->first_name.''.$request->last_name;
           $user->email         = $request->email =="" ? NULL : strtolower($request->email);
           $user->password      = !empty($request->password) ? bcrypt($request->password):null;
           $user->phone_number  = $PNumber;
           $user->timezone      = $request->timezone;
           $user->user_type     = 'client';
           $user->status        = 'deactive'; 
           $userInfo            = $user->save();
           if($userInfo){
            $imageName=null;
            if($request->hasFile('picture_path'))
              {
                $imageName = $request->file('picture_path')->getClientOriginalName();
                $imageName = time().'_'.$imageName;
   
                 $path = Storage::disk('public')->putFileAs(
                'customerimages', $request->file('picture_path'), $imageName
                 );
                $imageName='storage/'.$path;
               }
            else
              {
               $imageName = null;
              }
             $customer = new Customer;
             $customer->user_id              = $user->id;
             $customer->first_name           = $request->first_name;   
             $customer->last_name            = $request->last_name;
             $customer->base_address         = $request->base_location;
             $customer->opt_code             = $request->hiddenotp;
             $customer->primary_phone_number = $PNumber;
             $customer->sub_area_id          = $request->sub_area_id;
             $customer->verfication_status   = 'verified';
             $customer->register_type        = 'instalment';
             $customer->picture_path         = $imageName;
             $customer->status               = true;
             $customer->save();
             $getotp=Otp::where('phone_number',$PNumber)->where('type','client')->first();
             if($getotp){
                 $getotp->status = 'complete';
                 $getotp->save();
             }
           }
           \Auth::login($user);
           $sub_area_id = Auth::user()->userType->sub_area_id;
            $getcity = City::whereHas('area.subarea',function($q) use($sub_area_id){
                             $q->where('id',$sub_area_id);
                              })->first();
                if($getcity){
                    Session::put('city',['city_id' => $getcity->id, 'city_name' => $getcity->name]);
                }
                else{
                    Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
                }
                $getwishlistcount = WishList::where('customer_id',Auth::user()->userType->id)->count();
                if($getwishlistcount){
                    Session::put('wishlist_count',$getwishlistcount);
                }
                else{
                    Session::put('wishlist_count',0);
                }
                return Redirect::to(''.Session::get('url').'')->with([
                    'status' => 'success',
                    'message' => 'welocme to instalment'
                ]);
        }
    }
 }


 public function customerprofileupdate(Request $request){
    $validator = Validator::make($request->all(), [
    'first_name'         => 'required|string',
    'last_name'          => 'required|string',
    'base_location'      => 'required|string',
    //'password'           => 'nullable|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
    'password'     => 'required|string|min:4|confirmed',
    'email'              => [
                               'nullable',
                               'email',
                               Rule::unique('users')->where(function($query) {
                                $query->whereNotNull('email')->where('id','=',Auth::user()->userType->id);
                                 })
                             ],
    'sub_area_id'        => 'required',
    'picture_path'       => 'image|mimes:png,jpg,jpeg|max:5120|required',
     ]);
     if($validator->fails()){
        Session::put('checkdashboard','account');
        return redirect()->route('web.dashboard')->withErrors($validator);
     }
     else{
        Session::put('checkdashboard','dashboard');
       $user = User::where('id',Auth::user()->id)->first();
       $user->name          = $request->first_name.''.$request->last_name;
       $user->email         = $request->email =="" ? NULL : strtolower($request->email);
       $user->password      = !empty($request->password) ? bcrypt($request->password):$user->password;
       $user->timezone      = $request->timezone;
       $user->user_type     = 'client';
       $user->status        = 'active'; 
       $userInfo            = $user->save();
        $imageName=null;
        if($request->hasFile('picture_path'))
          {
            $imageName = $request->file('picture_path')->getClientOriginalName();
            $imageName = time().'_'.$imageName;

             $path = Storage::disk('public')->putFileAs(
            'customerimages', $request->file('picture_path'), $imageName
             );
            $imageName='storage/'.$path;
           }
        else
          {
           $imageName = null;
          }
         $customer = Customer::where('id',Auth::user()->userType->id)->first();
         $customer->first_name           = $request->first_name;   
         $customer->last_name            = $request->last_name;
         $customer->base_address         = $request->base_location;
         $customer->sub_area_id          = $request->sub_area_id;
         $customer->verfication_status   = 'verified';
         $customer->register_type        = 'instalment';
         $customer->picture_path         = $imageName;
         $customer->status               = true;
         $customer->save();
       $sub_area_id = $customer->sub_area_id;
        $getcity = City::whereHas('area.subarea',function($q) use($sub_area_id){
                         $q->where('id',$sub_area_id);
                          })->first();
            if($getcity){
                Session::put('city',['city_id' => $getcity->id, 'city_name' => $getcity->name]);
            }
            else{
                Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
            }
            return redirect()->route('web.dashboard')->with([
                'status' => 'success',
                'message' => 'User Profile update successfully'
            ]);
    }
}

 public function subarea(Request $request)
 {
     $a=$request->data;
     $sub_areas=SubArea::whereHas('area',function($q) use ($a){
                         $q->where('city_id',$a);
                         })->get();
    return response()->json( array('status' => 'success','sub_areas'=>$sub_areas));
 }
}
