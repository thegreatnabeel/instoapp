<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Helper\SMSUtils;
use \SoapClient;
Use Redirect;
use Auth;
use App\Cms;
use App\Catagory;
use App\SubCatagory;
use App\User;
use App\WishList;
use App\City;
use Hash;
use App\Product;
use DB;
use Session;
class LoginController extends Controller
{

    use AuthenticatesUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function index()
    {
        if(Session::has('Logintabcheck')){
        }
        else{
            Session::put('Logintabcheck','withPhone');    
        }
        $login_cms = null;
        $get_category         = Catagory::has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.login',compact('login_cms','get_category','get_top_product_category','get_top_product_brand'));
    }

    public function login(Request $request){
        Session::put('Logintabcheck','withPhone');
        $request->validate([
        //'password'           => 'required|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        'password'     => 'required|string|min:4',
        'phone_number'       => 'required|numeric|digits_between:11,12',
         ]);
         $PNumber = ltrim($request->phone_number, '0');
            $PNumber='92'.$PNumber;
            $finduser = User::where('phone_number', $PNumber)->
                              where('user_type','client')->first();
            if($finduser){
               if(!Hash::check($request->password, $finduser->password)){
                return redirect()->route('web.login')->with([
                    'status' => 'error',
                    'message' => 'Password not match'
                ]);
                }
                else{
                \Auth::login($finduser);
                $finduser->fcm_token = $request->device_token1;
                $finduser->save();
                $sub_area_id = Auth::user()->userType->sub_area_id;
                $getcity = City::whereHas('area.subarea',function($q) use($sub_area_id){
                             $q->where('id',$sub_area_id);
                              })->first();
                if($getcity){
                    Session::put('city',['city_id' => $getcity->id, 'city_name' => $getcity->name]);
                }
                else{
                    Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
                }
                $getwishlistcount = WishList::where('customer_id',Auth::user()->userType->id)->count();
                if($getwishlistcount){
                    Session::put('wishlist_count',$getwishlistcount);
                }
                else{
                    Session::put('wishlist_count',0);
                }
                Session::forget('Logintabcheck');
                return Redirect::to(''.Session::get('url').'')->with([
                    'status' => 'success',
                    'message' => 'welocme to instalment'
                ]);
                }
            }
            else{
                Session::forget('Logintabcheck');
                return redirect()->route('web.login')->with([
                    'status' => 'error',
                    'message' => 'user not exist'
                ]);
            }
     }

     public function loginbyemail(Request $request){
        Session::put('Logintabcheck','withEmail');
        $request->validate([
        //'password1'           => 'required|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        'password'     => 'required|string|min:4|confirmed',
        'email'              => 'required|email',
         ]);
            $finduser = User::where('email', $request->email)->
                              where('user_type','client')->first();
            if($finduser){
               if(!Hash::check($request->password1, $finduser->password)){
                return redirect()->route('web.login')->with([
                    'status' => 'error',
                    'message' => 'Password not match'
                ]);
                }
                else{
                    Session::forget('Logintabcheck');
                \Auth::login($finduser);
                $finduser->fcm_token = $request->device_token2;
                $finduser->save();
                $sub_area_id = Auth::user()->userType->sub_area_id;
                $getcity = City::whereHas('area.subarea',function($q) use($sub_area_id){
                             $q->where('id',$sub_area_id);
                              })->first();
                if($getcity){
                    Session::put('city',['city_id' => $getcity->id, 'city_name' => $getcity->name]);
                }
                else{
                    Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
                }
                $getwishlistcount = WishList::where('customer_id',Auth::user()->userType->id)->count();
                if($getwishlistcount){
                    Session::put('wishlist_count',$getwishlistcount);
                }
                else{
                    Session::put('wishlist_count',0);
                }
                return Redirect::to(''.Session::get('url').'')->with([
                    'status' => 'success',
                    'message' => 'welocme to instalment'
                ]);
                }
            }
            else{
                Session::forget('Logintabcheck');
                return redirect()->route('web.login')->with([
                    'status' => 'error',
                    'message' => 'user not exist'
                ]);
            }
     }
     public function logout(Request $request)
     {
         Auth::logout();
         Session::forget('url');
         return redirect()->route('web.home')->with([
            'status' => 'success',
            'message' => 'User Logout Successfully'
        ]);
     }

}
