<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;
use App\Vendor;
use App\SubCatagory;
use App\OffereDetail;
use App\City;
use App\Product;
use App\Cms;
use App\OffereMaster;
use App\WishList;
use Auth;
use DB;
use Session;
class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $get_vendor_product = [];
        $load_more_product = [];
        $last_product_id = 0;
        $city_id = 1;
        $customer_id = 0;
        $sub_area_id = 0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
            $sub_area_id = Auth::user()->userType->sub_area_id;
            $city_id = Session::get('city')['city_id'];
       }
       else{
           $city_id =Session::get('city') == null ? 1: Session::get('city')['city_id'];
           $customer_id = 0;
           $sub_area_id = 0;
       }
        $vendor = Vendor::where('id',$id)->first();
        $get_category         = Catagory::has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });

        $get_vendor_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])
                                        ->whereHas('OffereMaster.Vendor',function($q) use($vendor){$q->where('id',$vendor->id);})    
                                        ->orderby('post_instalment','desc')->limit(12)->get();
        if($sub_area_id == 0){                                
        $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
                                            
        $get_vendor_product->each(function($product, $key) use($get_vendor_product,$city_id,$currentdate) {
        $get_vendor_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
        });
        }
        else{
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                                ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                                ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
            
            $get_vendor_product->each(function($product, $key) use($get_vendor_product,$city_id,$currentdate,$sub_area_id) {
            $get_vendor_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
            }); 
        }                    
        $get_featured_product->each(function($offere, $key) use($get_featured_product,$customer_id) {
        $get_featured_product[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();
        });
        $vendor_cms = Cms::where('page_title','Vendor')->first();

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.vendor',compact('get_category','get_featured_product','get_vendor_product','vendor','load_more_product','vendor_cms','get_top_product_category','get_top_product_brand'));
    }

    public function loadfeatureandproductforvendorpage(Request $request){
        $get_vendor_product = [];
        $get_featured_product =[];
        $load_more_product = [];
        $vendor =[];
        $city_id = 0;
        $customer_id =0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
       }
       else{
           $customer_id = 0;
       }
        $vendor = Vendor::where('id',$request->vendor_id)->first();
        $getcityid = City::where('name',$request->city_name)->first();
        if($getcityid){
            $city_id = $getcityid->id;
            Session::put('city',['city_id' => $getcityid->id, 'city_name' => $getcityid->name]);
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
            $get_vendor_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])
                                            ->whereHas('OffereMaster.Vendor',function($q) use($vendor){$q->where('id',$vendor->id);})    
                                            ->orderby('post_instalment','desc')->limit(12)->get();
            $get_vendor_product->each(function($product, $key) use($get_vendor_product,$city_id,$currentdate) {
            $get_vendor_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                                                     });                    
        $get_featured_product->each(function($offere, $key) use($get_featured_product,$customer_id) {
        $get_featured_product[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();
        });
       }
        else{
            $get_vendor_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])
                                         ->whereHas('OffereMaster.Vendor',function($q) use($vendor){$q->where('id',$vendor->id);})    
                                         ->orderby('post_instalment','desc')->limit(12)->get();
            $get_vendor_product->each(function($product, $key) use($get_vendor_product,$city_id,$currentdate) {
            $get_vendor_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                                                     });                    
        Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
        }
        $returnfeatureHTML = view('web.vendor_page_partial_view.feature_deal',['get_featured_product'=> $get_featured_product])->render();
        $returnproductHTML = view('web.vendor_page_partial_view.product',['get_vendor_product' => $get_vendor_product  , 'load_more_product' =>$load_more_product ])->render();
        return response()->json( array('success' => true, 'featurehtml'=> $returnfeatureHTML, 'producthtml' => $returnproductHTML,'location_id'=>$city_id ) );
    }

    public function loadvendorwiseproduct(Request $request){
        $load_more_product = [];
        $get_vendor_product = [];
        $city_id = $request->city_id;
        $vendor_id = $request->vendor_id; 
        $currentdate = date('Y-m-d');
        $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                     ->whereHas('OffereMaster.Vendor',function($q) use($vendor_id){$q->where('id',$vendor_id);})                                
                                     ->whereNotIn('id',$request->product_id)->orderby('post_instalment','desc')->limit(12)->get();

        if(Auth::check()==true && Auth::user()->user_type == "client")
        {
            $load_more_product->each(function($product, $key) use($load_more_product,$city_id,$currentdate,$sub_area_id) {
                $load_more_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                     ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                     ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                         });
        }
        else{
            $load_more_product->each(function($product, $key) use($load_more_product,$city_id,$currentdate) {
                $load_more_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                     ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                     ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                         });
        }                                                 
        $returnproductHTML = view('web.vendor_page_partial_view.product',['load_more_product' => $load_more_product, 'get_vendor_product' => $get_vendor_product])->render();                               
        return response()->json( array('success' => true, 'producthtml' => $returnproductHTML) );
    }
}
