<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Catagory;
use App\SubCatagory;
use App\OffereDetail;
use App\Cms;
use App\City;
use App\Product;
use App\WishList;
use DB;
use Auth;
use Session;
use App\Notification;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $last_category_id=0;
        $offere_count =[];
        $offere_count1 =[];
        $city_id =1;
        $customer_id =0;
        $currentdate = date('Y-m-d');
        $sub_area_id = 0;
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
            $city_id = Session::get('city')['city_id'];
            $sub_area_id = Auth::user()->userType->sub_area_id;
            
       }
       else{
           $city_id =Session::get('city') == null ? 1: Session::get('city')['city_id'];
           $customer_id = 0;
           $sub_area_id = 0;
       }
        $get_bannar_category  = Catagory::where('show_home_page',true)
                                        ->has('Product')->limit(3)->get();
        $get_category         = Catagory::has('Product')
                                        ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });
        $get_category_product = Catagory::has('product')->limit(5)->get();
        if($sub_area_id == 0){
        $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
        $get_category_product->each(function($category, $key) use($get_category_product) {
        $get_category_product[$key]->gethotproduct  = Product::with(['ProductImage.ProductResizeImage'])->where('hot_product',true)->where('catagory_id',$category->id)->orderby('post_instalment','desc')->limit(8)->get();
        });
        foreach($get_category_product as $key => $category){
            foreach($category->gethotproduct as $key1 => $product){
                $get_category_product[$key]->gethotproduct[$key1]->getminoffere =OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                                    ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                                    ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
            }
        }
        }
        else{
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
            $get_category_product->each(function($category, $key) use($get_category_product) {
            $get_category_product[$key]->gethotproduct  = Product::with(['ProductImage','ProductImage.ProductResizeImage'])->where('hot_product',true)->where('catagory_id',$category->id)->orderby('post_instalment','desc')->limit(8)->get();
            });
        
        foreach($get_category_product as $key => $category){
            foreach($category->gethotproduct as $key1 => $product){
                $get_category_product[$key]->gethotproduct[$key1]->getminoffere =OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                                                                    ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                                    ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
            }
        }
        }

        $get_featured_product->each(function($offere, $key) use($get_featured_product,$customer_id) {
        $get_featured_product[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();
        });
        if(count($get_category_product) > 0){
            $last_category_id = $get_category_product->max('id');
        }
        $get_home_page_seo_description = Cms::where('page_title','Home')->first();
        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        return view('/web/home',compact('get_bannar_category','get_category','get_featured_product','get_category_product','get_home_page_seo_description','last_category_id','get_top_product_category','get_top_product_brand'));
    }


    public function websearchproduct(Request $request){
        $status = false;
        $errMsg = '';
        $get_product = Product::where(DB::raw("CONCAT(`product_name`, ' ', `info`, ' ', `info1`)"), 'LIKE', "%" . $request->search . "%")
                    ->orWhere('product_description','LIKE','%'.$request->search.'%')->limit(5)->get();
        if($get_product){
            $status = true;
        }
        return response()->json(['status' => $status,'errMsg'=>$errMsg,'get_product' => $get_product]);
    }

    public function loadfeatureandcategory(Request $request){
        $get_featured_product = [];
        $get_category_product = [];
        $get_category_product1 = [];
        $last_category_id = 0;
        $city_id =0;
        $customer_id =0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
    }
    else{
        $customer_id = 0;
    }
        $getcityid = City::whereRaw('LOWER(`name`) LIKE ? ',[trim(strtolower($request->city_name)).'%'])->first();
        if($getcityid){
            $city_id =$getcityid->id;
            Session::put('city',['city_id' => $getcityid->id, 'city_name' => $getcityid->name]);
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($getcityid){$q->where('cities.id',$getcityid->id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
            $get_category_product = Catagory::has('product')->limit(5)->get();
            $get_category_product->each(function($category, $key) use($get_category_product) {
                $get_category_product[$key]->gethotproduct  = Product::with(['ProductImage','ProductImage.ProductResizeImage'])->where('hot_product',true)->where('catagory_id',$category->id)->orderby('post_instalment','desc')->limit(8)->get();
                });
            foreach($get_category_product as $key => $category){
            foreach($category->gethotproduct as $key1 => $product){
                    $get_category_product[$key]->gethotproduct[$key1]->getminoffere =OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                                            ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                                            ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                }
            }

            $get_featured_product->each(function($offere, $key) use($get_featured_product,$customer_id) {
            $get_featured_product[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();
            });
            if(count($get_category_product) > 0){
            $last_category_id = $get_category_product->max('id');
            }
        }
        else{
            $get_category_product = Catagory::has('product')->limit(5)->get();
            $get_category_product->each(function($category, $key) use($get_category_product) {
                $get_category_product[$key]->gethotproduct  = Product::with(['ProductImage','ProductImage.ProductResizeImage'])->where('hot_product',true)->where('catagory_id',$category->id)->orderby('post_instalment','desc')->limit(8)->get();
                 });
            foreach($get_category_product as $key => $category){
              foreach($category->gethotproduct as $key1 => $product){
                    $get_category_product[$key]->gethotproduct[$key1]->getminoffere =OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                }
            }

            $get_featured_product->each(function($offere, $key) use($get_featured_product,$customer_id) {
            $get_featured_product[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();
            });
                                                                                                         
            if(count($get_category_product) > 0){
              $last_category_id = $get_category_product->max('id');
            
        }
        if($last_category_id==null){
            $last_category_id=0;
        }
        Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
     }
        $returnfeatureHTML = view('web.home_page_partial_view.feature_deal',['get_featured_product'=> $get_featured_product])->render();
        $returncategoryHTML = view('web.home_page_partial_view.category',['get_category_product' => $get_category_product])->render();
        return response()->json( array('success' => true, 'featurehtml'=> $returnfeatureHTML, 'categoryhtml' => $returncategoryHTML,'last_category_id'=> $last_category_id,'location_id'=>$city_id ) );
    }

    public function loadcategory(Request $request){
        $get_category_product = [];
        $city_id = $request->city_id; 
        $last_category_id = 0;
        $currentdate = date('Y-m-d');
        $get_category_product = Catagory::has('product')->where('id','>',$request->category_id)->limit(5)->get();
        $get_category_product->each(function($category, $key) use($get_category_product) {
            $get_category_product[$key]->gethotproduct  = Product::with(['ProductImage','ProductImage.ProductResizeImage'])->where('hot_product',true)->where('catagory_id',$category->id)->orderby('post_instalment','desc')->limit(8)->get();
             });
            foreach($get_category_product as $key => $category){
              foreach($category->gethotproduct as $key1 => $product){
                    $get_category_product[$key]->gethotproduct[$key1]->getminoffere =OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                }
            }
                                                                                                         
            if(count($get_category_product) > 0){
              $last_category_id = $get_category_product->max('id');
            }
           if($last_category_id==null){
               $last_category_id=0;
           } 
        $returncategoryHTML = view('web.home_page_partial_view.category',['get_category_product' => $get_category_product])->render();                              
        return response()->json( array('success' => true, 'categoryhtml' => $returncategoryHTML, 'last_category_id' =>  $last_category_id));
    }

    public function getroute(Request $request){
    if($request->link == 'loginregister'){
        Session::put('url',$request->url);
    }
    else if($request->link == 'dasboard'){
        Session::put('checkdashboard',$request->url);
    }
        return response()->json( array('status' => true));
    }

    public function contactus()
    {
        $get_category         = Catagory::with('SubCatagory')->has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_contact_page_seo_description = Cms::where('page_title','ContactUs')->first();

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.contact-us',compact('get_category','get_contact_page_seo_description','get_top_product_category','get_top_product_brand'));
    }

    public function aboutus()
    {
        $get_category         = Catagory::with('SubCatagory')->has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_about_page_seo_description = Cms::where('page_title','AboutUs')->first();
        
        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.about-us',compact('get_category','get_about_page_seo_description','get_top_product_category','get_top_product_brand'));
    }

    public function faq()
    {
        $get_category         = Catagory::with('SubCatagory')->has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_faq_page_seo_description = Cms::where('page_title','FAQ')->first();
        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        return view('web.faq',compact('get_category','get_faq_page_seo_description','get_top_product_category','get_top_product_brand'));
    }

    public function privacy()
    {
        $get_category         = Catagory::with('SubCatagory')->has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_privacypoicy_page_seo_description = Cms::where('page_title','Privacy Policy')->first();

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.privacy-policy',compact('get_category','get_privacypoicy_page_seo_description','get_top_product_category','get_top_product_brand'));
    }

    public function termandcondition()
    {
        $get_category         = Catagory::with('SubCatagory')->has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_termcondition_page_seo_description = Cms::where('page_title','Term Condition')->first();

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.term-conditions',compact('get_category','get_termcondition_page_seo_description','get_top_product_category','get_top_product_brand'));
    }

    public function whybuyfromus()
    {
        $get_category         = Catagory::with('SubCatagory')->has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_whybuy_page_seo_description = Cms::where('page_title','Why Buy From Us')->first();

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.why-buy-from-us',compact('get_category','get_whybuy_page_seo_description','get_top_product_category','get_top_product_brand'));
    }

    public function store(Request $request)
    {
        $noti =new Notification;
        $token="d8blJ_Tssfo:APA91bGSdheyWgGlAet6jCR928sHCL-9I-RdQj6EUECIFC3ztyFeX4sn3qUS2omCYFgTcenP6NPqtEGymVoV-pOAAhJmTU2APunodnQBApJi2CIOakqf4tlVeFVZ9ZcFjSWCiPYG60fw";
        //$token=auth()->user()->where('id',3)->first();
        $noti->toSingleDevice($token,'sdfds','fdsf',null,null);
        return view('/web/home');
    }
}
