<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;   
use App\SubCatagory;
use App\OffereDetail;
use App\City;
use App\Product;
use App\SubArea;
use App\Customer;
use App\Cms;
use App\Order;
use Auth;
use DB;
use Session;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Session::has('checkdashboard')){
        }
        else{
            Session::put('checkdashboard','dashboard');    
        }
        $get_category         = Catagory::has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });
        $get_dashboard_cms    = Cms::where('page_title','Dashboard')->first();
        $get_order_history    = Order::with(['OffereDetail.OffereMaster.Vendor','OffereDetail.OffereMaster.Product'])->where('customer_id',Auth::user()->userType->id)->get();
        $cities               = City::all();
        $sub_area_id          = Auth::user()->userType->sub_area_id;
        $getcity              = City::whereHas('area.subarea',function($q) use($sub_area_id){
                                     $q->where('id',$sub_area_id);
                              })->first();
        $sub_areas           = SubArea::whereHas('area',function($q) use ($getcity){
                                       $q->where('city_id',$getcity->id);
                                })->get();
        $customer_detail      = Customer::with(['user','subarea'])->where('id',Auth::user()->userType->id)->first();

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        //return $get_order_history;
        return view('web.dashboard',compact('get_category','get_dashboard_cms','get_order_history','cities','getcity','sub_areas','customer_detail','get_top_product_category','get_top_product_brand'));
    }
}
