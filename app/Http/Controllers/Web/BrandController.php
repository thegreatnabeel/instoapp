<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;
use App\SubCatagory;
use App\OffereDetail;
use App\City;
use App\Product;
use App\Brand;
use App\OffereMaster;
use App\WishList;
use Auth;
use Session;
use DB;
class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($brand_slug)
    {
        $get_brand_product = [];
        $load_more_product = [];
        $last_product_id = 0;
        $city_id = 1;
        $customer_id = 0;
        $currentdate = date('Y-m-d');
        $sub_area_id = 0;
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
            $sub_area_id = Auth::user()->userType->sub_area_id;
            $city_id = Session::get('city')['city_id'];
       }
       else{
           $city_id =Session::get('city') == null ? 1: Session::get('city')['city_id'];
           $customer_id = 0;
           $sub_area_id = 0;
       }
        $brand = Brand::where('brand_slug',$brand_slug)->first();
        $get_category         = Catagory::has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });
        $get_brand_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                            ->where('brand_id',$brand->id)->orderby('post_instalment','desc')->limit(12)->get();
        if($sub_area_id == 0) {
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->whereHas('OffereMaster.Product.Brand',function($q) use($brand){$q->where('brands.id',$brand->id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
        $get_brand_product->each(function($product, $key) use($get_brand_product,$city_id,$currentdate) {
        $get_brand_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                                                     });
        }
        else{
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                            ->whereHas('OffereMaster.Product.Brand',function($q) use($brand){$q->where('brands.id',$brand->id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
        $get_brand_product->each(function($product, $key) use($get_brand_product,$city_id,$currentdate,$sub_area_id) {
        $get_brand_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                                                     });
        }

        
        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
                            
        return view('web.brand',compact('get_category','get_featured_product','get_brand_product','brand','load_more_product','get_top_product_category','get_top_product_brand'));
    }

    public function loadfeatureandbrandforbrandpage(Request $request){
        $get_brand_product = [];
        $get_featured_product =[];
        $load_more_product = [];
        $brands =[];
        $key_features =[];
        $city_id = 0;
        $customer_id =0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
       }
       else{
           $customer_id = 0;
       }
        $brand = Brand::where('id',$request->brand_id)->first();
        $getcityid = City::where('name',$request->city_name)->first();
        if($getcityid){
            $city_id = $getcityid->id;
            Session::put('city',['city_id' => $getcityid->id, 'city_name' => $getcityid->name]);
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->whereHas('OffereMaster.Product.Brand',function($q) use($brand){$q->where('brands.id',$brand->id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
            $get_brand_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                            ->where('brand_id',$brand->id)->orderby('post_instalment','desc')->limit(12)->get();
            $get_brand_product->each(function($product, $key) use($get_brand_product,$city_id,$currentdate) {
            $get_brand_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                                                     });                    
        $get_featured_product->each(function($offere, $key) use($get_featured_product,$customer_id) {
            $get_featured_product[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();
        });
       }
        else{
            $get_brand_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                            ->where('brand_id',$brand->id)->orderby('post_instalment','desc')->limit(12)->get();
            $get_brand_product->each(function($product, $key) use($get_brand_product,$city_id,$currentdate) {
            $get_brand_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                                                     });                    
        Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
        }
    
        $returnfeatureHTML = view('web.brand_page_partial_view.feature_deal',['get_featured_product'=> $get_featured_product])->render();
        $returnbrandHTML = view('web.brand_page_partial_view.product',['get_brand_product' => $get_brand_product,'load_more_product' =>$load_more_product ])->render();
        return response()->json( array('success' => true, 'featurehtml'=> $returnfeatureHTML, 'brandhtml' => $returnbrandHTML,'location_id'=>$city_id ) );
    }

    public function loadbrandwiseproduct(Request $request){
        $load_more_product = [];
        $get_brand_product = [];
        $city_id = $request->city_id;
        $sub_area_id = 0;
        $currentdate = date('Y-m-d'); 
        $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                        ->where('brand_id',$request->brand_id)->whereNotIn('id',$request->product_id)->orderby('post_instalment','desc')->limit(12)->get();
        if(Auth::check()==true && Auth::user()->user_type == "client")
        {
            $sub_area_id = Auth::user()->userType->sub_area_id;
            $load_more_product->each(function($product, $key) use($load_more_product,$city_id,$currentdate,$sub_area_id) {
                $load_more_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                     ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                     ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                         });
       }
       else{
        $load_more_product->each(function($product, $key) use($load_more_product,$city_id,$currentdate,$sub_area_id) {
            $load_more_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                     });
       }                    
        $returnproductHTML = view('web.brand_page_partial_view.product',['load_more_product' => $load_more_product, 'get_brand_product' => $get_brand_product,])->render();                               
        return response()->json( array('success' => true, 'producthtml' => $returnproductHTML) );
    }
}
