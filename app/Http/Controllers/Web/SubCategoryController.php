<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;
use App\SubCatagory;
use App\OffereDetail;
use App\City;
use App\Product;
use App\Brand;
use App\KeyFeature;
use App\SubKeyFeature;
use App\OffereMaster;
use App\WishList;
use Auth;
use DB;
use Session;
class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subcategory_slug)
    {
        $get_subcategory_product = [];
        $load_more_product = [];
        $last_product_id = 0;
        $brands =[];
        $key_features =[];
        $city_id = 1;
        $customer_id = 0;
        $sub_area_id = 0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
            $sub_area_id = Auth::user()->userType->sub_area_id;
            $city_id = Session::get('city')['city_id'];
       }
       else{
           $city_id =Session::get('city') == null ? 1: Session::get('city')['city_id'];
           $customer_id = 0;
           $sub_area_id = 0;
       }
        $sub_category = SubCatagory::where('sub_catagory_slug',$subcategory_slug)->first();
        $get_category         = Catagory::has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });
        $get_subcategory_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                            ->where('sub_catagory_id',$sub_category->id)->orderby('post_instalment','desc')->limit(12)->get();
        if($sub_area_id == 0){                                    
        $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->whereHas('OffereMaster.Product.SubCatagory',function($q) use($sub_category){$q->where('sub_catagories.id',$sub_category->id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
        
        $get_subcategory_product->each(function($product, $key) use($get_subcategory_product,$city_id,$currentdate) {
        $get_subcategory_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                    ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                    ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
        });
        }
        else{
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                                  ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                                  ->whereHas('OffereMaster.Product.SubCatagory',function($q) use($sub_category){$q->where('sub_catagories.id',$sub_category->id);})
                                                  ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();

            $get_subcategory_product->each(function($product, $key) use($get_subcategory_product,$city_id,$currentdate,$sub_area_id) {
            $get_subcategory_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                                                         ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                         ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
            }); 
        }
        $get_featured_product->each(function($offere, $key) use($get_featured_product,$customer_id) {
        $get_featured_product[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();
        });                   
        if($sub_category){
            $brands = Brand::whereHas('Product',function($q) use($sub_category){
                $q->where('sub_catagory_id',$sub_category->id);
                })->get();
    
            $key_features = KeyFeature::whereHas('SubCatagory.Category',function($q) use($sub_category){
                $q->where('sub_catagory_id',$sub_category->id);
                })->whereHas('SubKeyFeature',function($q){
                 $q->havingRaw('count(sub_key_features.key_feature_id) > 0');
                })->get();
            $key_features->each(function($key_feature, $key) use($key_features) {
            $key_features[$key]->key_feature_value  = SubKeyFeature::where('key_feature_id',$key_feature->id)
                           ->distinct()->get(['sub_key_feature_value','key_feature_id']);
             });
        }

        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.subcategory',compact('get_category','get_featured_product','get_subcategory_product','sub_category','load_more_product','key_features','brands','get_top_product_brand','get_top_product_category'));
    }

    public function loadfeatureandproductforsubcategorypage(Request $request){
        $get_subcategory_product = [];
        $get_featured_product =[];
        $load_more_product = [];
        $brands =[];
        $key_features =[];
        $city_id = 0;
        $customer_id =0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
       }
       else{
           $customer_id = 0;
       }
        $sub_category = SubCatagory::where('id',$request->subcategory_id)->first();
        $getcityid = City::where('name',$request->city_name)->first();
        if($getcityid){
            $city_id = $getcityid->id;
            Session::put('city',['city_id' => $getcityid->id, 'city_name' => $getcityid->name]);
            $get_featured_product = OffereDetail::with(['OffereMaster.Product.ProductImage.ProductResizeImage','OffereMaster.Vendor'])
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->whereHas('OffereMaster.Product.SubCatagory',function($q) use($sub_category){$q->where('sub_catagories.id',$sub_category->id);})
                                            ->where('featured',true)->where('offere_valid_date','>=',$currentdate)->get();
            $get_subcategory_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                            ->where('sub_catagory_id',$sub_category->id)->orderby('post_instalment','desc')->limit(12)->get();
            $get_subcategory_product->each(function($product, $key) use($get_subcategory_product,$city_id,$currentdate) {
            $get_subcategory_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                                                     });                    
        $get_featured_product->each(function($offere, $key) use($get_featured_product,$customer_id) {
            $get_featured_product[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();
         });
       }
        else{
            $get_subcategory_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                            ->where('sub_catagory_id',$sub_category->id)->orderby('post_instalment','desc')->limit(12)->get();
            $get_subcategory_product->each(function($product, $key) use($get_subcategory_product,$city_id,$currentdate) {
            $get_subcategory_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                                                     });                    
        Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
        }
        $returnfeatureHTML = view('web.subcategory_page_partial_view.feature_deal',['get_featured_product'=> $get_featured_product])->render();
        $returnsubcategoryHTML = view('web.subcategory_page_partial_view.product',['get_subcategory_product' => $get_subcategory_product , 'load_more_product' =>$load_more_product ])->render();
        return response()->json( array('success' => true, 'featurehtml'=> $returnfeatureHTML, 'categoryhtml' => $returnsubcategoryHTML,'location_id'=>$city_id ) );
    }

    public function loadsubcategoryproduct(Request $request){
        $load_more_product = [];
        $get_subcategory_product = [];
        $city_id = $request->city_id; 
        $brand_id = $request->brands;
        $currentdate = date('Y-m-d');
        $Sub_key_feature_value =$request->feature_values; 
        if($Sub_key_feature_value==null){
            if($brand_id==null){
                $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                        ->where('sub_catagory_id',$request->subcategory_id)->orderby('post_instalment','desc')->whereNotIn('id',$request->product_id)->limit(12)->get();
            }
            else{
                $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage']) 
                                          ->whereIn('brand_id',$brand_id)->where('sub_catagory_id',$request->subcategory_id)->whereNotIn('id',$request->product_id)->orderby('post_instalment','desc')->limit(12)->get();
            }
        }
        else{
            if($brand_id==null){
                $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage']) 
                                             ->whereHas('SubKeyFeature',function($q) use($Sub_key_feature_value){
                                             $q->whereIn('sub_key_feature_value',$Sub_key_feature_value);})   
                                             ->where('sub_catagory_id',$request->subcategory_id)->orderby('post_instalment','desc')->whereNotIn('id',$request->product_id)->limit(12)->get();
            }
            else{
                $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage']) 
                                              ->whereHas('SubKeyFeature',function($q) use($Sub_key_feature_value){
                                                $q->whereIn('sub_key_feature_value',$Sub_key_feature_value);})
                                             ->whereIn('brand_id',$brand_id)->where('sub_catagory_id',$request->subcategory_id)->whereNotIn('id',$request->product_id)->orderby('post_instalment','desc')->limit(12)->get();
            }
        }
        if(Auth::check()==true && Auth::user()->user_type == "client")
        {
            $sub_area_id = Auth::user()->userType->sub_area_id;
            $load_more_product->each(function($product, $key) use($load_more_product,$city_id,$currentdate,$sub_area_id) {
                $load_more_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                                                            ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                            ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                });
       }
       else{
        $load_more_product->each(function($product, $key) use($load_more_product,$city_id,$currentdate) {
            $load_more_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
            });
       }
        $returnproductHTML = view('web.subcategory_page_partial_view.product',['load_more_product' => $load_more_product, 'get_subcategory_product' => $get_subcategory_product,])->render();                               
        return response()->json( array('success' => true, 'producthtml' => $returnproductHTML) );
    }

    public function loadsubcategoryfilterproduct(Request $request){
        $load_more_product = [];
        $get_subcategory_product = [];
        $last_product_id =0;
        $city_id = $request->city_id;
        $Sub_key_feature_id=$request->feature_id;
        $brand_id = $request->brands;
        $Sub_key_feature_value = $request->feature_values;
        $currentdate = date('Y-m-d');
        if($brand_id==null){
            if($Sub_key_feature_value==null){
                $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])    
                                            ->where('sub_catagory_id',$request->subcategory_id)->orderby('post_instalment','desc')->limit(12)->get();
            }
            else{
                $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage']) 
                                            ->whereHas('SubKeyFeature',function($q) use($Sub_key_feature_value,$Sub_key_feature_id){
                                            $q->whereIn('sub_key_feature_value',$Sub_key_feature_value);})    
                                            ->where('sub_catagory_id',$request->subcategory_id)->orderby('post_instalment','desc')->limit(12)->get();
           }
          }
          else{
            if($Sub_key_feature_value==null){
                $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage'])
                                                ->whereIn('brand_id',$brand_id)->orderby('post_instalment','desc')->where('sub_catagory_id',$request->subcategory_id)->limit(12)->get();
            }
            else{
                $load_more_product = Product::with(['ProductImage','ProductImage.ProductResizeImage']) 
                                               ->whereHas('SubKeyFeature',function($q) use($Sub_key_feature_value,$Sub_key_feature_id){
                                               $q->whereIn('sub_key_feature_value',$Sub_key_feature_value);})
                                              ->whereIn('brand_id',$brand_id)->where('sub_catagory_id',$request->subcategory_id)->orderby('post_instalment','desc')->limit(12)->get();
           }
          }
          if(Auth::check()==true && Auth::user()->user_type == "client")
          {
              $sub_area_id = Auth::user()->userType->sub_area_id;
              $load_more_product->each(function($product, $key) use($load_more_product,$city_id,$currentdate,$sub_area_id) {
                  $load_more_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                                                              ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                              ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                  });
         }
         else{
          $load_more_product->each(function($product, $key) use($load_more_product,$city_id,$currentdate) {
              $load_more_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                          ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                          ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
              });
         }
      //return $load_more_product;
      $returnproductHTML = view('web.subcategory_page_partial_view.product',['load_more_product' => $load_more_product, 'get_subcategory_product' => $get_subcategory_product])->render();                               
      return response()->json( array('success' => true, 'producthtml' => $returnproductHTML) );
    }
}
