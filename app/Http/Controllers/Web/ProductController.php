<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;
use App\SubCatagory;
use App\OffereDetail;
use App\City;
use App\Product;
use App\WishList;
use App\KeyFeature;
use App\SubKeyFeature;
use App\OffereMaster;
use App\Order;
use Session;
use Auth;
use DB;
use App\GiveOffereDetail;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($product_slug)
    {
        $city_id =1;
        $offere_count =[];
        $get_offere_detail=[];
        $get_all_offere_detail =[];
        $customer_id = 0;
        $sub_area_id = 0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
            $sub_area_id = Auth::user()->userType->sub_area_id;
            $city_id = Session::get('city')['city_id'];
       }
       else{
           $city_id =Session::get('city') == null ? 1: Session::get('city')['city_id'];
           $customer_id = 0;
           $sub_area_id = 0;
       }
       $get_category         = Catagory::has('Product')
                                      ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_category->each(function($category, $key) use($get_category) {
        $get_category[$key]->subcatagory  = SubCatagory::has('Product')->where('catagory_id',$category->id)->get();
        });
        $product        = Product::with(['ProductImage','ProductImage.ProductResizeImage'])->where('product_slug',$product_slug)->first();
        if(!empty($product))
        {
            if($sub_area_id == 0){
                $product->get_offere_detail = OffereDetail::with('OffereMaster.Vendor')->select(DB::raw('*'))
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->join(
                                            DB::raw('(SELECT offere_master_id, MIN(installment) AS installment FROM offere_details WHERE post_on_instalment=1 AND offere_valid_date >=CURDATE() GROUP BY offere_master_id) as s2'), 
                                          function($query) {
                                            $query->on('offere_details.offere_master_id', '=', 's2.offere_master_id')
                                            ->on('offere_details.installment', '=', 's2.installment');
                                          })
                                          ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('offere_details.installment')->limit(3)->get();
            }
            else{
                $product->get_offere_detail = OffereDetail::with('OffereMaster.Vendor')->select(DB::raw('*'))
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                        ->join(
                                            DB::raw('(SELECT offere_master_id, MIN(installment) AS installment FROM offere_details WHERE post_on_instalment=1 AND offere_valid_date >=CURDATE() GROUP BY offere_master_id) as s2'), 
                                          function($query) {
                                            $query->on('offere_details.offere_master_id', '=', 's2.offere_master_id')
                                            ->on('offere_details.installment', '=', 's2.installment');
                                          })->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('offere_details.installment')->limit(3)->get();
            }            
            foreach($product->get_offere_detail as $key => $offere){
            $product->get_offere_detail[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();   
            }
        }
        for($i=0;$i<count($product->get_offere_detail);$i++){
            array_push($offere_count,$product->get_offere_detail[$i]->id);
        }
        if(!empty($product))
        {
            $ids = implode(', ', $offere_count);
            if($ids){}
            else{$ids = 0;}
            if($sub_area_id == 0){
                $product->get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')->select(DB::raw('*'))
                                                  ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                  ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                                  ->join(
                                                        DB::raw('(SELECT offere_master_id, MIN(installment) AS installment FROM offere_details WHERE id NOT IN('.$ids.') AND post_on_instalment=1 AND offere_valid_date >=CURDATE() GROUP BY offere_master_id) as s2'), 
                                                        function($query) {
                                                        $query->on('offere_details.offere_master_id', '=', 's2.offere_master_id')
                                                   ->on('offere_details.installment', '=', 's2.installment');
                                                })->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('offere_details.installment')->get();
            }
            else{
                $product->get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')->select(DB::raw('*'))
                                                ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                                ->join(
                                                    DB::raw('(SELECT offere_master_id, MIN(installment) AS installment FROM offere_details WHERE id NOT IN('.$ids.') AND post_on_instalment=1 AND offere_valid_date >=CURDATE() GROUP BY offere_master_id) as s2'), 
                                                    function($query) {
                                                    $query->on('offere_details.offere_master_id', '=', 's2.offere_master_id')
                                                ->on('offere_details.installment', '=', 's2.installment');
                                                })->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('offere_details.installment')->get();
            }
            foreach($product->get_all_offere_detail as $key => $offere){
            $product->get_all_offere_detail[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();   
           } 
        }
        for($i=0;$i<count($product->get_all_offere_detail);$i++){
            array_push($offere_count,$product->get_all_offere_detail[$i]->id);
        } 
        
        $related_product = Product::with(['ProductImage','ProductImage.ProductResizeImage']) 
                                           ->where('catagory_id',$product->catagory_id)
                                           ->where('id','!=',$product->id)->limit(4)->get();
        if($sub_area_id == 0){
            $related_product->each(function($product, $key) use($related_product,$city_id,$currentdate) {
            $related_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                  ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
             });
        }
        else{
            $related_product->each(function($product, $key) use($related_product,$city_id,$currentdate,$sub_area_id) {
                $related_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);}) 
                                                                     ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                      ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
                 });
        }

        if($sub_area_id == 0){
            $advance_max = OffereDetail::whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->max('Advance');
            $advance_min = OffereDetail::whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('Advance');
            $installment_max = OffereDetail::whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->max('installment');
            $installment_min = OffereDetail::whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
        }
        else{
            $advance_max = OffereDetail::whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->max('Advance');
            $advance_min = OffereDetail::whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('Advance');
            $installment_max = OffereDetail::whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->max('installment');
            $installment_min = OffereDetail::whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                        ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
        }
            if($advance_max==null){
                $advance_max=0;
            }
            else{
                $advance_max = floatval($advance_max);
            }
            if($advance_min==null){
                $advance_min=0;
            }
            else{
                $advance_min = floatval($advance_min);
            }
            if($installment_max==null){
                $installment_max=0;
            }
            else{
                $installment_max = floatval($installment_max);
            }
            if($installment_min==null){
                $installment_min=0;
            }
            else{
                $installment_min = floatval($installment_min);
            }
            Session::put('offere_count',$offere_count);

            $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();

        return view('web.product',compact('get_category','product','get_offere_detail','related_product','advance_max','advance_min','installment_max','installment_min','get_all_offere_detail','get_top_product_category','get_top_product_brand'));
    }

    public function loadproductbylcation(Request $request){
        $city_id =0;
        $offere_count = [];
        $product =null;
        $get_offere_detail=[];
        $get_all_offere_detail =[];
        $related_product =[];
        $advance_max=1;
        $advance_min=0;
        $installment_max=1;
        $installment_min=0;
        $customer_id =0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
       }
       else{
           $customer_id = 0;
       }
        $getcityid = City::where('name',$request->city_name)->first();
        if($getcityid){
            $city_id = $getcityid->id;
            Session::put('city',['city_id' => $getcityid->id, 'city_name' => $getcityid->name]);
            $getproduct = Product::where('id',$request->product_id)->first();

            $get_offere_detail = OffereDetail::with('OffereMaster.Vendor')->select(DB::raw('*'))
           ->whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
           ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
           ->join(
               DB::raw('(SELECT offere_master_id, MIN(installment) AS installment FROM offere_details WHERE post_on_instalment=1 AND offere_valid_date >=CURDATE() GROUP BY offere_master_id) as s2'), 
             function($query) {
               $query->on('offere_details.offere_master_id', '=', 's2.offere_master_id')
               ->on('offere_details.installment', '=', 's2.installment');
             })->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('offere_details.installment')->limit(3)->get();
            foreach($get_offere_detail as $key => $offere){
                $get_offere_detail[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();   
            }
        for($i=0;$i<count($get_offere_detail);$i++){
            array_push($offere_count,$get_offere_detail[$i]->id);
        }
           $ids = implode(', ', $offere_count);
           if($ids){}
            else{$ids = 0;}
           $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')->select(DB::raw('*'))
           ->whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
           ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
           ->join(
               DB::raw('(SELECT offere_master_id, MIN(installment) AS installment FROM offere_details WHERE id NOT IN('.$ids.') AND post_on_instalment=1 AND offere_valid_date >=CURDATE() GROUP BY offere_master_id) as s2'), 
             function($query) {
               $query->on('offere_details.offere_master_id', '=', 's2.offere_master_id')
               ->on('offere_details.installment', '=', 's2.installment');
             })->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('offere_details.installment')->get();
        foreach($get_all_offere_detail as $key => $offere){
          $get_all_offere_detail[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();   
        } 
        for($i=0;$i<count($get_all_offere_detail);$i++){
            array_push($offere_count,$get_all_offere_detail[$i]->id);
        } 
        
        $related_product = Product::with(['ProductImage','ProductImage.ProductResizeImage']) 
                                           ->where('catagory_id',$getproduct->catagory_id)
                                           ->where('id','!=',$getproduct->id)->limit(4)->get();
            $related_product->each(function($product, $key) use($related_product,$city_id,$currentdate) {
            $related_product[$key]->getminoffere  = OffereDetail::whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);}) 
                                                                 ->whereHas('OffereMaster',function($q) use($product){$q->where('product_id',$product->id);})
                                                                 ->where('post_on_instalment',1)->min('installment');
             });

            $advance_max = OffereDetail::whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->max('Advance');
            $advance_min = OffereDetail::whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('Advance');
            $installment_max = OffereDetail::whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->max('installment');
            $installment_min = OffereDetail::whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
                                        ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                        ->whereHas('OffereMaster',function($q) use($getproduct){$q->where('product_id',$getproduct->id);})
                                        ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->min('installment');
            if($advance_max==null){
                $advance_max=1;
            }
            else{
                $advance_max = floatval($advance_max);
            }
            if($advance_min==null){
                $advance_min=0;
            }
            else{
                $advance_min = floatval($advance_min);
            }
            if($installment_max==null){
                $installment_max=1;
            }
            else{
                $installment_max = floatval($installment_max);
            }
            if($installment_min==null){
                $installment_min=0;
            }
            else{
                $installment_min = floatval($installment_min);
            }
            if($offere_count==null){
                Session::put('offere_count',0);
            }
            else{
                Session::put('offere_count',$offere_count);
            }
        }
        else{
            $advance_max=1;
            $advance_min=0;
            $installment_max=1;
            $installment_min=0;
            Session::put('offere_count',0);
            Session::put('city',['city_id' => 1, 'city_name' => 'Lahore']);
        }
        $returnminoffereHTML = view('web.product_page_partial_view.min_offere',['product'=>$product,'get_offere_detail'=>$get_offere_detail ])->render();
        $returoffereHTML     = view('web.product_page_partial_view.offere',['product'=>$product,'get_all_offere_detail'=>$get_all_offere_detail ])->render();
        return response()->json( array('success' => true, 'minofferehtml' => $returnminoffereHTML,'offerehtml'=> $returoffereHTML,'advance_max'=>$advance_max,'advance_min'=>$advance_min,'installment_max'=>$installment_max,'installment_min'=>$installment_min,'city_id'=>$city_id ) );
    }


    public function loadofferebysort(Request $request){
        $city_id    = $request->city_id;
        $product_id = $request->product_id;
        $product    = null;
        $customer_id =0;
        $sub_area_id = 0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
            $sub_area_id = Auth::user()->userType->sub_area_id;
       }
       else{
           $customer_id = 0;
           $sub_area_id = 0;
       }
        if($sub_area_id == 0)
        {
           
           if($request->sort_by == "Highest"){
            $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')
                                            ->whereHas('OffereMaster',function($q) use($product_id){$q->where('product_id',$product_id);})
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('installment','desc')->get();
        }
        else{
            $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')
                                            ->whereHas('OffereMaster',function($q) use($product_id){$q->where('product_id',$product_id);})
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('installment')->get();
           }
        }
        else{
            if($request->sort_by == "Highest"){
                $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')
                                                ->whereHas('OffereMaster',function($q) use($product_id){$q->where('product_id',$product_id);})
                                                ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                                ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('installment','desc')->get();
            }
            else{
                $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')
                                                ->whereHas('OffereMaster',function($q) use($product_id){$q->where('product_id',$product_id);})
                                                ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                                ->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('installment')->get();
            }
        }
        foreach($get_all_offere_detail as $key => $offere){
            $get_all_offere_detail[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();   
          }
        
        
        $returoffereHTML     = view('web.product_page_partial_view.offere',['product'=>$product,'get_all_offere_detail'=>$get_all_offere_detail ])->render();
        return response()->json( array('success' => true, 'offerehtml'=> $returoffereHTML ) );  
    }

    public function loadofferebyfilter(Request $request){
        $city_id     = $request->city_id;
        $product_id  = $request->product_id;
        $advance     = explode('-',  $request->advance, 2);
        $emi         = explode('-', $request->emi, 2);
        $month       = $request->month;
        $product     = null;
        $advance_max = $advance[1];
        $advance_min = $advance[0];
        $emi_max     = $emi[1];
        $emi_min     = $emi[0];
        $customer_id =0;
        $sub_area_id = 0;
        $currentdate = date('Y-m-d');
        $get_all_offere_detail =[];
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
            $sub_area_id = Auth::user()->userType->sub_area_id;
       }
       else{
           $customer_id = 0;
           $sub_area_id = 0;
       }
       if($sub_area_id == 0)
       {
            $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')
                                            ->whereHas('OffereMaster',function($q) use($product_id){$q->where('product_id',$product_id);})
                                            ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                            ->where('post_on_instalment',1)->whereBetween('Advance',[$advance_min,$advance_max])
                                            ->whereBetween('installment',[$emi_min,$emi_max])->where('offere_valid_date','>=',$currentdate)->where(function($query) use ( $request ){
                                            if($request->month != null){
                                              $query->Where('month', $request->month);
                                            }
                                            })->get();
        }
        else{

            $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')
                                            ->whereHas('OffereMaster',function($q) use($product_id){$q->where('product_id',$product_id);})
                                            ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                            ->where('post_on_instalment',1)->whereBetween('Advance',[$advance_min,$advance_max])
                                            ->whereBetween('installment',[$emi_min,$emi_max])->where('offere_valid_date','>=',$currentdate)->where(function($query) use ( $request ){
                                            if($request->month != null){
                                              $query->Where('month', $request->month);
                                            }
                                            })->get();
        }
            foreach($get_all_offere_detail as $key => $offere){
                $get_all_offere_detail[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();   
            }   
        $returoffereHTML     = view('web.product_page_partial_view.offere',['product'=>$product,'get_all_offere_detail'=>$get_all_offere_detail ])->render();
        return response()->json( array('success' => true, 'offerehtml'=> $returoffereHTML ) );  
    }

    public function loadmoreoffere(Request $request){
        $product = null;
        $get_all_offere_detail = [];
        $offere_count =[];
        $city_id = $request->city_id;
        $product_id = $request->product_id;
        $customer_id =0;
        $sub_area_id = 0;
        $currentdate = date('Y-m-d');
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
            $sub_area_id = Auth::user()->userType->sub_area_id;
       }
       else{
           $customer_id = 0;
           $sub_area_id = 0;
       }
        if( Session::get('offere_count')!=null && Session::get('offere_count')!=0){
            $offere_count = Session::get('offere_count');
        } 
           $ids = implode(', ', $offere_count);
           if($ids){}
            else{$ids = 0;}
            if($sub_area_id == 0){
                $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')->select(DB::raw('*'))
                                          ->whereHas('OffereMaster',function($q) use($product_id){$q->where('product_id',$product_id);})
                                          ->whereHas('OffereMaster.Vendor.SubArea.area.city',function($q) use($city_id){$q->where('cities.id',$city_id);})
                                          ->join(
                                             DB::raw('(SELECT offere_master_id, MIN(installment) AS installment FROM offere_details WHERE id NOT IN('.$ids.') AND post_on_instalment=1 AND offere_valid_date >=CURDATE() GROUP BY offere_master_id) as s2'), 
                                              function($query) {
                                              $query->on('offere_details.offere_master_id', '=', 's2.offere_master_id')
                                         ->on('offere_details.installment', '=', 's2.installment');
                                        })->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('offere_details.installment')->get();
            }
            else{

                $get_all_offere_detail = OffereDetail::with('OffereMaster.Vendor')->select(DB::raw('*'))
                                         ->whereHas('OffereMaster',function($q) use($product_id){$q->where('product_id',$product_id);})
                                         ->whereHas('OffereMaster.Vendor.SubArea',function($q) use($sub_area_id){$q->where('vendor_sub_areas.sub_area_id',$sub_area_id);})
                                         ->join(
                                            DB::raw('(SELECT offere_master_id, MIN(installment) AS installment FROM offere_details WHERE id NOT IN('.$ids.') AND post_on_instalment=1 AND offere_valid_date >=CURDATE() GROUP BY offere_master_id) as s2'), 
                                            function($query) {
                                            $query->on('offere_details.offere_master_id', '=', 's2.offere_master_id')
                                        ->on('offere_details.installment', '=', 's2.installment');
                                       })->where('post_on_instalment',1)->where('offere_valid_date','>=',$currentdate)->orderBy('offere_details.installment')->get();

            }
        foreach($get_all_offere_detail as $key => $offere){
            $get_all_offere_detail[$key]->wishlist  = WishList::where('offere_id',$offere->id)->where('customer_id',$customer_id)->first();   
        }
        for($i=0;$i<count($get_all_offere_detail);$i++){
        array_push($offere_count,$get_all_offere_detail[$i]->id);
        }              
        if($offere_count==null){
        }
        else{
            Session::put('offere_count',$offere_count);
        }
        $returoffereHTML     = view('web.product_page_partial_view.offere',['product'=>$product,'get_all_offere_detail'=>$get_all_offere_detail ])->render();
        return response()->json( array('success' => true, 'offerehtml'=> $returoffereHTML ) );
    }

    public function order(Request $request){
        $CurrentDate=date('Y-m-d');
        $customer_id =0;
        $status = false;
        $errMsg= '';
        if(Auth::check()==true && Auth::user()->user_type == "client"){
            $customer_id = Auth::user()->userType->id;
       }
       else{
           $customer_id = 0;
       }
        $get_offere_detail = OffereDetail::with(['OffereMaster','OffereMaster.Vendor'])->where('id',$request->offere_id)->first();
        $check_order = Order::where('customer_id',$customer_id)->where('offere_id',$get_offere_detail->id)
                            ->where('vendor_id',$get_offere_detail->offeremaster->vendor_id)
                            ->where('product_id',$get_offere_detail->offeremaster->product_id)->first();
        if($check_order){
            $status = false;
            $errMsg= 'Already order Saved';
        } 
        else{
            $get_offere_detail_for_cash = GiveOffereDetail::where('offere_id',$request->offere_id)->first();
            $order = new  Order();
            $order->customer_id = $customer_id;
            $order->offere_id   = $get_offere_detail->id;
            $order->vendor_id   = $get_offere_detail->offeremaster->vendor_id;
            $order->product_id  = $get_offere_detail->offeremaster->product_id;
            $order->cash_price  = $get_offere_detail_for_cash->cash_price;
            $order->order_date  = $CurrentDate;
            $order->place_order = 'instalment.pk';
            $order->status      = 'pending';
            $order->save();
            $status = true;
            $errMsg= 'Order Saved Successfully';
        }
        return response()->json( array('status' => $status, 'errMsg'=> $errMsg,'get_offere_detail'=>$get_offere_detail ) );
    }
}
