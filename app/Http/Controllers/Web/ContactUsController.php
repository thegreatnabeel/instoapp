<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Catagory;
use App\Cms;
use App\Product;
use DB;

class ContactUsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contactus()
    {
        $get_category         = Catagory::with('SubCatagory')->has('Product')
                                           ->where('show_nav',true)->orderBy('nav_order')->get();
        $get_contact_page_seo_description = Cms::where('page_title','ContactUs')->first();
        $get_top_product_category = Product::with('Category')->groupBy('catagory_id')->orderBy(DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        $get_top_product_brand = Product::with('Brand')->groupBy('brand_id')->orderBy( DB::raw('count(request_offere_number)','DESC'),'DESC')->limit(5)->get();
        return view('web.contact-us',compact('get_category','get_contact_page_seo_description','get_top_product_category','get_top_product_brand'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'name'             => 'required|string',
           'contact_number'   => 'required|numeric|digits_between:11,12',
           'email'            => 'required|email',
           'message'          => 'required|string',
           'captch'           => 'required|confirmed'
        ]);
        return redirect()->route('web.contactus')->with([
            'status' => 'success',
            'message' => 'Email Send Successfully.'
        ]);
    }
}
