<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Order;
use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Session::get('checkorderlink') == 'total_orders'){
            $orders  = Order::with(['Customer','OffereDetail.OffereMaster.Vendor','OffereDetail.OffereMaster.Product'])->get();
        }
        else if(Session::get('checkorderlink') == 'total_new_orders'){
            $orders  = Order::with(['Customer','OffereDetail.OffereMaster.Vendor','OffereDetail.OffereMaster.Product'])->where('status','pending')->get();
        }
        else{
            $orders  = Order::with(['Customer','OffereDetail.OffereMaster.Vendor','OffereDetail.OffereMaster.Product'])->get();
        }
        return view('admin.Order.index', compact('orders'));
    }
    public function changeorderstatus(Request $request)
    {
        $status = false;
        $order = Order::where('id',$request->id)->first();
        if($order){
            $order->status = $request->value;
            $order->save();
            $status = true;
        }
        return response()->json( array('status' => $status ));
    }
}
