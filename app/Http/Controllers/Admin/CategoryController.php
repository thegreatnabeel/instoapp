<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Catagory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Catagory::all();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'catagory_name'        => 'required|string',
           'catagory_slug'        => 'required|string',
           'catagory_img_path'    => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
        ]);

        $imageName=null;
        if($request->hasFile('catagory_img_path'))
        {
            $imageName = $request->file('catagory_img_path')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'subcatagoryimages', $request->file('catagory_img_path'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $catagory = new  Catagory();
         $catagory->catagory_name         = $request->catagory_name;
         $catagory->catagory_slug         = $request->catagory_slug;
         $catagory->catagory_description  = $request->catagory_description;
         $catagory->catagory_img_path     = $imageName;
         $catagory->show_nav              = !$request->has('show_nav')?0:1;
         $catagory->show_home_page        = !$request->has('show_home_page')?0:1;
         $catagory->nav_order             = $request->nav_order==null?0:$request->nav_order;
         $catagory->seo_description       = $request->seo_description;
         $catagory->meta_description      = $request->meta_description == null?$request->catagory_name:$request->meta_description;
         $catagory->meta_keyword          = $request->meta_keyword==null?$request->catagory_name:$request->meta_keyword;
         $catagory->save();
        return redirect()->route('admin.categories.index')->with([
            'status' => 'success',
            'message' => 'Category has been created'
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Catagory::findOrFail($id);
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'catagory_name'        => 'required|string',
           'catagory_slug'        => 'required|string',
           'catagory_img_path'    => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
        ]);

        $imageName=null;
        if($request->hasFile('catagory_img_path'))
        {
            $imageName = $request->file('catagory_img_path')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'subcatagoryimages', $request->file('catagory_img_path'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $catagory = Catagory::findOrFail($id);
         $catagory->catagory_name         = $request->catagory_name;
         $catagory->catagory_slug         = $request->catagory_slug;
         $catagory->catagory_img_path     = $imageName==null?$catagory->catagory_img_path:$imageName;
         $catagory->catagory_description  = $request->catagory_description;
         $catagory->show_nav              = !$request->has('show_nav')?0:1;
         $catagory->show_home_page        = !$request->has('show_home_page')?0:1;
         $catagory->nav_order             = $request->nav_order==null?0:$request->nav_order;
         $catagory->seo_description       = $request->seo_description;
         $catagory->meta_description      = $request->meta_description==null?$request->catagory_name:$request->meta_description;
         $catagory->meta_keyword          = $request->meta_keyword==null?$request->catagory_name:$request->meta_keyword;
         $catagory->save();
        return redirect()->route('admin.categories.index')->with([
            'status' => 'success',
            'message' => 'Category has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catagory = Catagory::findOrFail($id);
        $catagory->delete();

        return redirect(route('admin.categories.index'))->with(['status' => 'success', 'message' => 'Category Deleted Successfully!']);
    }
}
