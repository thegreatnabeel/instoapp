<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\OffereMaster;
use App\OffereDetail;
use Session;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_offere_master = OffereMaster::with(['Vendor','Product'])->orderBy('id','desc')->get();
        $get_offere_master->each(function($offere_master, $key) use($get_offere_master) {
            $get_offere_master[$key]->total_offere  = OffereDetail::where('offere_master_id',$offere_master->id)->count();
            });
        return view('admin.offer.index', compact('get_offere_master'));
    }

    public function offerDetail($id)
    {
        $get_offere  = OffereDetail::where('offere_master_id',$id)->get();
        return view('admin.offer.show', compact('get_offere'));
    }


}
