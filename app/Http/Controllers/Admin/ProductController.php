<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;
use App\SubCatagory;
use App\Catagory;
use App\KeyFeature;
use App\SubKeyFeature;
use App\Brand;
use App\Product;
use App\ProductImage;
use App\ProductResizeImage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::with('Brand')->with(['SubCatagory'=>function($q){
                            $q->with('Category');
                            }])->get();
        
        //return $products;
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Catagory::all();
        $brands=Brand::all();
        return view('admin.product.create', compact('brands','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request;
         $this->validate($request, [
           'product_name'        => 'required|string',
           'info'                => 'required|string',
           'info1'               => 'required|string',
           'product_slug'        => 'required|string|unique:products',
           'sub_catagory_id'     => 'required',
           'brand_id'            => 'required',
           'catagory_id'         => 'required',
           'product_image'       => 'required',
           'product_feature'     => 'required|string',
           'product_description' => 'required|string',
           'seo_description'     => 'nullable|string',
           'meta_description'    => 'nullable|string',
           'meta_keyword'        => 'nullable|string',
        ]);
         $product = new Product();
         $product->sub_catagory_id     = $request->sub_catagory_id;
         $product->brand_id            = $request->brand_id;
         $product->catagory_id         = $request->catagory_id;
         $product->product_slug        = $request->product_slug;
         $product->info                = $request->info;
         $product->info1               = $request->info1;
         $product->product_name        = $request->product_name;
         $product->product_description = $request->product_description;
         $product->product_feature     = $request->product_feature;
         $product->new_arrival         = !$request->has('new_arrival')?0:1;
         $product->hot_product         = !$request->has('hot_product')?0:1;
         $product->seo_description     = $request->seo_description;
         $product->meta_description    = $request->meta_description==null?$request->product_name:$request->meta_description;
         $product->meta_keyword        = $request->meta_keyword==null?$request->product_name:$request->meta_description;
         $product->save();
         if($request->meta_key[0]!=null){
         $i=0;
         foreach ($request->meta_key as $key => $value) {
            $subkeyfeature = new SubKeyFeature();
            $subkeyfeature->product_id            = $product->id;
            $subkeyfeature->key_feature_id        = $value;
            $subkeyfeature->sub_key_feature_value = $request->meta_value[$i];
            $subkeyfeature->save();
            $i++;
         }
        }


        $imageName=null;
        $path1=null;
        $path2=null;
        $path3=null;
        $path4=null;
        if($request->hasFile('product_image'))
        {
            $imageName  = $request->file('product_image')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName  = time().'_'.$imageName;
            $imageName1 = time().'_'.'190_190'.$imageName;
            $imageName2 = time().'_'.'530_430'.$imageName;
            $imageName3 = time().'_'.'400_200'.$imageName;
            $imageName4 = time().'_'.'234_234'.$imageName;


            $path = Storage::disk('public')->putFileAs(
                'productimages', $request->file('product_image'), $imageName
            );
            Image::load($request->file('product_image'))
                                ->fit(Manipulations::FIT_FILL, 190, 190)
                                    ->background('ffffff')
                                        ->save(public_path('storage/productimages/' .$imageName1));
            Image::load($request->file('product_image'))
                                ->fit(Manipulations::FIT_FILL, 530, 430)
                                    ->background('ffffff')
                                        ->save(public_path('storage/productimages/' .$imageName2));
            Image::load($request->file('product_image'))
                                ->fit(Manipulations::FIT_FILL, 400, 200)
                                    ->background('ffffff')
                                        ->save(public_path('storage/productimages/' .$imageName3));    

            Image::load($request->file('product_image'))
                                ->fit(Manipulations::FIT_FILL, 234, 234)
                                    ->background('ffffff')
                                        ->save(public_path('storage/productimages/' .$imageName4)); 

            $mainpath = 'storage/'.$path;
            $path1    = 'storage/productimages/'.$imageName1;
            $path2    = 'storage/productimages/'.$imageName2;
            $path3    = 'storage/productimages/'.$imageName3;
            $path4    = 'storage/productimages/'.$imageName4;
        }
        else
        {
            $mainpath = null;
            $path1    = null;
            $path2    = null;
            $path3    = null;
            $path4    = null;
        }

        $productimage = new ProductImage();
        $productimage->product_image = $mainpath;
        $productimage->product_id    = $product->id;
        $productimage->alt           = $product->product_name.' '.'on installment';
        $productimage->image_type    = "main";
        $productimage->save(); 

        $productresizeimage1 = new ProductResizeImage();
        $productresizeimage1->product_resize_image_path = $path1;
        $productresizeimage1->product_image_id          = $productimage->id;
        $productresizeimage1->alt                       = $product->product_name.' '.'on installment';
        $productresizeimage1->save();

        $productresizeimage2 = new ProductResizeImage();
        $productresizeimage2->product_resize_image_path = $path2;
        $productresizeimage2->product_image_id          = $productimage->id;
        $productresizeimage2->alt                       = $product->product_name.' '.'on installment';
        $productresizeimage2->save();

        $productresizeimage3 = new ProductResizeImage();
        $productresizeimage3->product_resize_image_path = $path3;
        $productresizeimage3->product_image_id          = $productimage->id;
        $productresizeimage3->alt                       = $product->product_name.' '.'on installment';
        $productresizeimage3->save();

        $productresizeimage4 = new ProductResizeImage();
        $productresizeimage4->product_resize_image_path = $path4;
        $productresizeimage4->product_image_id          = $productimage->id;
        $productresizeimage4->alt                       = $product->product_name.' '.'on installment';
        $productresizeimage4->save();

        if($request->hasfile('images'))
         {

            foreach($request->file('images') as $image)
            {
                $name=$image->getClientOriginalName();
                $name = str_replace(" ","",$name);
                $name  = time().'_'.$name;
                $gallery_path = Storage::disk('public')->putFileAs(
                'productimages', $image, $name
            );
            $gallery_path ='storage/'.$gallery_path;
            $productimagegallery = new ProductImage();
            $productimagegallery->product_image = $gallery_path;
            $productimagegallery->product_id    = $product->id;
            $productimagegallery->alt           = $product->product_name.' '.'on installment';
            $productimagegallery->image_type    = "gallery";
            $productimagegallery->save();  
            }
         }
        return redirect()->route('admin.product.index')->with([
            'status' => 'success',
            'message' => 'Product has been created'
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Catagory::all();
        $brands     = Brand::all();
        $product    = Product::with(['SubCatagory','SubKeyFeature'])
                            ->with(['ProductImage' => function($q){
                                $q->with('ProductResizeImage');
                            }])->where('id',$id)->first();

                            //return $product;
        $keyfeatures = KeyFeature::where('sub_catagory_id',$product->sub_catagory_id)->get(); 
        return view('admin.product.edit', compact('categories','brands','product','keyfeatures'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name'        => 'required|string',
            'info'                => 'required|string',
            'info1'               => 'required|string',
            'product_slug'        => 'required|string',
            'sub_catagory_id'     => 'required',
            'brand_id'            => 'required',
            'catagory_id'         => 'required',
            'product_feature'     => 'required|string',
            'product_description' => 'required|string',
            'seo_description'     => 'nullable|string',
            'meta_description'    => 'nullable|string',
            'meta_keyword'        => 'nullable|string',
         ]);
          $product = Product::findOrFail($id);
          $product->sub_catagory_id     = $request->sub_catagory_id;
          $product->brand_id            = $request->brand_id;
          $product->catagory_id         = $request->catagory_id;
          $product->product_slug        = $request->product_slug;
          $product->info                = $request->info;
          $product->info1               = $request->info1;
          $product->product_name        = $request->product_name;
          $product->product_description = $request->product_description;
          $product->product_feature     = $request->product_feature;
          $product->new_arrival         = !$request->has('new_arrival')?0:1;
          $product->hot_product         = !$request->has('hot_product')?0:1;
          $product->seo_description     = $request->seo_description;
          $product->meta_description    = $request->meta_description==null?$request->product_name:$request->meta_description;
          $product->meta_keyword        = $request->meta_keyword==null?$request->product_name:$request->meta_description;
          $product->save();
          $getSubKeyFeatures = SubKeyFeature::where('product_id',$product->id)->get();

          foreach($getSubKeyFeatures as $key => $SubKeyFeature){
            $SubKeyFeature->delete();
          }
          if($request->meta_key[0]!="Select Key Feature"){
          $i=0;
          foreach ($request->meta_key as $key => $value) {
             $subkeyfeature = new SubKeyFeature();
             $subkeyfeature->product_id            = $product->id;
             $subkeyfeature->key_feature_id        = $value;
             $subkeyfeature->sub_key_feature_value = $request->meta_value[$i];
             $subkeyfeature->save();
             $i++;
          }
        }
          
 
 
         $imageName=null;
         $path1=null;
         $path2=null;
         $path3=null;
         $path4=null;
         if($request->hasFile('product_image'))
         {
             $prodcutimage = ProductImage::where('image_type','main')->where('product_id',$product->id)->first();
             $prodcutimage->ProductResizeImage()->delete();
             
             $imageName  = $request->file('product_image')->getClientOriginalName();
             $imageName = str_replace(" ","",$imageName);
             $imageName  = time().'_'.$imageName;
             $imageName1 = time().'_'.'190_190'.$imageName;
             $imageName2 = time().'_'.'530_430'.$imageName;
             $imageName3 = time().'_'.'400_200'.$imageName;
             $imageName4 = time().'_'.'234_234'.$imageName;
 
 
             $path = Storage::disk('public')->putFileAs(
                 'productimages', $request->file('product_image'), $imageName
             );
             Image::load($request->file('product_image'))
                                 ->fit(Manipulations::FIT_FILL, 190, 190)
                                     ->background('ffffff')
                                         ->save(public_path('storage/productimages/' .$imageName1));
             Image::load($request->file('product_image'))
                                 ->fit(Manipulations::FIT_FILL, 530, 430)
                                     ->background('ffffff')
                                         ->save(public_path('storage/productimages/' .$imageName2));
             Image::load($request->file('product_image'))
                                 ->fit(Manipulations::FIT_FILL, 400, 200)
                                     ->background('ffffff')
                                         ->save(public_path('storage/productimages/' .$imageName3));    
 
             Image::load($request->file('product_image'))
                                 ->fit(Manipulations::FIT_FILL, 234, 234)
                                     ->background('ffffff')
                                         ->save(public_path('storage/productimages/' .$imageName4)); 
 
             $mainpath = 'storage/'.$path;
             $path1    = 'storage/productimages/'.$imageName1;
             $path2    = 'storage/productimages/'.$imageName2;
             $path3    = 'storage/productimages/'.$imageName3;
             $path4    = 'storage/productimages/'.$imageName4;
             $productimg = ProductImage::where('id',$prodcutimage->id)->first(); 
             $productimg->product_image = $mainpath;
             $productimg->alt           = $product->product_name.' '.'on installment';
             $productimg->save(); 
     
             $productresizeimage1 = new ProductResizeImage();
             $productresizeimage1->product_resize_image_path = $path1;
             $productresizeimage1->product_image_id          = $productimg->id;
             $productresizeimage1->alt                       = $product->product_name.' '.'on installment';
             $productresizeimage1->save();
     
             $productresizeimage2 = new ProductResizeImage();
             $productresizeimage2->product_resize_image_path = $path2;
             $productresizeimage2->product_image_id          = $productimg->id;
             $productresizeimage2->alt                       = $product->product_name.' '.'on installment';
             $productresizeimage2->save();
     
             $productresizeimage3 = new ProductResizeImage();
             $productresizeimage3->product_resize_image_path = $path3;
             $productresizeimage3->product_image_id          = $productimg->id;
             $productresizeimage3->alt                       = $product->product_name.' '.'on installment';
             $productresizeimage3->save();
     
             $productresizeimage4 = new ProductResizeImage();
             $productresizeimage4->product_resize_image_path = $path4;
             $productresizeimage4->product_image_id          = $productimg->id;
             $productresizeimage4->alt                       = $product->product_name.' '.'on installment';
             $productresizeimage4->save();
         }
         else
         {
             $mainpath = null;
             $path1    = null;
             $path2    = null;
             $path3    = null;
             $path4    = null;
         }
 
         if($request->hasfile('images'))
          {
 
             foreach($request->file('images') as $image)
             {
                 $name=$image->getClientOriginalName();
                 $name = str_replace(" ","",$name);
                 $name  = time().'_'.$name;
                 $gallery_path = Storage::disk('public')->putFileAs(
                 'productimages', $image, $name
             );
             $gallery_path ='storage/'.$gallery_path;
             $productimagegallery = new ProductImage();
             $productimagegallery->product_image = $gallery_path;
             $productimagegallery->product_id    = $product->id;
             $productimagegallery->alt           = $product->product_name.' '.'on installment';
             $productimagegallery->image_type    = "gallery";
             $productimagegallery->save();  
             }
          }
         return redirect()->route('admin.product.index')->with([
             'status' => 'success',
             'message' => 'Product has been updated'
         ]);
    }


    public function deletegalleryimage(Request $request){
        $status=false;
        $getImage = ProductImage::findOrFail($request->imageid);
        if($getImage){
            $getImage->delete();
            $status=true;
        }
        return response()->json(['status' => $status]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect(route('admin.product.index'))->with(['status' => 'success', 'message' => 'Product Deleted Successfully!']);
    }

    public function subcategorybyid(Request $request){
        $subcategory = SubCatagory::where('catagory_id',$request->data)->get();
        return response()->json([
           'status' => 'success',
           'subcategory' => $subcategory
        ]);
    }

    public function keyfeaturebysubid(Request $request){
        $featurekey = KeyFeature::where('sub_catagory_id',$request->data)->get();
        return response()->json([
           'status' => 'success',
           'featurekey' => $featurekey
        ]);
    }

    public function changenewarrival(Request $request){
        $status = false;
        $value = "";
        $getProduct = Product::where('id',$request->Id)->first();
        if($getProduct){
            if($getProduct->new_arrival == 0){
                $getProduct->new_arrival = 1;
                $getProduct->save();
                $status = true;
                $value = "New Arrival";
            }
            else{
                $getProduct->new_arrival = 0;
                $getProduct->save();
                $status = true;
                $value = "";
            }
        }
        else{
            $value = "";
        }

        return response()->json([
            'status' => $status,
            'value' => $value
         ]);

    }

    public function changehotproduct(Request $request){
        $status = false;
        $value = "";
        $getProduct = Product::where('id',$request->Id)->first();
        if($getProduct){
            if($getProduct->hot_product == 0){
                $getProduct->hot_product = 1;
                $getProduct->save();
                $status = true;
                $value = "Hot Product";
            }
            else{
                $getProduct->hot_product = 0;
                $getProduct->save();
                $status = true;
                $value = "";
            }
        }
        else{
            $value = "";
        }

        return response()->json([
            'status' => $status,
            'value' => $value
         ]);

    }
}
