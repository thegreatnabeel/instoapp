<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\InfoGraphic;

class InfoGraphicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $InfoGraphics=InfoGraphic::all();

        return view('admin.InfoGraphic.index', compact('InfoGraphics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.InfoGraphic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'graphic_image' => 'image|mimes:png,jpg,jpeg|max:5120|required',
        ]);

        $imageName=null;
        if($request->hasFile('graphic_image'))
        {
            $imageName = $request->file('graphic_image')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'infographic', $request->file('graphic_image'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $infographic = new  InfoGraphic();
         $infographic->info_graphic_path              = $imageName;
         $infographic->save();
        return redirect()->route('admin.InfoGraphic.index')->with([
            'status' => 'success',
            'message' => 'Info Graphic has been created'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $infographic = InfoGraphic::findOrFail($id);

        return view('admin.InfoGraphic.edit', compact('infographic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'graphic_image' => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
        ]);

        $imageName=null;
        if($request->hasFile('graphic_image'))
        {
            $imageName = $request->file('graphic_image')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'infographic', $request->file('graphic_image'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $infographic = InfoGraphic::findOrFail($id);
         $infographic->info_graphic_path              = $imageName == null ?  $infographic->info_graphic_path : $imageName;
         $infographic->save();
        return redirect()->route('admin.InfoGraphic.index')->with([
            'status' => 'success',
            'message' => 'Info Graphic has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $infographic = InfoGraphic::findOrFail($id);
        $infographic->delete();

        return redirect(route('admin.InfoGraphic.index'))->with(['status' => 'success', 'message' => 'Info Graphic Deleted Successfully!']);
    }
}
