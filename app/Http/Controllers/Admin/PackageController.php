<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Package;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages=Package::all();
        return view('admin.package.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name'            => 'required|string|unique:packages',
           'no_of_feature'   => 'required|numeric',
           'no_of_post'      => 'required|numeric',
           'price'           => 'required|numeric',
           'offere_validity' => 'required|numeric'
        ]);

         $package = new  Package();
         $package->name            = $request->name;
         $package->no_of_feature   = $request->no_of_feature;
         $package->no_of_post      = $request->no_of_post;
         $package->price           = $request->price;
         $package->offere_validity = $request->offere_validity;
         $package->save();
        return redirect()->route('admin.package.index')->with([
            'status' => 'success',
            'message' => 'Package has been created'
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::findOrFail($id);
        return view('admin.package.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'name'           => 'required|string',
           'no_of_feature'  => 'required|numeric',
           'no_of_post'     => 'required|numeric',
           'price'          => 'required|numeric',
           'offere_validity' => 'required|numeric'
        ]);

         $package = Package::findOrFail($id);
         $package->name           = $request->name;
         $package->no_of_feature  = $request->no_of_feature;
         $package->no_of_post     = $request->no_of_post;
         $package->price          = $request->price;
         $package->offere_validity = $request->offere_validity;
         $package->save();
        return redirect()->route('admin.package.index')->with([
            'status' => 'success',
            'message' => 'Package has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Package::findOrFail($id);
        $package->delete();

        return redirect(route('admin.package.index'))->with(['status' => 'success', 'message' => 'Package Deleted Successfully!']);
    }
}
