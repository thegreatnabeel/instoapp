<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Customer;
use App\User;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('user')->get();
        return view('admin.customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::with('user')->where('id',$id)->first();
        return view('admin.customer.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $user     = User::where('id',$customer->user_id)->first();
        $user->delete();
        $customer->delete();
        return redirect(route('admin.vendor.index'))->with(['status' => 'success', 'message' => 'Customer Deleted Successfully!']);
    }

    public function customerchangestatus(Request $request)
    {
        $customer = Customer::findOrFail($request->id);
        $customer->status = $request->status == "true" ? 1 : 0;
        $customer->save();
        return response()->json([
           'status' => 'success'
        ]);
    }

    public function cutomerchangevarification(Request $request)
    {
        $customer = Customer::findOrFail($request->id);
        $customer->verfication_status = $request->verfication_status;
        $customer->save();
        return response()->json([
           'status' => 'success'
        ]);
    }
}
