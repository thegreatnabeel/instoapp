<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Brand;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands=Brand::all();

        return view('admin.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'brand_name'        => 'required|string',
           'brand_slug'        => 'required|string',
           'brand_description' => 'required|string',
           'logo'              => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
        ]);

        $imageName=null;
        if($request->hasFile('logo'))
        {
            $imageName = $request->file('logo')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'brand', $request->file('logo'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $brand = new  Brand();
         $brand->brand_name        = $request->brand_name;
         $brand->brand_slug        = $request->brand_slug;
         $brand->brand_description = $request->brand_description;
         $brand->seo_description   = $request->seo_description;
         $brand->meta_description  = $request->meta_description==null?$request->brand_name:$request->meta_description;
         $brand->meta_keyword      = $request->meta_keyword==null?$request->brand_name:$request->meta_keyword;
         $brand->brand_img_path    = $imageName;
         $brand->save();
        return redirect()->route('admin.brand.index')->with([
            'status' => 'success',
            'message' => 'Brand has been created'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::findOrFail($id);

        return view('admin.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'brand_name'        => 'required|string',
           'brand_slug'        => 'required|string',
           'brand_description' => 'required|string',
           'logo'              => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
        ]);

        $imageName=null;
        if($request->hasFile('logo'))
        {
            $imageName = $request->file('logo')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'brand', $request->file('logo'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $brand = Brand::findOrFail($id);
         $brand->brand_name        = $request->brand_name;
         $brand->brand_slug        = $request->brand_slug;
         $brand->brand_description = $request->brand_description;
         $brand->seo_description   = $request->seo_description;
         $brand->meta_description  = $request->meta_description==null?$request->brand_name:$request->meta_description;
         $brand->meta_keyword      = $request->meta_keyword==null?$request->brand_name:$request->meta_keyword;
         $brand->brand_img_path    = $imageName!=null?$imageName:$brand->brand_img_path;
         $brand->save();
        return redirect()->route('admin.brand.index')->with([
            'status' => 'success',
            'message' => 'Brand has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        $brand->delete();

        return redirect(route('admin.brand.index'))->with(['status' => 'success', 'message' => 'Brand Deleted Successfully!']);
    }
}
