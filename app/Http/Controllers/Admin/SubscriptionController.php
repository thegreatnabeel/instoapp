<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Package;
use App\VendorPackage;
use App\Vendor;
use App\OffereDetail;
use App\GiveOffereMaster;
use App\PackageHistory;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendorpackages = VendorPackage::with('vendor')->get();
        return view('admin.subscription.index', compact('vendorpackages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $packages = Package::all();
        $vendors = Vendor::all();
        return view('admin.subscription.create', compact('packages','vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function getremainfeatureandpost(Request $request){
         $vendorId = $request->Id;
         $TotalFeature = 0;
         $TotalPost = 0;

         $getFeatureTotal = OffereDetail::whereHas('OffereMaster',function($q) use ($vendorId){
                                          $q->where('vendor_id',$vendorId);
                                          })->count();
         $getPostTotal     = GiveOffereMaster::where('vendor_id',$vendorId)->count();
         $getPackageDetail = VendorPackage::where('vendor_id',$vendorId)->first();
         if($getPackageDetail){
            $TotalFeature = $getPackageDetail->no_of_feature - $getFeatureTotal;
            $TotalPost    = $getPackageDetail->no_of_post - $getPostTotal;
         }
         else{
            $TotalFeature = 0;
            $TotalPost = 0;
         }
        return response()->json([
            'getFeatureTotal' => $TotalFeature,
            'getPostTotal'    => $TotalPost
         ]);
     }

     public function getpackagefeatureandpost(Request $request){
         $getPackage = Package::where('id',$request->Id)->first();
         return response()->json([
            'getPackage' => $getPackage,
         ]);
     }
    public function store(Request $request)
    {
        $this->validate($request, [
           'vendor_id'             => 'required',
           'package_id'            => 'required',
           'no_of_feature'         => 'required|numeric',
           'no_of_post'            => 'required|numeric',
           'remian_no_of_post'     => 'numeric',
           'remian_no_of_feature'  => 'numeric',
           'offere_validity'       => 'required|numeric',
        ]);

        $currentDate=date('Y-m-d');
        $newDateMonth= date('Y-m-d', strtotime("+30 days"));

        $vendorpackage = VendorPackage::where('vendor_id',$request->vendor_id)->first();

        if($vendorpackage){
            $packagehistory = new PackageHistory();
            $packagehistory->package_id           = $vendorpackage->package_id;
            $packagehistory->vendor_id            = $vendorpackage->vendor_id;
            $packagehistory->no_of_feature        = $vendorpackage->no_of_feature;
            $packagehistory->remain_no_of_feature = $request->remian_no_of_feature;
            $packagehistory->no_of_post           = $vendorpackage->no_of_post;
            $packagehistory->remain_no_of_post    = $request->remian_no_of_post;
            $packagehistory->start_date           = $vendorpackage->start_date;
            $packagehistory->expiry_date          = $currentDate;
            $packagehistory->end_date             = $vendorpackage->end_date;
            $packagehistory->offere_validity      = $vendorpackage->offere_validity;
            $packagehistory->status               = "active";
            $packagehistory->save();

        $vendorpackage->package_id      = $request->package_id;
        $vendorpackage->vendor_id       = $request->vendor_id;
        $vendorpackage->no_of_feature   = $request->no_of_feature + $request->remian_no_of_feature;
        $vendorpackage->no_of_post      = $request->no_of_post + $request->remian_no_of_post;
        $vendorpackage->start_date      = $currentDate;
        $vendorpackage->end_date        = $newDateMonth;
        $vendorpackage->offere_validity = $request->offere_validity;
        $vendorpackage->status          = "active";
        $vendorpackage->save();
        return redirect()->route('admin.subscription.index')->with([
            'status' => 'success',
            'message' => 'Package Subscription has been created'
        ]); 
        }
        else{
            return redirect()->route('admin.subscription.index')->with([
                'status' => 'error',
                'message' => 'Package Subscription has not been created'
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $packages = Package::all();
        $vendors = Vendor::all();
        $vendorpackage = VendorPackage::findOrFail($id);
        $getPackagehistoy = PackageHistory::where('vendor_id',$vendorpackage->vendor_id)->whereDate('expiry_date',$vendorpackage->start_date)->first();
        return view('admin.subscription.edit', compact('vendorpackage','packages','vendors','getPackagehistoy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'package_id'            => 'required',
            'no_of_feature'         => 'required|numeric',
            'no_of_post'            => 'required|numeric',
            'remian_no_of_post'     => 'numeric',
            'remian_no_of_feature'  => 'numeric',
            'offere_validity'       => 'required|numeric',
         ]);

         $vendorpackage = VendorPackage::where('id',$id)->first();
         $vendorpackage->package_id      = $request->package_id;
         $vendorpackage->no_of_feature   = $request->no_of_feature + $request->remian_no_of_feature;
         $vendorpackage->no_of_post      = $request->no_of_post + $request->remian_no_of_post;
         $vendorpackage->offere_validity = $request->offere_validity;
         $vendorpackage->status          = "active";
         $vendorpackage->save();
        return redirect()->route('admin.subscription.index')->with([
            'status' => 'success',
            'message' => 'Package subscription has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Package::findOrFail($id);
        $package->delete();

        return redirect(route('admin.package.index'))->with(['status' => 'success', 'message' => 'Package Deleted Successfully!']);
    }

    public function packagehistory()
    {
        $packagehistories = PackageHistory::with('vendor')->groupBy('vendor_id')->selectRaw('*,count(id) as total_package')->get();
        return view('admin.subscription.packagehistory', compact('packagehistories'));
    }

    public function vendorpackagehistory($id)
    {
        $packagehistories = PackageHistory::with('vendor','package')->where('vendor_id',$id)->get();
        return view('admin.subscription.vendorpackagehistory', compact('packagehistories'));
    }
}
