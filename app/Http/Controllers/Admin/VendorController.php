<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Vendor;
use App\User;


class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::with('user')->get();
        return view('admin.vendor.index', compact('vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vendor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vendor = Vendor::with('user')->where('id',$id)->first();
        return view('admin.vendor.show', compact('vendor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::findOrFail($id);
        $user     = User::where('id',$vendor->user_id)->first();
        $user->delete();
        $vendor->delete();
        return redirect(route('admin.vendor.index'))->with(['status' => 'success', 'message' => 'Vendor Deleted Successfully!']);
    }

    public function changestatus(Request $request)
    {
        $vendor = Vendor::findOrFail($request->id);
        $vendor->status = $request->status == "true" ? 1 : 0;
        $vendor->save();
        $user = User::findOrFail($vendor->user_id);
        $user->user_status = $vendor->status;
        $user->save();
        return response()->json([
           'status' => 'success'
        ]);
    }

    public function changevarification(Request $request)
    {
        $vendor = Vendor::findOrFail($request->id);
        $vendor->verfication_status = $request->verfication_status;
        $vendor->save();
        $user = User::findOrFail($vendor->user_id);
        $user->status = $vendor->verfication_status == "Verfied"?"active":"deactive";
        $user->save();
        return response()->json([
           'status' => 'success'
        ]);
    }
}
