<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Ad;
use App\Product;
use App\SubCatagory;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads=Ad::all();
        return view('admin.ads.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $product=Product::all();
        $sub_catagory=SubCatagory::all();
        return view('admin.ads.create',compact('product','sub_catagory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'screen_id'        => 'required|numeric',
           'type'             => 'required|string',
           'type_id'          => 'nullable|numeric',
           'ad_image'         => 'image|mimes:png,jpg,jpeg|max:5120|required',
        ]);

        $imageName=null;
        if($request->hasFile('ad_image'))
        {
            $imageName = $request->file('ad_image')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'adimages', $request->file('ad_image'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $ad = new  Ad();
         $ad->screen_id        = $request->screen_id;
         $ad->type             = $request->type;
         $ad->type_id          = $request->type_id==null?0:$request->type_id;
         $ad->ad_image_path    = $imageName;
         $ad->external_link    = $request->external_link;
         $ad->save();
        return redirect()->route('admin.ads.index')->with([
            'status' => 'success',
            'message' => 'Ad has been created'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Ad::findOrFail($id);
        $product=Product::all();
        $subcatagory=SubCatagory::all();
        return view('admin.ads.edit', compact('ad','product','subcatagory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'screen_id'        => 'required|numeric',
           'type'             => 'required|string',
           'type_id'          => 'nullable|numeric',
           'ad_image'         => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
        ]);

        $imageName=null;
        if($request->hasFile('ad_image'))
        {
            $imageName = $request->file('ad_image')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'adimages', $request->file('ad_image'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $ad = Ad::findOrFail($id);
         $ad->screen_id        = $request->screen_id;
         $ad->type             = $request->type;
         $ad->type_id          = $request->type_id==null?0:$request->type_id;
         $ad->ad_image_path    = $imageName == null ? $ad->ad_image_path : $imageName;
         $ad->external_link    = $request->external_link;
         $ad->save();
        return redirect()->route('admin.ads.index')->with([
            'status' => 'success',
            'message' => 'Ad has been updated Successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Ad::findOrFail($id);
        $ad->delete();

        return redirect(route('admin.ads.index'))->with(['status' => 'success', 'message' => 'Ad Deleted Successfully!']);
    }
}
