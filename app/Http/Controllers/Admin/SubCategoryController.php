<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\SubCatagory;
use App\Catagory;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories=SubCatagory::with('Category')->get();
        return view('admin.subcategories.index', compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Catagory::all();
        return view('admin.subcategories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'catagory_id'            => 'required',
           'sub_catagory_name'      => 'required|string',
           'sub_catagory_slug'      => 'required|string',
           'sub_catagory_img_path'  => 'image|mimes:png,jpg,jpeg|max:5120|required',
           'seo_description'        => 'nullable|string',
           'meta_description'       => 'nullable|string',
           'meta_keyword'           => 'nullable|string',
        ]);

        $imageName=null;
        if($request->hasFile('sub_catagory_img_path'))
        {
            $imageName = $request->file('sub_catagory_img_path')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;
            

            $path = Storage::disk('public')->putFileAs(
                'subcatagoryimages', $request->file('sub_catagory_img_path'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $subcategory = new  SubCatagory();
         $subcategory->catagory_id               = $request->catagory_id;
         $subcategory->sub_catagory_name         = $request->sub_catagory_name;
         $subcategory->sub_catagory_slug         = $request->sub_catagory_slug;
         $subcategory->sub_catagory_description  = $request->sub_catagory_description;
         $subcategory->sub_catagory_img_path     = $imageName;
         $subcategory->show_home_page            = !$request->has('show_home_page')?0:1;
         $subcategory->seo_description           = $request->seo_description;
         $subcategory->meta_description          = $request->meta_description==null?$request->sub_catagory_name:$request->meta_description;
         $subcategory->meta_keyword              = $request->meta_keyword==null?$request->sub_catagory_name:$request->meta_keyword;
         $subcategory->save();
        return redirect()->route('admin.subcategories.index')->with([
            'status' => 'success',
            'message' => 'Sub Category has been created'
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = SubCatagory::findOrFail($id);
        $categories    = Catagory::all();
        return view('admin.subcategories.edit', compact('categories','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'catagory_id'            => 'required',
           'sub_catagory_name'      => 'required|string',
           'sub_catagory_slug'      => 'required|string',
           'sub_catagory_img_path'  => 'image|mimes:png,jpg,jpeg|max:5120|nullable',
           'seo_description'        => 'nullable|string',
           'meta_description'       => 'nullable|string',
           'meta_keyword'           => 'nullable|string',
        ]);

        $imageName=null;
        if($request->hasFile('sub_catagory_img_path'))
        {
            $imageName = $request->file('sub_catagory_img_path')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;
            $path = Storage::disk('public')->putFileAs(
                'subcatagoryimages', $request->file('sub_catagory_img_path'), $imageName
            );
            $imageName= 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

         $subcategory = SubCatagory::findOrFail($id);
         $subcategory->catagory_id               = $request->catagory_id;
         $subcategory->sub_catagory_name         = $request->sub_catagory_name;
         $subcategory->sub_catagory_slug         = $request->sub_catagory_slug;
         $subcategory->sub_catagory_description  = $request->sub_catagory_description;
         $subcategory->sub_catagory_img_path     = $imageName==null?$subcategory->sub_catagory_img_path:$imageName;
         $subcategory->show_home_page            = !$request->has('show_home_page')?0:1;
         $subcategory->seo_description           = $request->seo_description;
         $subcategory->meta_description          = $request->meta_description==null?$request->sub_catagory_name:$request->meta_description;
         $subcategory->meta_keyword              = $request->meta_keyword==null?$request->sub_catagory_name:$request->meta_keyword;
         $subcategory->save();
        return redirect()->route('admin.subcategories.index')->with([
            'status' => 'success',
            'message' => 'Sub Category has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = SubCatagory::findOrFail($id);
        $subcategory->delete();

        return redirect(route('admin.subcategories.index'))->with(['status' => 'success', 'message' => 'Sub Category Deleted Successfully!']);
    }
}
