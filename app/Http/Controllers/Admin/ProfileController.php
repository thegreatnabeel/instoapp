<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Owner;
use App\User;
use Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $owners=Owner::all();
        return view('admin.owner.index', compact('owners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.owner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'first_name'              => 'required|string',
           'email'                   => 'required|unique:users,email',
           'password'                => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
           'last_name'               => 'required|nullable',
           'primary_phone_number'    => 'required|numeric',
           'picture_path'            => 'image|mimes:png,jpg,jpeg|max:5120|required',
           'secondary_phone_number'  => 'nullable|string',
           'postal_address'          => 'nullable|string',
           'base_address'            => 'required|string',
        ]);

        $imageName=null;
        if($request->hasFile('picture_path'))
        {
            $imageName = $request->file('picture_path')->getClientOriginalName();
            $imageName = str_replace(" ","",$imageName);
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'profileimages', $request->file('picture_path'), $imageName
            );
            $imageName = 'storage/'.$path;
        }
        else
        {
            $imageName = null;
        }

        $user =new User();
        $user->name         = $request->first_name.''.$request->last_name;
        $user->user_type    = "admin";
        $user->phone_number = $request->primary_phone_number;
        $user->email        = strtolower($request->email);
        $user->password     = !empty($request->password) ? bcrypt($request->password):null;
        $user->status       = "active";
        $user->save();
        
         $owner = new  Owner();
         $owner->first_name              = $request->first_name;
         $owner->user_id	             = $user->id;
         $owner->last_name               = $request->last_name;
         $owner->primary_phone_number    = $request->primary_phone_number;
         $owner->picture_path            = $imageName;
         $owner->secondary_phone_number  = $request->secondary_phone_number;
         $owner->postal_address          = $request->postal_address;
         $owner->base_address            = $request->base_address;
         $owner->save();
        return redirect()->route('admin.owner.index')->with([
            'status' => 'success',
            'message' => 'Owner has been created'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $owner = Owner::with('User')->where('id',$id)->first(); 
        return view('admin.owner.edit', compact('owner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name'              => 'required|string',
            'password'                => 'nullable|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'last_name'               => 'required|nullable',
            'primary_phone_number'    => 'required|numeric',
            'picture_path'            => 'image|mimes:png,jpg,jpeg|max:5120',
            'secondary_phone_number'  => 'nullable|string',
            'postal_address'          => 'nullable|string',
            'base_address'            => 'required|string',
         ]);
 
         $imageName=null;
         if($request->hasFile('picture_path'))
         {
             $imageName = $request->file('picture_path')->getClientOriginalName();
             $imageName = str_replace(" ","",$imageName);
             $imageName = time().'_'.$imageName;
 
             $path = Storage::disk('public')->putFileAs(
                 'profileimages', $request->file('picture_path'), $imageName
             );
             $imageName= 'storage/'.$path;
         }
         else
         {
             $imageName = null;
         }
         $owner = Owner::findOrFail($id);
         
         $user = User::where('id',$owner->user_id)->first();
         if($request->password!=null){
            $user->name         = $request->first_name.''.$request->last_name;
            $user->user_type    = "admin";
            $user->phone_number = $request->primary_phone_number;
            $user->password     = !empty($request->password) ? bcrypt($request->password):null;
            $user->save();
         }
         else{
            $user->name         = $request->first_name.''.$request->last_name;
            $user->user_type    = "admin";
            $user->phone_number = $request->primary_phone_number;
            $user->save();
         }         
          $owner->first_name              = $request->first_name;
          $owner->last_name               = $request->last_name;
          $owner->primary_phone_number    = $request->primary_phone_number;
          $owner->picture_path            = $imageName==null?$owner->picture_path:$imageName;
          $owner->secondary_phone_number  = $request->secondary_phone_number;
          $owner->postal_address          = $request->postal_address;
          $owner->base_address            = $request->base_address;
          $owner->save(); 
         return redirect()->route('admin.owner.index')->with([
             'status' => 'success',
             'message' => 'Owner has been updated'
         ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $owner = Owner::findOrFail($id);
        $owner->delete();
        return redirect(route('admin.owner.index'))->with(['status' => 'success', 'message' => 'Owner Deleted Successfully!']);
    }
}
