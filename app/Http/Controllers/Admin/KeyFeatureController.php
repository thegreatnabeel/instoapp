<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\SubCatagory;
use App\KeyFeature;

class KeyFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $keyfeatures=KeyFeature::with('SubCatagory')->get();
        return view('admin.keyfeature.index', compact('keyfeatures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subcategories=SubCatagory::all();
        return view('admin.keyfeature.create', compact('subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'sub_catagory_id' => 'required',
           'title'           => 'required|string',
        ]);

         $keyfeature = new  KeyFeature();
         $keyfeature->sub_catagory_id    = $request->sub_catagory_id;
         $keyfeature->title              = $request->title;
         $keyfeature->show_in_filter     = !$request->has('show_in_filter')?0:1;
         $keyfeature->save();
        return redirect()->route('admin.keyfeature.index')->with([
            'status' => 'success',
            'message' => 'Key Feature has been created'
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $keyfeature     = KeyFeature::findOrFail($id);
        $subcategories  = SubCatagory::all();
        return view('admin.keyfeature.edit', compact('keyfeature','subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'sub_catagory_id' => 'required',
           'title'           => 'required|string',
        ]);

         $keyfeature = KeyFeature::findOrFail($id);
         $keyfeature->sub_catagory_id    = $request->sub_catagory_id;
         $keyfeature->title              = $request->title;
         $keyfeature->show_in_filter     = !$request->has('show_in_filter')?0:1;
         $keyfeature->save();
        return redirect()->route('admin.keyfeature.index')->with([
            'status' => 'success',
            'message' => 'Key Feature has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $keyfeature = KeyFeature::findOrFail($id);
        $keyfeature->delete();

        return redirect(route('admin.keyfeature.index'))->with(['status' => 'success', 'message' => 'Key Feature Deleted Successfully!']);
    }
}
