<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Cms;

class CMSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cms=Cms::all();
        return view('admin.cms.index', compact('cms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'page_title'       => 'required|string',
           'page_content'     => 'required|string',
           'seo_description' => 'nullable|string',
           'meta_description' => 'nullable|string',
           'meta_keyword'     => 'nullable|string',
        ]);

         $cms = new  Cms();
         $cms->page_title         = $request->page_title;
         $cms->page_content       = $request->page_content;
         $cms->seo_description    = $request->seo_description;
         $cms->meta_description   = $request->meta_description==null?$request->page_title:$request->meta_description;
         $cms->meta_keyword       = $request->meta_keyword==null?$request->page_title:$request->meta_keyword;
         $cms->save();
        return redirect()->route('admin.cms.index')->with([
            'status' => 'success',
            'message' => 'Page has been created'
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cm = Cms::findOrFail($id);
        return view('admin.cms.edit', compact('cm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'page_title'     => 'required|string',
           'page_content'   => 'required|string',
           'seo_description' => 'nullable|string',
           'meta_description' => 'nullable|string',
           'meta_keyword'     => 'nullable|string',
        ]);

         $cms = Cms::findOrFail($id);
         $cms->page_title         = $request->page_title;
         $cms->page_content       = $request->page_content;
         $cms->seo_description    = $request->seo_description;
         $cms->meta_description   = $request->meta_description==null?$request->page_title:$request->meta_description;
         $cms->meta_keyword       = $request->meta_keyword==null?$request->page_title:$request->meta_keyword;
         $cms->save();
        return redirect()->route('admin.cms.index')->with([
            'status' => 'success',
            'message' => 'Page has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cms = Cms::findOrFail($id);
        $cms->delete();

        return redirect(route('admin.cms.index'))->with(['status' => 'success', 'message' => 'Page Deleted Successfully!']);
    }
}
