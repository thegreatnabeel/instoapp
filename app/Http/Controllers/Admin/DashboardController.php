<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Product;
use App\Brand;
use App\Catagory;
use App\Vendor;
use App\Customer;
use App\Order;
use App\OffereDetail;
use Session;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $CurrentDate=date('Y-m-d');
        $total_products = 0;
        $total_brands   = 0;
        $total_category = 0;
        try{
            if(Auth()->user()->user_type == "admin"){
                $total_products   = Product::count();
                $total_brands     = Brand::count();
                $total_category   = Catagory::count();
                $total_customers  = Customer::count();
                $total_vendors    = Vendor::count();
                $total_orders     = Order::count();
                $total_offeres     = OffereDetail::count();
                $total_new_orders = Order::where('status','pending')->count();

            }
        }
        catch(Exception $e)
     {
        $status="error";
        $message=$e->getMessage();
        Auth::logout();
        return view('auth.login'); 
     }
        return view('/admin/home',compact('total_products','total_brands','total_category','total_customers','total_vendors','total_orders','total_new_orders','total_offeres'));
    }

     public function showorder(Request $request){
        Session::put('checkorderlink',$request->value);
        return response()->json( array('status' => true));
    }
}
