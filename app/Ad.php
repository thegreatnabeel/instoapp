<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    //
    public function getAdImagePathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }
    
}
