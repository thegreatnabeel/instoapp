<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\RequestOffere;

class RequestOffereExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RequestOffereExpired:RequestOffereExpired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Request Offere Expired After One Day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = \Carbon\Carbon::today();
        $this->info('Fetching Records.....');
        RequestOffere::whereDate('offere_datetime', '<=', $date)->chunk(100, function ($request_offeres) {
            $request_offeres->each(function ($request_offere) {
                $request_offere->delete();
            });
        });

        $this->info('Command Run Successfully.....');
    }
}
