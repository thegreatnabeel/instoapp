<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use SoftDeletes;
    public function SubCatagory()
    {
        return $this->belongsToMany(SubCatagory::class, 'sub_catagories','vendor_id','sub_catagory_id');
    }

    public function SubArea()
    {
        return $this->belongsToMany(SubArea::class, 'vendor_sub_areas','vendor_id','sub_area_id');
    }

    public function Product()
    {
        return $this->belongsToMany(Product::class, 'ofere_masters','product_id','vendor_id');
    }

    public function Package()
    {
        return $this->belongsToMany(Package::class, 'vendor_packages','package_id','vendor_id');
    }

    public function packagehistory()
    {
        return $this->belongsToMany(Package::class, 'package_histories','package_id','vendor_id');
    }

    public function getPicturePathAttribute($path)
    {
        if($path){
        return asset(str_replace('public/', 'storage/', $path));
        }
        else{
            return null;
        }
    }
    public function getCnicFrontImgPathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }

    public function getCnicBackImgPathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }
    public function user()
    {
       return $this->belongsTo(User::Class,'user_id','id')->withTrashed();
    }


    public function GiveoffereMaster()
    {
        return $this->hasMany(GiveOffereMaster::class);
    }
}
