<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiveOffereMaster extends Model
{
    //
    public function CustomerRequestOffere()
    {
        return $this->belongsTo(RequestOffere::class,'offere_request_id','id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id','id')->withTrashed();
    }

    public function GiveoffereDetail()
    {
        return $this->hasMany(GiveOffereDetail::class);
    }

    public function vendorofferecount()
    {
        return $this->hasMany(VendorOffereCount::class);
    }
}
