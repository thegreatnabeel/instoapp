<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    //
    public function OffereDetail()
    {
        return $this->belongsTo(OffereDetail::class,'offere_id','id');
    }

    public function Customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id')->withTrashed();
    }
}
