<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    //

    public function Product()
    {
        return $this->belongsToMany(Product::class, 'request_offeres','customer_id','product_id')->withPivot('customer_id');
    }

    public function WishListOffere()
    {
        return $this->belongsToMany(OffereDetail::class, 'wish_lists','offere_id','customer_id');
    }

    public function getPicturePathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }

    public function getCnicFrontImgPathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
            
    }

    public function getCnicBackImgPathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
            
    }
    public function user()
    {
       return $this->belongsTo(User::Class);
    }
    public function subarea()
    {
    	return $this->belongsTo(SubArea::class,'sub_area_id','id');
    }

    public function OrderOfereDetail()
    {
    return $this->belongsToMany(OffereDetail::class, 'orders','cusstomer_id','offere_id');
    }

}
