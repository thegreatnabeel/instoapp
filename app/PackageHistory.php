<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageHistory extends Model
{
    public function vendor()
    {
        return $this->belongsTo(Vendor::class)->withTrashed();
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}
