<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoGraphic extends Model
{
   public function getInfoGraphicPathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }
}
