<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Catagory extends Model
{
	use SoftDeletes;
    public function SubCatagory()
    {
        return $this->hasMany(SubCatagory::class);
    }

    public function Product()
    {
        return $this->hasMany(Product::class,'catagory_id','id');
    }
    public function gethotproduct(){
        return $this->Product()->where('hot_product',true)->with(['ProductImage','ProductImage.ProductResizeImage'])->orderby('post_instalment','desc')->limit(8);
    }

    public function gethotproductbycategoryid(){
        return $this->Product()->with(['ProductImage','ProductImage.ProductResizeImage'])->limit(12);
    }

    public function getCatagoryImgPathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }
}
