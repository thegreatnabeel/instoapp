<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function Vendor()
    {
        return $this->belongsToMany(Vendor::class, 'vendor_packages','vendor_id','package_id');
    }

    public function vendorhistory()
    {
        return $this->belongsToMany(Vendor::class, 'package_histories','vendor_id','package_id');
    }
}
