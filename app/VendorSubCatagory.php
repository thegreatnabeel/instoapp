<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorSubCatagory extends Model
{
    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id','id')->withTrashed();
    }

    public function SubCategory()
    {
        return $this->belongsTo(SubCatagory::class,'sub_catagory_id','id');
    }
}
