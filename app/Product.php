<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Product extends Model
{
    use SoftDeletes;
    public function getRouteKeyName()
    {
       return 'product_slug';
     }
    public function ProductImage()
    {
        return $this->hasMany(ProductImage::class);
    }

     public function Brand()
    {
    	return $this->belongsTo(Brand::class,'brand_id','id');
    }

    public function SubCatagory()
    {
    	return $this->belongsTo(SubCatagory::class,'sub_catagory_id','id');
    }

    public function Catagory()
    {
    	return $this->belongsTo(Catagory::class,'catagory_id','id');
    }
    
    public function SubKeyFeature()
    {
    return $this->belongsToMany(KeyFeature::class, 'sub_key_features','product_id','key_feature_id')->withPivot(['id','sub_key_feature_value']);
    }

    public function Customer()
    {
       return $this->belongsToMany(Customer::class, 'request_offeres','product_id','customer_id')->withPivot('status');
    }

    public function Vendor()
    {
        return $this->belongsToMany(Vendor::class, 'ofere_masters','product_id','vendor_id');
    }

    public function OffereMaster()
    {
        return $this->hasMany(OffereMaster::class);
    }

    public function getofferemaster()
    {
        return $this->OffereMaster()->with('getmininstallment');
    }

    public function Category()
    {
    	return $this->belongsTo(Catagory::class,'catagory_id','id');
    }

    public function subkeydata()
    {
        return $this->hasMany(SubKeyFeature::class);
    }

    public function Order()
    {
        return $this->hasMany(Order::class);
    }
    
}
