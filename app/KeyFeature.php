<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeyFeature extends Model
{
    //
    public function SubCatagory()
    {
    	return $this->belongsTo(SubCatagory::class,'sub_catagory_id','id');
    }

    public function SubKeyFeature()
    {
        return $this->belongsToMany(Product::class, 'sub_key_features')->withPivot('id');
    }
}
