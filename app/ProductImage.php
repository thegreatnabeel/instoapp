<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
   public function Product()
    {
    	return $this->belongsTo(Product::class,'product_id','id');
    }

    public function ProductResizeImage()
    {
        return $this->hasMany(ProductResizeImage::class)->orderBy('id','asc');
    }

    public function getProductImageAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }
}
