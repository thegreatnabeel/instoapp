<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class RequestOffere extends Model
{
    use SoftDeletes;
    public function Product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function getProduct()
    {
        return $this->Product()->with(['ProductImage'])->withTrashed();
    }

    public function Customer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');
    }
    public function GiveoffereMaster()
    {
        return $this->hasMany(GiveOffereMaster::class,'offere_request_id','id');
    }

    public function getGiveoffereMaster()
    {
        return $this->GiveoffereMaster()->with(['vendor','GiveoffereDetail.getofferedetail'])->has('GiveoffereDetail.getofferedetail');
    }

    public function getOffereDatetimeAttribute()
    {
        return Carbon::parse($this->attributes['offere_datetime'])->setTimezone('Asia/Karachi')->format('Y-m-d H:i:s');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone('Asia/Karachi')->format('Y-m-d H:i:s');
    }
}
