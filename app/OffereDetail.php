<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OffereDetail extends Model
{
	use SoftDeletes;
    public function OffereMaster()
    {
    	return $this->belongsTo(OffereMaster::class,'offere_master_id','id');
    }

    public function WishListCustomer()
    {
        return $this->belongsToMany(Customer::class, 'wish_lists','customer_id','offere_id');
    }

    public function OrderCustomer()
    {
    return $this->belongsToMany(Customer::class, 'orders','offere_id','cusstomer_id');
    }

    public function GiveoffereDetail()
    {
        return $this->hasMany(GiveoffereDetail::class);
    }
}
