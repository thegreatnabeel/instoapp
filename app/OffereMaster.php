<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OffereMaster extends Model
{
    public function OffereDetail()
    {
        return $this->hasMany(OffereDetail::class);
    }

    public function getmininstallment()
    {
        return $this->OffereDetail()->where('post_on_instalment',1)->orderBy('installment')->limit(2);
    }

    public function expiredoffere()
    {
        $currentdate = date('Y-m-d');
        return $this->OffereDetail()->where('offere_valid_date','<',$currentdate);
    }

    public function activeoffere()
    {
        $currentdate = date('Y-m-d');
        return $this->OffereDetail()->where('offere_valid_date','>=',$currentdate);
    }

    public function Vendor()
    {
        return $this->belongsTo(Vendor::class)->withTrashed();
    }

    public function Product()
    {
        return $this->belongsTo(Product::class);
    }
}
