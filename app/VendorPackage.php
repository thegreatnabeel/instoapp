<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorPackage extends Model
{
    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id','id')->withTrashed();
    }

    public function package()
    {
        return $this->belongsTo(Package::class,'package_id','id');
    }
}
