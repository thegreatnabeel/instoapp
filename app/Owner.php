<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    public function getPicturePathAttribute($path)
    {
        if($path){
            return asset(str_replace('public/', 'storage/', $path));
            }
            else{
                return null;
            }
    }

    public function User()
    {
       return $this->belongsTo(User::Class);
    }
}
