<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
     use HasApiTokens, Notifiable;
     use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function userType()
    {
        if($this->user_type == 'admin')
            return $this->hasOne(Owner::class,'user_id','id');
        elseif ($this->user_type == 'vendor')
            return $this->hasOne(Vendor::class,'user_id','id')->withTrashed();
        elseif($this->user_type == 'client')
            return $this->hasOne(Customer::class,'user_id','id');
    }

    public function owner()
    {
       return $this->hasOne(Owner::Class,'user_id');
    }
    public function vendor()
    {
       return $this->hasOne(Vendor::Class,'user_id');
    }
    public function customer()
    {
       return $this->hasOne(Customer::Class,'user_id');
    }
}