$(document).ready(function() {
    "use strict";
    $(window).scroll(function() {
        var getWidth = $(window).scrollTop();
        if (getWidth > 70) {
            $(".navbar-fixed-top").addClass("sticky");
            $(".topBar").slideUp();
        } else {
            $(".navbar-fixed-top").removeClass("sticky");
            $(".topBar").slideDown();
        }
    });
    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).siblings('.panel-heading').addClass('active');
      });
    
      $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).siblings('.panel-heading').removeClass('active');
      });
    var windowWidth = $(window).width();
    if (windowWidth > 768) {
        $(".dropdown").on("mouseenter mouseleave tap", function() {
            $(this).toggleClass("open");
        });
    } else {
        $(".dropdown").on("click tap", function() {
            $(this).toggleClass("open");
        });
    }
    $("#showDropdown").click(function() {
        $(".dropDownSpecsMenu").fadeIn();
    });
    $("#closeDropDownSpecsMenu").click(function() {
        $(".dropDownSpecsMenu").fadeOut();
    });
    $(".panel-title > a").click(function() {
        $(this)
            .find("i")
            .toggleClass("fa-plus fa-minus")
            .closest("panel")
            .siblings("panel")
            .find("i")
            .removeClass("fa-minus")
            .addClass("fa-plus");
    });
    $("#bannerCarousel").owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 3000,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $("#proSlider").owlCarousel({
        loop: true,
        margin: 0,
        center: true,
        stagePadding: 350,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1,
                stagePadding: 30
            },
            600: {
                items: 1,
                stagePadding: 150
            },
            1000: {
                items: 1
            }
        }
    });
    $("#featuredDeals").owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        autoplay: true,
        nav: true,
        navText: [
            "<i class='glyphicon glyphicon-chevron-left'></i>",
            "<i class='glyphicon glyphicon-chevron-right'></i>"
        ],
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1
            },
            360: {
                items: 1
            },
            400: {
                items: 2
            },
            500: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });
});
