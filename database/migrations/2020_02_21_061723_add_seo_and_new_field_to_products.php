<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoAndNewFieldToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('info')->nullable()->after('product_slug');
            $table->string('info1')->nullable()->after('info');
            $table->integer('get_offere_number')->nullable()->after('product_feature');
            $table->integer('request_offere_number')->nullable()->after('get_offere_number');
            $table->boolean('new_arrival')->nullable()->after('request_offere_number');
            $table->string('seo_description')->nullable()->after('new_arrival');
            $table->string('meta_description')->nullable()->after('seo_description');
            $table->string('meta_keyword')->nullable()->after('meta_description');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
