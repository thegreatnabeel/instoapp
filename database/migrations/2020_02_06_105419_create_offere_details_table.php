<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffereDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offere_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('offere_master_id');
            $table->decimal('Advance',30,2);
            $table->decimal('month',30,2);
            $table->decimal('installment',30,2);
            $table->decimal('total_price',30,2);
            $table->decimal('cash_price',30,2);
            $table->boolean('featured');
            $table->boolean('post_on_instalment');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offere_details');
    }
}
