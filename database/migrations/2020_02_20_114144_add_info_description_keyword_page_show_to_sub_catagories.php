<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfoDescriptionKeywordPageShowToSubCatagories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_catagories', function (Blueprint $table) {
            $table->string('info')->nullable()->after('sub_catagory_description');
            $table->string('seo_description')->nullable()->after('info');
            $table->boolean('show_home_page')->default(0)->after('seo_description');
            $table->string('meta_description')->nullable()->after('show_home_page');
            $table->string('meta_keyword')->nullable()->after('meta_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_catagories', function (Blueprint $table) {
            //
        });
    }
}
