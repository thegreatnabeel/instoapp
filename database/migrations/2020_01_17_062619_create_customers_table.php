<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('primary_phone_number');
            $table->string('secondary_phone_number')->nullable();
            $table->string('opt_code');
            $table->text('postal_address')->nullable();
            $table->date('dob')->nullable();
            $table->string('age')->nullable();
            $table->string('base_address')->nullable();
            $table->integer('sub_area_id');
            $table->string('picture_path')->nullable();
            $table->string('cnic_front_img_path')->nullable();
            $table->string('cnic_back_img_path')->nullable();
            $table->string('verfication_status');
            $table->string('register_type');
            $table->boolean('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}