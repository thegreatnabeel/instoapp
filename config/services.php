<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'facebook' => [
    'client_id' => '2979197398760097',
    'client_secret' => '8f5ee6851c78ba38ef69c21def9a928a',
    'redirect' => 'https://www.abc.instalment.pk/callback/facebook',
    ],
    'google' => [
        'client_id' => '374517416932-gp4g2p3320ml5f8f2iub3mb10jne0bve.apps.googleusercontent.com',
        'client_secret' => '8V8Lm-DWe9UbkBf_HEBtRQxh',
        'redirect' => 'https://www.abc.instalment.pk/callback/google',
    ],

];
