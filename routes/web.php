<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Web\HomeController@index')->name('web.home');

Route::post('/websearchproduct', 'Web\HomeController@websearchproduct')->name('websearchproduct');

Route::get('/category/{category_slug}', 'Web\CategoryController@index')->name('category');

Route::post('load/featureandcategory', 'Web\HomeController@loadfeatureandcategory')->name('load.featureandcategory');


Route::post('/store', 'Web\HomeController@store')->name('store');

Route::post('load/category', 'Web\HomeController@loadcategory')->name('load.category');

Route::post('load/featureandcategoryforcategorypage', 'Web\CategoryController@loadfeatureandcategoryforcategorypage')->name('load.featureandcategoryforcategorypage');

Route::post('load/product', 'Web\CategoryController@loadproduct')->name('load.product');

Route::post('load/filter/product', 'Web\CategoryController@loadfilterproduct')->name('load.filter.product');

Route::get('/product/{product_slug}', 'Web\ProductController@index')->name('product');

Route::post('load/productbylcation', 'Web\ProductController@loadproductbylcation')->name('load.productbylcation');

Route::post('load/offerebysort', 'Web\ProductController@loadofferebysort')->name('load.offerebysort');

Route::post('load/offerebyfilter', 'Web\ProductController@loadofferebyfilter')->name('load.offerebyfilter');

Route::post('load/moreoffere', 'Web\ProductController@loadmoreoffere')->name('load.moreoffere');

Route::get('/signup', 'Web\RegisterController@index')->name('signup');

Route::get('/login', 'Web\LoginController@index')->name('web.login');

Route::post('/login/login', 'Web\LoginController@login')->name('web.login.login');

Route::post('/login/loginbyemail', 'Web\LoginController@loginbyemail')->name('web.login.loginbyemail');

Route::post('/login/logout', 'Web\LoginController@logout')->name('web.login.logout');

Route::get('/brand/{brand_slug}', 'Web\BrandController@index')->name('brand');

Route::post('load/featureandbrandforbrandpage', 'Web\BrandController@loadfeatureandbrandforbrandpage')->name('load.featureandbrandforbrandpage');

Route::post('load/brandwiseproduct', 'Web\BrandController@loadbrandwiseproduct')->name('load.brandwiseproduct');

Route::post('adddeletewishlist', 'Web\WishListController@adddeletewishlist')->name('adddeletewishlist');

Route::get('/subcategory/{subcategory_slug}', 'Web\SubCategoryController@index')->name('subcategory');

Route::post('load/featureandproductforsubcategorypage', 'Web\SubCategoryController@loadfeatureandproductforsubcategorypage')->name('load.featureandproductforsubcategorypage');

Route::post('load/subcategory/product', 'Web\SubCategoryController@loadsubcategoryproduct')->name('load.subcategory.product');

Route::post('load/subcategory/filter/product', 'Web\SubCategoryController@loadsubcategoryfilterproduct')->name('load.subcategory.filter.product');

Route::get('/dashboard', 'Web\DashboardController@index')->name('web.dashboard');

Route::get('/vendor/{id}', 'Web\VendorController@index')->name('web.vendor');

Route::post('load/featureandproductforvendorpage', 'Web\VendorController@loadfeatureandproductforvendorpage')->name('load.featureandproductforvendorpage');

Route::post('load/vendorwiseproduct', 'Web\VendorController@loadvendorwiseproduct')->name('load.vendorwiseproduct');

Route::get('/search/{search_slug}', 'Web\SearchController@index')->name('web.search');

Route::post('load/featureandproductforsearchpage', 'Web\SearchController@loadfeatureandproductforsearchpage')->name('load.featureandproductforsearchpage');

Route::post('load/searchwiseproduct', 'Web\SearchController@loadsearchwiseproduct')->name('load.searchwiseproduct');

Route::post('getroute', 'Web\HomeController@getroute')->name('getroute');

Route::post('order', 'Web\ProductController@order')->name('order');

Route::post('web/sendotp', 'Web\RegisterController@sendotp')->name('web.sendotp');

Route::post('web/checkotp', 'Web\RegisterController@checkotp')->name('web.checkotp');

Route::post('web/customerregistration', 'Web\RegisterController@customerregistration')->name('web.customerregistration');

Route::post('web/subarea', 'Web\RegisterController@subarea')->name('web.subarea');

Route::get('web/contact-us', 'Web\HomeController@contactus')->name('web.contactus');

Route::get('web/about-us', 'Web\HomeController@aboutus')->name('web.aboutus');

Route::get('web/faq', 'Web\HomeController@faq')->name('web.faq');

Route::get('web/privacy-policy', 'Web\HomeController@privacy')->name('web.privacy-policy');

Route::get('web/term-conditions', 'Web\HomeController@termandcondition')->name('web.term-conditions');

Route::get('web/why-buy-from-us', 'Web\HomeController@whybuyfromus')->name('web.why-buy-from-us');

Route::post('web/customerprofileupdate', 'Web\RegisterController@customerprofileupdate')->name('web.customerprofileupdate');

Route::get('/forget', 'Web\ForgetController@index')->name('web.forget');

Route::post('web/forget/sendotp', 'Web\ForgetController@forgetsendotp')->name('web.forget.sendotp');

Route::post('web/.forget/checkotp', 'Web\ForgetController@forgetcheckotp')->name('web.forget.checkotp');

Route::post('web/forget/password', 'Web\ForgetController@customerforgetPassword')->name('web.forget.customerforgetPassword');

Route::get('web/contact-us', 'Web\ContactUsController@contactus')->name('web.contactus');

Route::post('web/contact-us/send', 'Web\ContactUsController@store')->name('web.contactus.send');

Route::get('admin/', function () {
    return view('auth.login');
});
Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
});



//Auth::routes();
Route::get('auth/redirect/{provider}', 'Auth\FacebookLoginController@redirect');
Route::get('callback/{provider}', 'Auth\FacebookLoginController@callback');

Route::post('/sendOTP', 'Auth\LoginController@sendOTP')->name('sendOTP');

Route::middleware(['auth','disablepreventback'])->group(function () {
    
 Route::get('admin/home', 'Admin\DashboardController@index')->name('home');

  Route::resource('/admin/brand', 'Admin\BrandController', [ 'names' => [
            'index'    => 'admin.brand.index',
            'create'   => 'admin.brand.create',
            'store'    => 'admin.brand.store',
            'edit'     => 'admin.brand.edit',
            'update'   => 'admin.brand.update',
            'destroy'  => 'admin.brand.destroy',
        ]
    ]);

  Route::resource('/admin/InfoGraphic', 'Admin\InfoGraphicController', [ 'names' => [
            'index'    => 'admin.InfoGraphic.index',
            'create'   => 'admin.InfoGraphic.create',
            'store'    => 'admin.InfoGraphic.store',
            'edit'     => 'admin.InfoGraphic.edit',
            'update'   => 'admin.InfoGraphic.update',
            'destroy'  => 'admin.InfoGraphic.destroy',
        ]
    ]);

  Route::resource('/admin/ads', 'Admin\AdsController', [ 'names' => [
            'index'    => 'admin.ads.index',
            'create'   => 'admin.ads.create',
            'store'    => 'admin.ads.store',
            'edit'     => 'admin.ads.edit',
            'update'   => 'admin.ads.update',
            'destroy'  => 'admin.ads.destroy',
        ]
    ]);

  Route::resource('/admin/categories', 'Admin\CategoryController', [ 'names' => [
            'index'    => 'admin.categories.index',
            'create'   => 'admin.categories.create',
            'store'    => 'admin.categories.store',
            'edit'     => 'admin.categories.edit',
            'update'   => 'admin.categories.update',
            'destroy'  => 'admin.categories.destroy',
        ]
    ]);

  Route::resource('/admin/subcategories', 'Admin\SubCategoryController', [ 'names' => [
            'index'    => 'admin.subcategories.index',
            'create'   => 'admin.subcategories.create',
            'store'    => 'admin.subcategories.store',
            'edit'     => 'admin.subcategories.edit',
            'update'   => 'admin.subcategories.update',
            'destroy'  => 'admin.subcategories.destroy',
        ]
    ]);

  Route::resource('/admin/product', 'Admin\ProductController', [ 'names' => [
            'index'    => 'admin.product.index',
            'create'   => 'admin.product.create',
            'store'    => 'admin.product.store',
            'edit'     => 'admin.product.edit',
            'update'   => 'admin.product.update',
            'destroy'  => 'admin.product.destroy',
        ]
    ]);
  Route::post('/subcategorybyid', 'Admin\ProductController@subcategorybyid')->name('subcategorybyid');
  Route::post('/keyfeaturebysubid', 'Admin\ProductController@keyfeaturebysubid')->name('keyfeaturebysubid');
  Route::post('/deletegalleryimage', 'Admin\ProductController@deletegalleryimage')->name('deletegalleryimage');
  Route::post('/changenewarrival', 'Admin\ProductController@changenewarrival')->name('changenewarrival');
  Route::post('/changehotproduct', 'Admin\ProductController@changehotproduct')->name('changehotproduct');

  Route::resource('/admin/keyfeature', 'Admin\KeyFeatureController', [ 'names' => [
            'index'    => 'admin.keyfeature.index',
            'create'   => 'admin.keyfeature.create',
            'store'    => 'admin.keyfeature.store',
            'edit'     => 'admin.keyfeature.edit',
            'update'   => 'admin.keyfeature.update',
            'destroy'  => 'admin.keyfeature.destroy',
        ]
    ]);

  Route::resource('/admin/package', 'Admin\PackageController', [ 'names' => [
            'index'    => 'admin.package.index',
            'create'   => 'admin.package.create',
            'store'    => 'admin.package.store',
            'edit'     => 'admin.package.edit',
            'update'   => 'admin.package.update',
            'destroy'  => 'admin.package.destroy',
        ]
    ]);

  Route::resource('/admin/cms', 'Admin\CMSController', [ 'names' => [
            'index'    => 'admin.cms.index',
            'create'   => 'admin.cms.create',
            'store'    => 'admin.cms.store',
            'edit'     => 'admin.cms.edit',
            'update'   => 'admin.cms.update',
            'destroy'  => 'admin.cms.destroy',
        ]
    ]);

    Route::resource('/admin/owner', 'Admin\ProfileController', [ 'names' => [
        'index'    => 'admin.owner.index',
        'create'   => 'admin.owner.create',
        'store'    => 'admin.owner.store',
        'edit'     => 'admin.owner.edit',
        'update'   => 'admin.owner.update',
        'destroy'  => 'admin.owner.destroy',
    ]
]);

Route::resource('/admin/vendor', 'Admin\VendorController', [ 'names' => [
    'index'    => 'admin.vendor.index',
    'create'   => 'admin.vendor.create',
    'store'    => 'admin.vendor.store',
    'edit'     => 'admin.vendor.edit',
    'update'   => 'admin.vendor.update',
    'show'   => 'admin.vendor.show',
    'destroy'  => 'admin.vendor.destroy',
]
]);

Route::post('/changestatus', 'Admin\VendorController@changestatus')->name('changestatus');
Route::post('/changevarification', 'Admin\VendorController@changevarification')->name('changevarification');

Route::resource('/admin/customer', 'Admin\CustomerController', [ 'names' => [
    'index'    => 'admin.customer.index',
    'create'   => 'admin.customer.create',
    'store'    => 'admin.customer.store',
    'edit'     => 'admin.customer.edit',
    'update'   => 'admin.customer.update',
    'destroy'  => 'admin.customer.destroy',
    'show'     => 'admin.customer.show',
]
]);
Route::post('/customerchangestatus', 'Admin\CustomerController@customerchangestatus')->name('customerchangestatus');
Route::post('/cutomerchangevarification', 'Admin\CustomerController@cutomerchangevarification')->name('cutomerchangevarification');


Route::resource('/admin/subscription', 'Admin\SubscriptionController', [ 'names' => [
    'index'    => 'admin.subscription.index',
    'create'   => 'admin.subscription.create',
    'store'    => 'admin.subscription.store',
    'edit'     => 'admin.subscription.edit',
    'update'   => 'admin.subscription.update',
    'destroy'  => 'admin.subscription.destroy',
]
]);

Route::post('/getremainfeatureandpost', 'Admin\SubscriptionController@getremainfeatureandpost')->name('getremainfeatureandpost');

Route::post('/getpackagefeatureandpost', 'Admin\SubscriptionController@getpackagefeatureandpost')->name('getpackagefeatureandpost');

Route::get('admin/packagehistory', 'Admin\SubscriptionController@packagehistory')->name('packagehistory');

Route::get('admin/vendorpackagehistory/{id}', 'Admin\SubscriptionController@vendorpackagehistory')->name('vendorpackagehistory');

Route::get('/wishlist', 'Web\WishlistController@index')->name('web.wishlist');

Route::resource('/admin/Order', 'Admin\OrderController', [ 'names' => [
    'index'    => 'admin.Order.index',
    'create'   => 'admin.Order.create',
    'store'    => 'admin.Order.store',
    'edit'     => 'admin.Order.edit',
    'update'   => 'admin.Order.update',
    'destroy'  => 'admin.Order.destroy',
]
]);

Route::get('/admin/offer/index', 'Admin\OfferController@index')->name('admin.offer.index');

Route::get('/admin/offer/show/{id}', 'Admin\OfferController@offerDetail')->name('admin.offer.show');


Route::post('/changeorderstatus', 'Admin\OrderController@changeorderstatus')->name('changeorderstatus');

Route::post('showorder', 'Admin\DashboardController@showorder')->name('showorder');

});

Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
    \Illuminate\Support\Facades\Artisan::call('passport:install');
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    \Illuminate\Support\Facades\Artisan::call('migrate');

    return 'Commands run successfully.';
});

