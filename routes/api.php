<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('sendOTP', "API\Client\LoginController@sendOTP")->name('sendOTP');
Route::post('checkOTP', "API\Client\LoginController@checkOTP")->name('checkOTP');
Route::get('cities', "API\ProductDetail\CityController@cities")->name('cities');
Route::get('category', "API\ProductDetail\SubCatagoryController@category")->name('category');

Route::post('productdetail', "API\ProductDetail\SubCatagoryController@productdetail")->name('productdetail');

Route::post('brandkeyfeaturedetail', "API\ProductDetail\SubCatagoryController@brandkeyfeaturedetail")->name('brandkeyfeaturedetail');


Route::post('area', "API\ProductDetail\CityController@area")->name('area');
Route::post('subarea', "API\ProductDetail\CityController@subarea")->name('subarea');
Route::post('subareabycity', "API\ProductDetail\CityController@subareabycity")->name('subareabycity');
Route::post('multisubarea', "API\ProductDetail\CityController@multisubarea")->name('multisubarea');

Route::post('customerregistration', "API\Client\LoginController@customerregistration")->name('customerregistration');

Route::post('facebooklogin', "API\Client\LoginController@facebooklogin")->name('facebooklogin');

Route::post('googlelogin', "API\Client\LoginController@googlelogin")->name('googlelogin');

Route::post('loginbynumber', "API\Client\LoginController@loginbynumber")->name('loginbynumber');

Route::post('productsearch', "API\ProductDetail\ProductSearchController@productsearch")->name('productsearch');

Route::post('allproductsearch', "API\ProductDetail\ProductSearchController@allproductsearch")->name('allproductsearch');

Route::post('productsearchbyfilter', "API\ProductDetail\ProductSearchController@productsearchbyfilter")->name('productsearchbyfilter');

Route::post('sorting', "API\ProductDetail\SubCatagoryController@sorting")->name('sorting');

Route::get('infographic', "API\ProductDetail\SubCatagoryController@infographic")->name('infographic');

Route::get('ads', "API\ProductDetail\SubCatagoryController@ads")->name('ads');

Route::post('adslink', "API\ProductDetail\SubCatagoryController@adslink")->name('adslink');

Route::get('packages', "API\Vendor\VendorRegistrationController@packages")->name('packages');

Route::post('vendorregistration', "API\Vendor\VendorRegistrationController@vendorregistration")->name('vendorregistration');

Route::post('vendorregistrationvalidation', "API\Vendor\VendorRegistrationController@vendorregistrationvalidation")->name('vendorregistrationvalidation');

Route::post('forgetpasswordbynumber', "API\Client\LoginController@forgetpasswordbynumber")->name('forgetpasswordbynumber');

Route::get('staticpage', "API\ProductDetail\SubCatagoryController@staticpage")->name('staticpage');

Route::middleware('auth:api')->group(function(){

Route::post('logout', "API\Client\LoginController@logout")->name('logout');

Route::post('customerrequestoffere', "API\ProductDetail\OffereController@customerrequestoffere")->name('customerrequestoffere');

Route::post('getcustomerrequestoffere', "API\ProductDetail\OffereController@getcustomerrequestoffere")->name('getcustomerrequestoffere');

Route::post('getcustomerwishlist', "API\ProductDetail\OffereController@getcustomerwishlist")->name('getcustomerwishlist');

Route::post('offere', "API\ProductDetail\OffereController@offere")->name('offere');

Route::post('getalloffere', "API\ProductDetail\OffereController@getalloffere")->name('getalloffere');

Route::post('updateoffere', "API\ProductDetail\OffereController@updateoffere")->name('updateoffere');

Route::post('deleteoffere', "API\ProductDetail\OffereController@deleteoffere")->name('deleteoffere');

Route::post('adddeletewishlist', "API\ProductDetail\OffereController@adddeletewishlist")->name('adddeletewishlist');

Route::post('customerprofileupdate', "API\Client\LoginController@customerprofileupdate")->name('customerprofileupdate');

Route::post('vendorprofileupdate', "API\Vendor\VendorRegistrationController@vendorprofileupdate")->name('vendorprofileupdate');

Route::post('changepassword', "API\Client\LoginController@changepassword")->name('changepassword');

Route::get('getallorder', "API\ProductDetail\OrderController@getallorder")->name('getallorder');

Route::post('addorder', "API\ProductDetail\OrderController@addorder")->name('addorder');

Route::post('changeofferedate', "API\ProductDetail\OffereController@changeofferedate")->name('changeofferedate');

Route::get('vendorreceivedoffere', "API\ProductDetail\OffereController@vendorreceivedoffere")->name('vendorreceivedoffere');

Route::get('vendorcredit', "API\ProductDetail\OffereController@vendorcredit")->name('vendorcredit');

Route::post('sendoffere', "API\ProductDetail\OffereController@sendoffere")->name('sendoffere');

Route::post('productwisevendoroffere', "API\ProductDetail\OffereController@productwisevendoroffere")->name('productwisevendoroffere');

Route::get('productreport', "API\ProductDetail\OffereController@productreport")->name('productreport');

Route::get('customerreport', "API\ProductDetail\OffereController@customerreport")->name('customerreport');

Route::get('offerereport', "API\ProductDetail\OffereController@offerereport")->name('offerereport');

Route::post('gettotalvendorbycustomerrequest', "API\ProductDetail\OffereController@gettotalvendorbycustomerrequest")->name('gettotalvendorbycustomerrequest');

Route::post('newofferestatuschange', "API\ProductDetail\OffereController@newofferestatuschange")->name('newofferestatuschange');

Route::post('register_fcm', "API\Client\LoginController@register_fcm")->name('register_fcm');

Route::post('packageEmail', "API\Vendor\VendorRegistrationController@packageEmail")->name('packageEmail');

});
