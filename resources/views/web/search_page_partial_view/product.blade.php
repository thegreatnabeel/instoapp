@if(count($load_more_product) > 0)
@foreach ($load_more_product as $product)
<input type="hidden" name="product_id" value="{{$product->id}}">
        <div class="col-md-3 col-sm-3 col-xs-12 noPadd">
          <div class="productMain">
            <div class="productImage">
              @if( !$product->productimage->isEmpty())
              <img src="{{$product->productimage[0]->productresizeimage[3]->product_resize_image_path}}" alt="{{$product->productimage[0]->productresizeimage[3]->alt}}">
              @else
              <img src="{{ asset("images/web-images/product.png")}}" alt="">
              @endif
              <div class="overlay">
                <a href="{{route('product', $product->product_slug)}}"  class="btn btnDetails">View Details</a>
              </div>
            </div>
            <div class="productDesc">
              <h3 class="uppercase">
                <span>{{$product->product_name}}</span><br>
                <span>{{$product->info}}</span>
              </h3>
              @if( $product->getminoffere!=null)
              <h5>Starting from</h5>
              <h4><span>Rs.</span> {{number_format($product->getminoffere)}} <span>/ month</span></h4>
              @else
              <h5>&nbsp;</h5>
              <h4>&nbsp; <span></span></h4>
              @endif
            </div>
          </div>
        </div>
@endforeach
@else
@if(count($get_search_product)>0)
@foreach ($get_search_product as $product)
<input type="hidden" name="product_id" value="{{$product->id}}">
        <div class="col-md-3 col-sm-3 col-xs-12 noPadd">
          <div class="productMain">
            <div class="productImage">
              @if( !$product->productimage->isEmpty())
              <img src="{{$product->productimage[0]->productresizeimage[3]->product_resize_image_path}}" alt="{{$product->productimage[0]->productresizeimage[3]->alt}}">
              @else
              <img src="{{ asset("images/web-images/product.png")}}" alt="">
              @endif
              <div class="overlay">
                <a href="{{route('product', $product->product_slug)}}"  class="btn btnDetails">View Details</a>
              </div>
            </div>
            <div class="productDesc">
              <h3 class="uppercase">
                <span>{{$product->product_name}}</span><br>
                <span>{{$product->info}}</span>
              </h3>
              @if( $product->getminoffere!=null)
              <h5>Starting from</h5>
              <h4><span>Rs.</span> {{number_format($product->getminoffere)}} <span>/ month</span></h4>
              @else
              <h5>&nbsp;</h5>
              <h4>&nbsp; <span></span></h4>
              @endif
            </div>
          </div>
        </div>
@endforeach
@else
@endif
@endif

