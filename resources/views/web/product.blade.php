@extends('web.layout')
@section('title', "{$product->product_name} | Instalment.pk")

@section('keywords',$product?$product->meta_description:'')

@section('description',$product?$product->meta_keyword:'')
@push('css')
<link rel="stylesheet" href="{{ asset('css/magiczoom.css')}}">
<link rel="stylesheet" href="{{ asset('css/nouislider.min.css')}}">
@endpush    
@section('content')
<div class="productInnerMain">
  <input type="hidden" value="{{$product->id}}" id="txthiddenproductid"/>
  <div class="container">
    <div class="row productInnerThumb">
      <div class="col-md-4 col-sm-12 col-xs-12 productLeft">
        <div class="productInnerImage">
          <h3><span>{{$product->product_name}}</span>
            <span>{{$product->info}}</span></h3>
          @if($product->productimage)
          <a id="product_media" data-options="zoomWidth: 650; zoomHeight: 500;" href="{{$product->productimage[0]->product_image}}" class="MagicZoom"><img src="{{$product->productimage[0]->product_image}}" alt="{{$product->productimage[0]->alt}}"></a>
          @else
          <a id="product_media" data-options="zoomWidth: 650; zoomHeight: 500;" href="{{ asset("images/web-images/led.png")}}" class="MagicZoom"><img src="{{ asset("images/web-images/led.png")}}" alt=""></a>
          @endif
          <div class="productSmall">
            <ul class="list-unstyled">
              @if($product->productimage)
              @foreach ($product->productimage as $image)
              <li><a href="{{$image->product_image}}" data-zoom-id="product_media" class="mz-thumb">
                <img src="{{$image->product_image}}" alt="{{$image->alt}}">
                </a>
               </li>
              @endforeach
              @else
               <li><a href="{{ asset("images/web-images/led.png")}}" data-zoom-id="product_media" class="mz-thumb">
                 <img src="{{ asset("images/web-images/led.png")}}">
                  </a>
               </li>
              @endif	
          </ul>
          <div class="clearfix"></div>
          </div>
          
        </div>
      </div>
      <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="productInnerDetails hidden-sm hidden-xs min_offere">
        @include('web.product_page_partial_view.min_offere')
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
        <div class="panel-heading boxHeader" role="tab" id="headingOne">
          <h2 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Product Description <i class="accordion_icon fa fa-minus"></i>
          </a>
          </h2>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            {!!$product?$product->product_description:''!!}
          </div>
        </div>
        </div>
        <div class="panel panel-default">
        <div class="panel-heading boxHeader" role="tab" id="headingTwo">
          <h2 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
           KEY FEATURES <i class="accordion_icon fa fa-plus"></i>
          </a>
          </h2>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
            @if($product->product_feature)
            @foreach(explode(',', $product->product_feature) as $feature)
            <div class="col-md-2">
             <button type="button"  class ="btn btnMain">{{explode(':',$feature)[0]}}</button>
             <p class="text-center">{{explode(':',$feature)[1]}}</p>
            </div>
            @endforeach
            @endif
          </div>
        </div>
        </div>
      </div>
      </div>
    </div>
    <div class="productInnerDetails hidden-lg hidden-md forSmDev min_offere" >
      @include('web.product_page_partial_view.min_offere')
    </div>
    <div class="boxMain">
      <div class="boxHeader">
        <div class="row">
          <div class="col-md-10 col-sm-9 col-xs-12">
            <h2>MORE OFFERS TO CONSIDER</h2>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-12 text-right">
            <select class="form-control sortby">
              <option value="Sort By">Sort By</option>
              <option value="Highest">Highest</option>
              <option value="Lowest">Lowest</option>
            </select>
          </div>
        </div>
      </div>
      <div class="boxBody filterBody">
        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="row">
            <label>Advance</label>
          </div>
          <div id="slider-advance"></div>
          <div class="row">
            <p id="slider-advance-value"></p>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="row">
            <label>EMI</label>
          </div>
          <div id="slider-emi"></div>
          <div class="row">
            <p id="slider-emi-value"></p>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 rowForSm">
          <label>Max Instalments</label>
          <input type="text" class="form-control" id="txtmonth">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 btnRowForSm">
          <button type="button" class="btn btn-primary btnMain" onclick="applybutton()">Apply Search</button>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
   <div class="venderInfo alloffere">
    @include('web.product_page_partial_view.offere')
  </div>
  <div class="col-md-12" style="margin-top: 20px; margin-bottom: 20px;">
    <div class="text-center col-md-4 col-sm-4 col-xs-4 col-md-offset-4 col-sm-offset-4 col-xs-offset-4">
        <a href="javascript:void(0)" class="btn btn-primary btnMain" id="btnloadmoreoffere">Load More Offers</a>
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="featured lastPadding">
    @include('web.product_page_partial_view.related_product')
  </div>
  </div>
  <div class="information">
		<div class="container">
			{!!$product?$product->seo_description:''!!}
		</div>
	</div>
</div>

<div id="callModal" class="modal fade callModalStyle" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>Please Ensure Following Documents to Order</h4>
        <ul>
          <li>Copy of CNIC</li>
          <li>Utility Bills</li>
          <li>Source of income (Visiting Card/Salary Slip)</li>
          <li>Valid Security Cheqaue</li>
          <li>Gurantor/Reference</li>
        </ul>
        <a href="javascript:void(0)" class="btn btn-primary btnMain" id="sellerDetBtn">Show Seller Details</a>
        <a href="{{route('web.home')}}" class="btn btn-primary btnMain">Home Page</a>
      </div>
    </div>

  </div>
</div>
<div id="selerDet" class="modal fade callModalStyle" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>Thanks for showing interest in this offer. We will not share your contact information with anyone.</h4>
        <h3>Seller Information</h3>
        <p><b>Name:</b> <span id="vendorname"></span></p>
        <p><b>Business Name:</b> <span id="companyname"></span></p>
        <p><b>Phone Number:</b> <span id="mobilenumber"></span></p>
        <p><b>Address</b> <span id="address"></span></p>
        <p><b>Please Mention Instalment.pk when calling Seller to get a good deal.</b></p>
        <a href="javascript:void(0)" class="btn btn-primary btnMain" id="call"> Call Now <i class="fa fa-phone"></i></a>
        <a href="javascript:void(0)" class="btn btn-primary btnMain" onclick="modalHide()">Close</a>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script src="{{ asset('js/magiczoom.js')}}"></script>
<script src="{{ asset('js/nouislider.min.js')}}"></script>
<script type="text/javascript">
	var slider_advance = document.getElementById('slider-advance');

	noUiSlider.create(slider_advance, {
		start: [{{$advance_min}}, {{$advance_max == 0 ? 1 : $advance_max + 1 }}],
    connect: true,
    step: 1,
		range: {
			'min': {{$advance_min}},
			'max': {{$advance_max == 0 ? 1 : $advance_max + 1 }}
		}
  });
  var slider_advance_value = document.getElementById('slider-advance-value');
     slider_advance.noUiSlider.on('update', function (values, handle) {
      slider_advance_value.innerHTML = values.join(' - ');;
  });
	 var slider_emi = document.getElementById('slider-emi');

	noUiSlider.create(slider_emi, {
		start: [{{$installment_min}}, {{$installment_max == 0 ? 1: $installment_max + 1}}],
    connect: true,
    step:1,
		range: {
			'min': {{$installment_min}},
			'max': {{$installment_max == 0 ? 1: $installment_max + 1}}
		}
  });
  var slider_emi_value = document.getElementById('slider-emi-value');
    slider_emi.noUiSlider.on('update', function (values, handle) {
        slider_emi_value.innerHTML = values.join(' - ');
  });
  $(".sortby").change(function(){
    if($(".sortby").val() =="Sort By") {
    }
    else{
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    });
    $.ajax({
        type: "POST",
        url: "{{ route('load.offerebysort') }}",
        data: { sort_by : $(".sortby").val(),city_id:$("#txthiddenlocation").val(),product_id : $("#txthiddenproductid").val()},
        success: function(data){
          $(".alloffere").empty();
          $(".alloffere").append(data.offerehtml);
        },
        error: function(error){
            toastr['error']('something went wrong');
        }
    });
    }
  });

  function datachangelocationwise(city_name){
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('load.productbylcation') }}",
      data: { city_name : city_name,product_id : $("#txthiddenproductid").val()},
      success: function(data){
        $(".min_offere").empty();
        $(".alloffere").empty();
        $(".min_offere").append(data.minofferehtml);
        $(".alloffere").append(data.offerehtml);
        $("#location_change").html('');
        $("#location_change").html(city_name);
        if(data.city_id!=0){
          $("#txthiddenlocation").val(data.city_id);
        }
        slider_advance.noUiSlider.updateOptions({
          start: [parseInt(data.advance_min), parseInt(data.advance_max) == 1 ? parseInt(data.advance_max) : parseInt(data.advance_max) + 1 ],
          connect: true,
          step: 1,
          range: {
            'min': parseInt(data.advance_min),
            'max': parseInt(data.advance_max) == 1 ? parseInt(data.advance_max) :parseInt(data.advance_max) + 1
          }
      });
        
           slider_advance.noUiSlider.on('update', function (values, handle) {
            slider_advance_value.innerHTML = values.join(' - ');;
        });
        slider_emi.noUiSlider.updateOptions({
          start: [parseInt(data.installment_min), parseInt(data.installment_max) == 1 ? parseInt(data.installment_max) : parseInt(data.installment_max) + 1],
          connect: true,
          step:1,
          range: {
            'min': parseInt(data.installment_min),
            'max': parseInt(data.installment_max) == 1 ? parseInt(data.installment_max) : parseInt(data.installment_max) + 1
          }

      });
          slider_emi.noUiSlider.on('update', function (values, handle) {
              slider_emi_value.innerHTML = values.join(' - ');
        });
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
  function applybutton(){
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('load.offerebyfilter') }}",
      data: { advance :$("#slider-advance-value").html(),emi:$("#slider-emi-value").html(),month:$("#txtmonth").val(),city_id:$("#txthiddenlocation").val(),product_id : $("#txthiddenproductid").val()},
      success: function(data){
        $(".alloffere").empty();
        $(".alloffere").append(data.offerehtml);
        $("#btnloadmoreoffere").hide();
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
  
  $("#btnloadmoreoffere").click(function (){
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('load.moreoffere') }}",
      data: { city_id:$("#txthiddenlocation").val(),product_id : $("#txthiddenproductid").val()},
      success: function(data){
        $(".alloffere").append(data.offerehtml);
        $("#offeretext").html('');
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  });
var offere_id = 0;
  function product_order(id){
    $("#callModal").modal('show');
    offere_id = id;
  }

  $('#sellerDetBtn').click( () => {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('order') }}",
      data: { offere_id:offere_id },
      success: function(data){
        if(data.status == true){
          $("#vendorname").html('');
          $("#companyname").html('');
          $("#mobilenumber").html('');
          $("#address").html('');
          $("#vendorname").html(data.get_offere_detail.offere_master.vendor.first_name+' '+data.get_offere_detail.offere_master.vendor.last_name);
          $("#companyname").html(data.get_offere_detail.offere_master.vendor.company_name);
          $("#mobilenumber").html(data.get_offere_detail.offere_master.vendor.primary_phone_number);
          $("#address").html(data.get_offere_detail.offere_master.vendor.business_address);
          $("#call").removeAttr('href');
          $("#call").attr('href','tel:+'+data.get_offere_detail.offere_master.vendor.primary_phone_number+'');

        $('#selerDet').modal("show"); 
        $('#callModal').modal("hide");
        toastr['success'](''+data.errMsg+'');
        }
        else{
          $("#vendorname").html('');
          $("#companyname").html('');
          $("#mobilenumber").html('');
          $("#address").html('');
          $("#vendorname").html(data.get_offere_detail.offere_master.vendor.first_name+' '+data.get_offere_detail.offere_master.vendor.last_name);
          $("#companyname").html(data.get_offere_detail.offere_master.vendor.company_name);
          $("#mobilenumber").html(data.get_offere_detail.offere_master.vendor.primary_phone_number);
          $("#address").html(data.get_offere_detail.offere_master.vendor.business_address);
          $("#call").removeAttr('href');
          $("#call").attr('href','tel:+'+data.get_offere_detail.offere_master.vendor.primary_phone_number+'');
          $('#selerDet').modal("show"); 
          $('#callModal').modal("hide");
          toastr['error'](''+data.errMsg+'');
        }
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  });
  function modalHide(){
    $('#selerDet').modal("hide");
  }
</script>
    
@endpush