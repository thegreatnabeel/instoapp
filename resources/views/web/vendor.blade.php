@extends('web.layout')
@section('title', 'Vendor')

@section('keywords',$vendor_cms?$vendor_cms->meta_description:'')

@section('description',$vendor_cms?$vendor_cms->meta_keyword:'')
    
@section('content')

          <div class="feature">
            <!-- include feature deal  -->
            @include('web.vendor_page_partial_view.feature_deal')
            <!-- end include feature deal  -->
          </div>      



<input type="hidden" id="txthiddenvendorid" value="{{$vendor->id}}"/>
<div class="featured featured2 lastPadding">
  <div class="container">
    <div class="boxMain">
      <div class="Vendor">
        <!-- include all Product -->
      @include('web.vendor_page_partial_view.vendor')
      <!-- end include all Product  -->
      </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="text-center col-md-4 col-sm-4 col-xs-4 col-md-offset-4 col-sm-offset-4 col-xs-offset-4 loadMoreBtn">
      <a href="javascript:void(0)" class="btn btn-primary btnMain" onclick="loadproduct()">Load More</a>
    </div>
</div>
<div class="clearfix"></div>
<div class="information">
  <div class="container">
  {!!$vendor_cms?$vendor_cms->seo_description:''!!}
  </div>
</div>
@endsection
@push('js')
<script>
  function datachangelocationwise(city_name){
    $(".feature").empty();
    $("#loadnewproduct").empty();
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('load.featureandproductforvendorpage') }}",
      data: { city_name : city_name,vendor_id : $("#txthiddenvendorid").val()},
      success: function(data){
        $(".feature").append(data.featurehtml);
        $("#loadnewproduct").append(data.producthtml);
        $("#location_change").html('');
        $("#location_change").html(city_name);
        $("#txthiddenlocation").val(data.location_id);
        if(data.last_product_id > 0){
          $("#txthiddenproductid").val(data.last_product_id);
        }
        $("#featuredDeals").owlCarousel({
          loop: true,
          margin: 0,
          responsiveClass: true,
          autoplay: true,
          nav: true,
          navText: [
              "<i class='glyphicon glyphicon-chevron-left'></i>",
              "<i class='glyphicon glyphicon-chevron-right'></i>"
          ],
          autoplayTimeout: 3000,
          responsive: {
              0: {
                  items: 1
              },
              360: {
                  items: 1
              },
              400: {
                  items: 2
              },
              500: {
                  items: 2
              },
              600: {
                  items: 3
              },
              1000: {
                  items: 4
              }
          }
      });
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
  function loadproduct(){
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  var product_id = $('input[name^="product_id"]').map(function(){return $(this).val();}).get();
  $.ajax({
      type: "POST",
      url: "{{ route('load.vendorwiseproduct') }}",
      data: { product_id : product_id,vendor_id : $("#txthiddenvendorid").val(),city_id:$("#txthiddenlocation").val() },
      success: function(data){
        $("#loadnewproduct").append(data.producthtml);
      
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
</script>
    
@endpush