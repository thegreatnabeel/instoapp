@if(!empty($product))
@if(count($product->get_all_offere_detail)>0)
@for ($i = 0; $i < count($product->get_all_offere_detail); $i++)
<div class="row">
  <div class="col-md-2 col-sm-2 col-xs-6 vendorNameCenter">
      @if($product->get_all_offere_detail[$i]->offeremaster->vendor->picture_path)
        <img src="{{$product->get_all_offere_detail[$i]->offeremaster->vendor->picture_path}}" alt="">
        @else
        <img src="{{ asset("images/web-images/qaziSm.png")}}" alt="">
      @endif
      <h5>{{$product->get_all_offere_detail[$i]->offeremaster->vendor->company_name??''}} <span>{{$product->get_all_offere_detail[$i]->offeremaster->vendor->first_name.' '.$product->get_all_offere_detail[$i]->offeremaster->vendor->last_name}}</span></h5>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 hidden-lg hidden-md hidden-sm paddTop wishMob">
  <label style="display: block;"></label>
  @if(Auth::check()==true && Auth::user()->user_type == "client")
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="product_wishlist('{{$product->get_all_offere_detail[$i]->id}}')"><i class="{{$product->get_all_offere_detail[$i]->wishlist!=null ? 'fas' : 'far' }} fa-heart wishlist{{$product->get_all_offere_detail[$i]->id}}"></i></a>
  <a href="javascript:void(0)" class="btn btnCall" title="Call Now" onclick="product_order('{{$product->get_all_offere_detail[$i]->id}}')"><i class="fa fa-phone"></i></a>
  @else
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="login()"><i class="far fa-heart"></i></a>
  <a href="javascript:void(0)" class="btn btnCall" title="Call Now" onclick="login()"><i class="fa fa-phone"></i></a>
  @endif
  </div>
  <div class="col-md-3 col-sm-3 col-xs-6 paddTop">
    <h4>Monthly Installment <span>Rs.{{number_format($product->get_all_offere_detail[$i]->installment??0)}}/-</span></h4>
  </div>
  <div class="col-md-1 col-sm-1 col-xs-6 paddTop">
    <h4>Month <span>{{number_format($product->get_all_offere_detail[$i]->month??0)}}</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 paddTop">
    <h4>Advance <span>Rs.{{number_format($product->get_all_offere_detail[$i]->Advance??0)}}/-</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 paddTop">
    <h4>Total Amount  <span>Rs.{{number_format($product->get_all_offere_detail[$i]->total_price??0)}}/-</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-12 hidden-xs paddTop">
  <label style="display: block;"></label>
  @if(Auth::check()==true && Auth::user()->user_type == "client")
      <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="product_wishlist('{{$product->get_all_offere_detail[$i]->id}}')"><i class="{{$product->get_all_offere_detail[$i]->wishlist!=null ? 'fas' : 'far' }} fa-heart wishlist{{$product->get_all_offere_detail[$i]->id}}"></i></a>
      <a href="javascript:void(0)" class="btn btnCall hidden-sm hidden-xs" title="Call Now" onclick="product_order('{{$product->get_all_offere_detail[$i]->id}}')">Call Now</a>
      <a href="javascript:void(0)" class="btn btnCall hidden-lg hidden-md" title="Call Now" onclick="product_order('{{$product->get_all_offere_detail[$i]->id}}')"><i class="fa fa-phone"></i></a>
      @else
      <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="login()"><i class="far fa-heart"></i></a>
      <a href="javascript:void(0)" class="btn btnCall hidden-sm hidden-xs" title="Call Now" onclick="login()">Call Now</a>
      <a href="javascript:void(0)" class="btn btnCall hidden-lg hidden-md" title="Call Now" onclick="login()"><i class="fa fa-phone"></i></a>
      @endif
  </div>
  <div class="clearfix"></div>
</div>
@endfor
@else
@endif
@else
@if(count($get_all_offere_detail)>0)
@for ($i = 0; $i < count($get_all_offere_detail); $i++)
<div class="row">
  <div class="col-md-2 col-sm-2 col-xs-6 vendorNameCenter">
      @if($get_all_offere_detail[$i]->offeremaster->vendor->picture_path)
        <img src="{{$get_all_offere_detail[$i]->offeremaster->vendor->picture_path}}" alt="">
        @else
        <img src="{{ asset("images/web-images/qaziSm.png")}}" alt="">
      @endif
      <h5>{{$get_all_offere_detail[$i]->offeremaster->vendor->company_name??''}} <span>{{$get_all_offere_detail[$i]->offeremaster->vendor->first_name.' '.$get_all_offere_detail[$i]->offeremaster->vendor->last_name}}</span></h5>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 hidden-lg hidden-md hidden-sm paddTop wishMob">
  <label style="display: block;"></label>
  @if(Auth::check()==true && Auth::user()->user_type == "client")
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="product_wishlist('{{$get_all_offere_detail[$i]->id}}')"><i class="{{$get_all_offere_detail[$i]->wishlist!=null ? 'fas' : 'far' }} fa-heart wishlist{{$get_all_offere_detail[$i]->id}}"></i></a>
  <a href="javascript:void(0)" class="btn btnCall" title="Call Now" onclick="product_order('{{$get_all_offere_detail[$i]->id}}')"><i class="fa fa-phone"></i></a>
  @else
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="login()"><i class="far fa-heart"></i></a>
  <a href="javascript:void(0)" class="btn btnCall" title="Call Now" onclick="login()"><i class="fa fa-phone"></i></a>
  @endif
  </div>
  <div class="col-md-3 col-sm-3 col-xs-6 paddTop">
    <h4>Monthly Installment <span>Rs.{{number_format($get_all_offere_detail[$i]->installment??0)}}/-</span></h4>
  </div>
  <div class="col-md-1 col-sm-1 col-xs-6 paddTop">
    <h4>Month <span>{{number_format($get_all_offere_detail[$i]->month??0)}}</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 paddTop">
    <h4>Advance <span>Rs.{{number_format($get_all_offere_detail[$i]->Advance??0)}}/-</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 paddTop">
    <h4>Total Amount  <span>Rs.{{number_format($get_all_offere_detail[$i]->total_price??0)}}/-</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-12 hidden-xs paddTop">
  <label style="display: block;"></label>
  @if(Auth::check()==true && Auth::user()->user_type == "client")
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="product_wishlist('{{$get_all_offere_detail[$i]->id}}')"><i class="{{$get_all_offere_detail[$i]->wishlist!=null ? 'fas' : 'far' }} fa-heart wishlist{{$get_all_offere_detail[$i]->id}}"></i></a>
  <a href="javascript:void(0)" class="btn btnCall hidden-sm hidden-xs" title="Call Now" onclick="product_order('{{$get_all_offere_detail[$i]->id}}')">Call Now</a>
  <a href="javascript:void(0)" class="btn btnCall hidden-lg hidden-md" title="Call Now" onclick="product_order('{{$get_all_offere_detail[$i]->id}}')"><i class="fa fa-phone"></i></a>
  @else
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="login()"><i class="far fa-heart"></i></a>
  <a href="javascript:void(0)" class="btn btnCall hidden-sm hidden-xs" title="Call Now" onclick="login()">Call Now</a>
  <a href="javascript:void(0)" class="btn btnCall hidden-lg hidden-md" title="Call Now" onclick="login()"><i class="fa fa-phone"></i></a>
  @endif
  </div>
  <div class="clearfix"></div>
</div>
@endfor
@else
@endif
@endif