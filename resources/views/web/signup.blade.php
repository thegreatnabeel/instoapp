@extends('web.layout')
@section('title', 'Sign up')

@section('keywords',$register_cms?$register_cms->meta_description:'')

@section('description',$register_cms?$register_cms->meta_keyword:'')
    
@section('content')
<div class="aboutInner">
  <div class="container">
    <div class="boxMain">
      <div class="boxHeader faqHeader">
      <h2>Register</h2>
    </div>
      <div class="boxBody">
         <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">			
                  <div id="phoneField" style="{{Session::get('register')['divname']=='phoneField' ? '':'display:none;'}}">
                    <div class="form-group ">
                    <label>Phone Number</label>
                      <input type="tel" class="form-control" placeholder="Enter Phone Number" id="txtphonenumber">
                    <span class="help-block text-danger" id="phonerror"></span>
                    </div>
                    <div class="form-group">
                      <button type="button" class="btn btn-primary btnMain" id="goToOtp">Register Now</button>
                    </div>
                  </div>
                  <div id="otpFields" style="{{Session::get('register')['divname']=='otpFields' ? '':'display:none;'}}">
                    <div class="form-group">
                    <label>OTP</label>
                    <div class="col-12 jpa">
                      <input type="text" class="form-control" name="otp_code" id="otp_code1"  maxlength="1">
                      <input type="text" class="form-control" name="otp_code" id="otp_code2"  maxlength="1">
                      <input type="text" class="form-control" name="otp_code" id="otp_code3"  maxlength="1">
                      <input type="text" class="form-control" name="otp_code" id="otp_code4"  maxlength="1">
                    </div>
                    </div>
                    <div class="form-group">
                      <button type="button" class="btn btn-primary btnMain" id="goToRegister">Register Now</button>
                    </div>
                  </div>
                  <div id="registerField" style="{{Session::get('register')['divname']=='registerField' ? '':'display:none;'}}">
                    <form action="{{ route('web.customerregistration') }}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" id="device_token" name="device_token">
                      <input type="hidden" id="txthiddenphonenumber" name="hiddenphone" value="{{Session::get('register')['phone_number']}}"/>
                      <input type="hidden" id="txthiddenotp" name="hiddenotp" value="{{Session::get('register')['opt']}}"/>
                      <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                        <label>First Name</label>
                         <input type="text" class="form-control" placeholder="Enter First Name" name="first_name" value={{ old('first_name') }}>
                         @if($errors->has('first_name'))
                         <span class="help-block text-danger">{{ $errors->first('first_name') }}</span>
                         @endif
                       </div>
                       <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                        <label>Last Name</label>
                         <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" value={{ old('last_name') }}>
                         @if($errors->has('last_name'))
                         <span class="help-block text-danger">{{ $errors->first('last_name') }}</span>
                         @endif
                       </div>
                       <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                       <label>Email</label>
                        <input type="email" class="form-control" placeholder="Enter Email" name="email" value={{ old('email') }}>
                        @if($errors->has('email'))
                        <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                        @endif
                      </div>
                      <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label>Password</label>
                         <input type="password" class="form-control" placeholder="Enter Password" name="password" value={{ old('password') }}>
                         @if($errors->has('password'))
                         <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                         @endif
                       </div>
                       <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <label>Confirmed Password</label>
                         <input type="password" class="form-control" placeholder="Enter Confirmed Password" name="password_confirmation" value={{ old('password_confirmation') }}>
                         @if($errors->has('password_confirmation'))
                         <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                         @endif
                       </div>
                       <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                        <label>City <span>*</span></label>
                         <select class="form-control city_id" name="city_id" >
                        <option value="">Select City</option>
                        @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name}}</option>
                        @endforeach
                        </select>
                         @if($errors->has('city_id'))
                            <span class="help-block text-danger">{{ $errors->first('city_id') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('sub_area_id') ? 'has-error' : '' }}">
                      <label> Sub Area <span>*</span></label>
                       <select class="form-control sub_area_id" name="sub_area_id" >
                      <option value="">Select Sub Area</option>
                      </select>
                       @if($errors->has('sub_area_id'))
                          <span class="help-block text-danger">{{ $errors->first('sub_area_id') }}</span>
                      @endif
                  </div>
                  <div class="form-group {{ $errors->has('base_location') ? 'has-error' : '' }}">
                    <label>Base Location</label>
                     <input type="text" class="form-control" placeholder="Enter Base Location" name="base_location" value={{ old('base_location') }}>
                     @if($errors->has('base_location'))
                     <span class="help-block text-danger">{{ $errors->first('base_location') }}</span>
                     @endif
                   </div>
                   <div class="form-group">
                     <label>Profile Image</label>
                     <input type="file" class="form-control" accept="image/png, image/jpeg, image/gif, image/jpg" name="picture_path" required=""/>
                   </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btnMain">Register Now</button>
                    </div>
                    </form>
                  </div>
            </div>
          </div>
          </div>
      </div>
    </div>	
  </div>
</div>
@endsection
@push('js')
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<script src="{{ asset('js/jquery.autotab.js')}}"></script>
<link rel="manifest" href="{{request()->root()}}/manifest.json">
<script>
    // Initialize Firebase
    // TODO: Replace with your project's customized code snippet
    var config = {
      apiKey: "AIzaSyA5-qXHCegTJFMmQYjno3uD7ybNJKt7yFE",
      authDomain: "insto-32129.firebaseapp.com",
      databaseURL: "https://insto-32129.firebaseio.com",
      projectId: "insto-32129",
      storageBucket: "insto-32129.appspot.com",
      messagingSenderId: "389948375048",
      appId: "1:389948375048:web:621f461535bad354dc91bd",
      measurementId: "G-PN1B37GJ8L"
    };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();
    messaging
        .requestPermission()
        .then(function () { 
            console.log("Notification permission granted.");

            // get the token in the form of promise
            return messaging.getToken()
        })
        .then(function(token) {
            $("#device_token").val(token);
            console.log(token);
        })

        .catch(function (err) {
            console.log("Unable to get permission to notify.", err);
        });

        messaging.onMessage(function(payload) {
          console.log("Message received. ", payload);
          NotisElem.innerHTML = NotisElem.innerHTML + JSON.stringify(payload) 
      });

      $(document).ready(function () {
        $('#otp_code1').autotab({ target: '#otp_code2', format: 'numeric' });
        $('#otp_code2').autotab({ target: '#otp_code3', format: 'numeric', previous: '#otp_code1' });
        $('#otp_code3').autotab({ target: '#otp_code4', format: 'numeric', previous: '#otp_code2' });
        $('#otp_code4').autotab({ previous: '#otp_code3', format: 'numeric' });
      });
$('#goToOtp').click( () => {
  $.ajax({
    url: "{{route('web.sendotp')}}",
    data: {phone_number : $("#txtphonenumber").val()},
    type: "POST",
    success: function (json) {
    if(json.status==1)
      {
        toastr['success']("OTP Send Successfully");
        $("#phonerror").html('');
        $('#phoneField').hide();
        $('#otpFields').show();
      }
      else if(json.status==2){
        if(json.errMsg.phone_number[0] == 'The phone number must be between 11 and 12 digits.'){
          $("#phonerror").html('Please enter number in 03xxxxxxxxx');
        }
        else{
          $("#phonerror").html(json.errMsg.phone_number[0]);
        }
       
      }
      else if(json.status==3){
        $("#phonerror").html(json.errMsg);
      }
      else
       {
        toastr['error']("OTP Not Send"); 
        $("#phonerror").html('');
      }
    }
  });
});
$('#goToRegister').click( () => {
 var otp = $('input[name^="otp_code"]').map(function(){return $(this).val();}).get();
 if(otp.length == 4 && otp[0]!="" && otp[1]!="" && otp[2]!="" && otp[3]!=""){
   var otp_number = otp[0]+''+otp[1]+''+otp[2]+''+otp[3];
  $.ajax({
    url: "{{route('web.checkotp')}}",
    data: {phone_number : $("#txtphonenumber").val(), otp:otp_number },
    type: "POST",
    success: function (json) {
    if(json.status==true)
      {
        toastr['success']("OTP verify successfully");
        $("#txthiddenphonenumber").val($("#txtphonenumber").val());
        $("#txthiddenotp").val(otp_number);
        $('#otpFields').hide(); 
        $('#registerField').show();
      }
      else
       {
        toastr['error']("Please Enter Correct Otp"); 
      }
    }
  });
}
else{
 toastr['error']("4 digits otp required");
}
});

//$(".city_id").select2();
$(".city_id").change( function(){
 var id = $(this).val();
 if(id==null){

 }
 else{
  $.ajax({
    url: "{{ route('web.subarea') }}",
   type: 'POST',
   data : { data: id},
   success: function(response){
        if(response.status == 'success'){
          $(".sub_area_id").empty();
            html = '<option value="">Select Sub Area</option>';
            $(response.sub_areas).each( function (i,d){
                html += '<option value="'+ d.id +'">'+ d.name +'</option>';
            });
            $(".sub_area_id").html(html);
            //$(".sub_area_id").select2();
        }else{
            toastr['error']("Something went wrong.");
        }
   },
   error: function(error){
       toastr['error']("Something went wrong.");
   }

});
 }
});
</script>
    
@endpush