@extends('web.layout')
@section('title', "{$category->catagory_name} | Instalment.pk")

@section('keywords',$category?$category->meta_description:'')

@section('description',$category?$category->meta_keyword:'')
    
@section('content')

          <div class="feature">
            <!-- include feature deal  -->
            @include('web.category_page_partial_view.feature_deal')
            <!-- end include feature deal  -->
          </div>      



<input type="hidden" id="txthiddencategoryid" value="{{$category->id}}"/>
<div class="featured featured2 lastPadding">
  <div class="container">
    <div class="boxMain">
      <div class="category">
        <!-- include all category -->
      @include('web.category_page_partial_view.category')
      <!-- end include all category  -->
      </div>
      <div class="dropDownSpecsMenu" style="display: none;">
        @include('web.category_page_partial_view.filter')	
      </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="text-center col-md-4 col-sm-4 col-xs-4 col-md-offset-4 col-sm-offset-4 col-xs-offset-4 loadMoreBtn">
      <a href="javascript:void(0)" class="btn btn-primary btnMain" onclick="loadproduct()">Load More</a>
    </div>
</div>
<div class="clearfix"></div>
<div class="information">
  <div class="container">
  {!!$category?$category->seo_description:''!!}
  </div>
</div>
@endsection
@push('js')
<script>
  function datachangelocationwise(city_name){
    $(".feature").empty();
    $("#loadnewproduct").empty();
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('load.featureandcategoryforcategorypage') }}",
      data: { city_name : city_name,category_id : $("#txthiddencategoryid").val()},
      success: function(data){
        $(".feature").append(data.featurehtml);
        $("#loadnewproduct").append(data.categoryhtml);
        $("#location_change").html('');
        $("#location_change").html(city_name);
        $("#txthiddenlocation").val(data.location_id);
        $('#showDropdown').click(function(){
          $('.dropDownSpecsMenu').fadeIn();
        });
        $('#closeDropDownSpecsMenu').click(function(){
          $('.dropDownSpecsMenu').fadeOut();
        });
        $("#featuredDeals").owlCarousel({
          loop: true,
          margin: 0,
          responsiveClass: true,
          autoplay: true,
          nav: true,
          navText: [
              "<i class='glyphicon glyphicon-chevron-left'></i>",
              "<i class='glyphicon glyphicon-chevron-right'></i>"
          ],
          autoplayTimeout: 3000,
          responsive: {
              0: {
                  items: 1
              },
              360: {
                  items: 1
              },
              400: {
                  items: 2
              },
              500: {
                  items: 2
              },
              600: {
                  items: 3
              },
              1000: {
                  items: 4
              }
          }
      });
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
  var  brands = [];
  var feature_values = [];
  var feature_id = [];
  function loadproduct(){
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  var product_id = $('input[name^="product_id"]').map(function(){return $(this).val();}).get();
  $.ajax({
      type: "POST",
      url: "{{ route('load.product') }}",
      data: { product_id : product_id , category_id : $("#txthiddencategoryid").val(),city_id:$("#txthiddenlocation").val(),feature_values : feature_values,brands :brands, },
      success: function(data){
        $("#loadnewproduct").append(data.producthtml);
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
  function filter_product(id,type){
    if(type == 'feature'){
      feature_values.splice( $.inArray(id,feature_values) ,1 );
    }
    else{
      brands.splice( $.inArray(id,brands) ,1 );
    }
    $.each($("input[name='check_box_feature']:checked"), function(){
      if($.inArray($(this).val(), feature_values) !== -1){
      }
      else{
        feature_values.push($(this).val());
      }
      if($.inArray($(this).data('key-feature-id'), feature_id) !== -1){
      }
      else{
        feature_id.push($(this).data('key-feature-id'));
      }
    });
    $.each($("input[name='check_box_brand']:checked"), function(){
      if($.inArray($(this).val(), brands) !== -1){
      }
      else{
        brands.push($(this).val());
      }
    });
    $("#loadnewproduct").empty();
    $.ajax({
      type: "POST",
      url: "{{ route('load.filter.product') }}",
      data: { city_id:$("#txthiddenlocation").val(), category_id : $("#txthiddencategoryid").val(),feature_values : feature_values,brands :brands,feature_id:feature_id },
      success: function(data){
        $("#loadnewproduct").append(data.producthtml);
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
</script>
    
@endpush