<div class="featured">
  <div class="container">
    <div class="boxMain">
      <div class="boxHeader">
        <h2>Featured Deals</h2>
      </div>
      <div class="boxBody">
        <div class="owl-carousel" id="featuredDeals">
        @foreach ($get_featured_product as $feature_product)
          <div class="item">
            <div class="productMain">
              <div class="productImage">
                <img src="{{$feature_product->offeremaster->product->productimage[0]->productresizeimage[3]->product_resize_image_path}}" alt="">
                <div class="overlay">
                  @if(Auth::check()==true && Auth::user()->user_type == "client")
                  <a href="javascript:void(0)" onclick="product_wishlist('{{$feature_product->id}}')"><i class="{{$feature_product->wishlist!=null ? 'fas' : 'far' }} fa-heart wishlist{{$feature_product->id}}"></i></a>
                  @else
                  <a href="javascript:void(0)" onclick="login()"><i class="far fa-heart"></i></a>
                  @endif
                  <a href="{{ route('product', $feature_product->offeremaster->product->product_slug)}}"  class="btn btnDetails" >View Details</a>
                </div>
              </div>
              <div class="productDesc">
                <h3 class="uppercase">{{ trans($feature_product->offeremaster->product->product_name)}}</h3>
                <h5 class="uppercase">By: {{ trans($feature_product->offeremaster->vendor->company_name)}}</h5>
                <h4>{{number_format($feature_product->installment)}} <span>Rs. / month</span></h4>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>