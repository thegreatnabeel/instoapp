@if(!empty($vendor))
      <div class="boxHeader">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <h2>{{$vendor->company_name}}</h2>
          </div>
        </div>
      </div>
      <div class="boxBody">
      <div class="row">
        <div class="col-md-3">
          @include('web.vendor_page_partial_view.vendor_detail')
        </div> 
      <div class="col-md-9" id="loadnewproduct">
        @include('web.vendor_page_partial_view.product')			
      </div>
    </div>
    </div>    
@else   
@endif

