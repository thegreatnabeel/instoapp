<div class="brandInformation">
    <div class="brandLogo">
        @if($vendor->picture_path==null)
        <img src="{{ asset('images/web-images/qaziSm.png') }}" alt="">

        <img src="{{$vendor->picture_path}}" alt="">
        @endif
    </div>
    <div class="brandDesc">
        <h3>{{$vendor->first_name??"".''.$vendor->last_name??""}}</h3>
        <h2>Description</h2>
        <p>{{$vendor->business_address??""}}</p>
    </div>
</div>