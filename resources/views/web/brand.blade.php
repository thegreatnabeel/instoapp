@extends('web.layout')
@section('title', "{$brand->brand_name} | Instalment.pk")

@section('keywords',$brand?$brand->meta_description:'')

@section('description',$brand?$brand->meta_keyword:'')
    
@section('content')

          <div class="feature">
            <!-- include feature deal  -->
            @include('web.brand_page_partial_view.feature_deal')
            <!-- end include feature deal  -->
          </div>      



<input type="hidden" id="txthiddenbrandid" value="{{$brand->id}}"/>
<div class="featured featured2 lastPadding">
  <div class="container">
    <div class="boxMain">
      <div class="brand">
        <!-- include all brand -->
      @include('web.brand_page_partial_view.brand')
      <!-- end include all brand  -->
      </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="text-center col-md-4 col-sm-4 col-xs-4 col-md-offset-4 col-sm-offset-4 col-xs-offset-4 loadMoreBtn">
      <a href="javascript:void(0)" class="btn btn-primary btnMain" onclick="loadproduct()">Load More</a>
    </div>
</div>
<div class="clearfix"></div>
<div class="information">
  <div class="container">
  {!!$brand?$brand->seo_description:''!!}
  </div>
</div>
@endsection
@push('js')
<script>
  function datachangelocationwise(city_name){
    $(".feature").empty();
    $("#loadnewproduct").empty();
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('load.featureandbrandforbrandpage') }}",
      data: { city_name : city_name,brand_id : $("#txthiddenbrandid").val()},
      success: function(data){
        $(".feature").append(data.featurehtml);
        $("#loadnewproduct").append(data.brandhtml);
        $("#location_change").html('');
        $("#location_change").html(city_name);
        $("#txthiddenlocation").val(data.location_id);
        $("#featuredDeals").owlCarousel({
          loop: true,
          margin: 0,
          responsiveClass: true,
          autoplay: true,
          nav: true,
          navText: [
              "<i class='glyphicon glyphicon-chevron-left'></i>",
              "<i class='glyphicon glyphicon-chevron-right'></i>"
          ],
          autoplayTimeout: 3000,
          responsive: {
              0: {
                  items: 1
              },
              360: {
                  items: 1
              },
              400: {
                  items: 2
              },
              500: {
                  items: 2
              },
              600: {
                  items: 3
              },
              1000: {
                  items: 4
              }
          }
      });
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
  function loadproduct(){
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  var product_id = $('input[name^="product_id"]').map(function(){return $(this).val();}).get();
  $.ajax({
      type: "POST",
      url: "{{ route('load.brandwiseproduct') }}",
      data: { product_id : product_id,brand_id : $("#txthiddenbrandid").val(),city_id:$("#txthiddenlocation").val() },
      success: function(data){
        $("#loadnewproduct").append(data.producthtml);
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
</script>
    
@endpush