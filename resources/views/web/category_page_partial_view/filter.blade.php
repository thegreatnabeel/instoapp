<div class="row">
  <div class="col-md-12 text-right">
    <a href="javascript:void(0)" id="closeDropDownSpecsMenu"><img src="{{ asset("images/web-images/closeIcon.png")}}" alt=""></a>
  </div>
</div>
@if(count($brands)>0)
<div class="dropdownItems">
  <h3>Brands</h3>
  <ul class="list-unstyled">
    @foreach ($brands as $brand)
    <li><label><input type="checkbox" name="check_box_brand" value="{{$brand->id}}" onclick="filter_product('{{$brand->id}}','brand')">{{$brand->brand_name}}</label></li>
    @endforeach
  </ul>
</div>
@else
@endif
@if(count($key_features)>0)
@foreach ($key_features as $key_feature)
<div class="dropdownItems">
  <h3>{{$key_feature->title}}</h3>
  <ul class="list-unstyled">
    @foreach ($key_feature->key_feature_value as $item)
    <li><label><input type="checkbox" name="check_box_feature" value="{{$item->sub_key_feature_value}}" data-key-feature-id ="{{$item->key_feature_id}}" onclick="filter_product('{{$item->sub_key_feature_value}}','feature')"> {{$item->sub_key_feature_value}}</label></li>    
    @endforeach
    
  </ul>
</div>
@endforeach
@else
@endif