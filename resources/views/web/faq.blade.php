@extends('web.layout')
@section('title', 'FAQs')

@section('keywords',$get_faq_page_seo_description?$get_faq_page_seo_description->meta_description:'')

@section('description',$get_faq_page_seo_description?$get_faq_page_seo_description->meta_keyword:'')
    
@section('content')

{!!$get_faq_page_seo_description?$get_faq_page_seo_description->page_content:''!!}

<div class="clearfix"></div>
<div class="information">
  <div class="container">
  {!!$get_faq_page_seo_description?$get_faq_page_seo_description->seo_description:''!!}
  </div>
</div>
@endsection