@extends('web.layout')
@section('title', 'Login')

@section('keywords',$login_cms?$login_cms->meta_description:'')

@section('description',$login_cms?$login_cms->meta_keyword:'')
    
@section('content')
<div class="aboutInner">
  <div class="container">
    <div class="boxMain">
      <div class="boxHeader faqHeader">
      <h2>Login</h2>
    </div>
      <div class="boxBody">
         <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
  <ul class="nav nav-tabs text-center">
    <li class="{{Session::get('Logintabcheck')=='withPhone'? 'active' :'' }}"><a data-toggle="tab" href="#withPhone">Login with Phone</a></li>
    <li class="{{Session::get('Logintabcheck')=='withEmail'? 'active' :'' }}"><a data-toggle="tab" href="#withEmail">Login with Email</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane fade in {{Session::get('Logintabcheck')=='withPhone'? 'active' :'' }}" id="withPhone">
      <form action="{{ route('web.login.login') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" id="device_token1" name="device_token1"> 
        <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
         <label>Phone Number</label>
          <input type="tel" class="form-control" placeholder="Enter Phone Number" name="phone_number" value={{ old('phone_number') }}>
        @if($errors->has('phone_number'))
          <span class="help-block text-danger">{{ $errors->first('phone_number')=="The phone number must be between 11 and 12 digits."?'Please enter number in 03xxxxxxxxx':$errors->first('phone_number') }}</span>
          @endif
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
          <label>Password</label>
          <input type="Password" class="form-control" name="password" placeholder="Enter Password" value={{ old('password') }}>
          @if($errors->has('password'))
          <span class="help-block text-danger">{{ $errors->first('password') }}</span>
          @endif
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label><input type="checkbox"> &nbsp; Remember me </label>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 forgotPass">
              <a href="{{route('web.forget')}}">Forgot Password?</a>
            </div>
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary btnMain">Login Now</button>
        </div>
        <div class="form-group">
          <button type="button" class="btn btn-primary btnMain">Signup as Customer</button>
        </div>
      </form>
    </div>
    <div class="tab-pane fade {{Session::get('Logintabcheck')=='withEmail'? 'active' :'' }} in" id="withEmail">
      <form action="{{ route('web.login.loginbyemail') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" id="device_token2" name="device_token2">
         <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
         <label>Email</label>
          <input type="email" class="form-control" placeholder="Enter Email" name="email" value={{ old('email') }}>
          @if($errors->has('email'))
          <span class="help-block text-danger">{{ $errors->first('email') }}</span>
          @endif
        </div>
        <div class="form-group {{ $errors->has('password1') ? 'has-error' : '' }}">
          <label>Password</label>
          <input type="Password" class="form-control" name="password1" placeholder="Enter Password" value={{ old('password1') }}>
          @if($errors->has('password1'))
          <span class="help-block text-danger">{{ $errors->first('password1') }}</span>
          @endif
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label><input type="checkbox"> &nbsp; Remember me </label>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 forgotPass">
              <a href="{{route('web.forget')}}">Forgot Password?</a>
            </div>
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary btnMain">Login Now</button>
        </div>
        <div class="form-group">
          <button type="button" class="btn btn-primary btnMain">Signup as Customer</button>
        </div>
      </form>
    </div>
  </div>
  </div>
          </div>
          </div>
      </div>
    </div>	
  </div>
</div>
@endsection
@push('js')
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<link rel="manifest" href="{{request()->root()}}/manifest.json">
<script>
    // Initialize Firebase
    // TODO: Replace with your project's customized code snippet
    var config = {
        apiKey: "AIzaSyA5-qXHCegTJFMmQYjno3uD7ybNJKt7yFE",
        authDomain: "insto-32129.firebaseapp.com",
        databaseURL: "https://insto-32129.firebaseio.com",
        projectId: "insto-32129",
        storageBucket: "insto-32129.appspot.com",
        messagingSenderId: "389948375048",
        appId: "1:389948375048:web:621f461535bad354dc91bd",
        measurementId: "G-PN1B37GJ8L"
    };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();
    messaging
        .requestPermission()
        .then(function () { 
            console.log("Notification permission granted.");

            // get the token in the form of promise
            return messaging.getToken()
        })
        .then(function(token) {
            $("#device_token1").val(token);
            $("#device_token2").val(token);
            console.log(token);
        })

        .catch(function (err) {
            console.log("Unable to get permission to notify.", err);
        });

    messaging.onMessage(function(payload) {
        console.log("Message received. ", payload);
    });
</script>   
@endpush