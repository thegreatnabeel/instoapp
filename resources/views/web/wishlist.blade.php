@extends('web.layout')
@section('title', 'Wish List')

@section('keywords',$wishlist_cms?$wishlist_cms->meta_description:'')

@section('description',$wishlist_cms?$wishlist_cms->meta_keyword:'')
    
@section('content')

<div class="featured">
<div class="container">
  <div class="boxHeader">
        <h2>Wishlist</h2>
      </div>
    <div class="aboutInner">
      @include('web.wishlist_page_partial_view.offere')
    </div> 
</div>
</div>
<div class="information">
  <div class="container">
    {!!$wishlist_cms?$wishlist_cms->seo_description:''!!}
  </div>
</div>

<div id="callModal" class="modal fade callModalStyle" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>Please Ensure Following Documents to Order</h4>
        <ul>
          <li>Copy of CNIC</li>
          <li>Utility Bills</li>
          <li>Source of income (Visiting Card/Salary Slip)</li>
          <li>Valid Security Cheqaue</li>
          <li>Gurantor/Reference</li>
        </ul>
        <a href="javascript:void(0)" class="btn btn-primary btnMain" id="sellerDetBtn">Show Seller Details</a>
        <a href="{{route('web.home')}}" class="btn btn-primary btnMain">Home Page</a>
      </div>
    </div>

  </div>
</div>
<div id="selerDet" class="modal fade callModalStyle" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>Thanks for showing interest in this offer. We will not share your contact information with anyone.</h4>
        <h3>Seller Information</h3>
        <p><b>Name:</b> <span id="vendorname"></span></p>
        <p><b>Business Name:</b> <span id="companyname"></span></p>
        <p><b>Phone Number:</b> <span id="mobilenumber"></span></p>
        <p><b>Address</b> <span id="address"></span></p>
        <p><b>Please Mention Instalment.pk when calling Seller to get a good deal.</b></p>
        <a href="javascript:void(0)" class="btn btn-primary btnMain" id="call">Call Now <i class="fa fa-phone"></i></a>
        <a href="javascript:void(0)" class="btn btn-primary btnMain" onclick="modalHide()">Close</a>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
<script>
  var offere_id = 0;
  function product_order(id){
    $("#callModal").modal('show');
    offere_id = id;
  }

  $('#sellerDetBtn').click( () => {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('order') }}",
      data: { offere_id:offere_id },
      success: function(data){
        if(data.status == true){
          $("#vendorname").html('');
          $("#companyname").html('');
          $("#mobilenumber").html('');
          $("#address").html('');
          $("#vendorname").html(data.get_offere_detail.offere_master.vendor.first_name+' '+data.get_offere_detail.offere_master.vendor.last_name);
          $("#companyname").html(data.get_offere_detail.offere_master.vendor.company_name);
          $("#mobilenumber").html(data.get_offere_detail.offere_master.vendor.primary_phone_number);
          $("#address").html(data.get_offere_detail.offere_master.vendor.business_address);
          $("#call").removeAttr('href');
          $("#call").attr('href','tel:+'+data.get_offere_detail.offere_master.vendor.primary_phone_number+'');

        $('#selerDet').modal("show"); 
        $('#callModal').modal("hide");
        toastr['success'](''+data.errMsg+'');
        }
        else{
          $("#vendorname").html('');
          $("#companyname").html('');
          $("#mobilenumber").html('');
          $("#address").html('');
          $("#vendorname").html(data.get_offere_detail.offere_master.vendor.first_name+' '+data.get_offere_detail.offere_master.vendor.last_name);
          $("#companyname").html(data.get_offere_detail.offere_master.vendor.company_name);
          $("#mobilenumber").html(data.get_offere_detail.offere_master.vendor.primary_phone_number);
          $("#address").html(data.get_offere_detail.offere_master.vendor.business_address);
          $("#call").removeAttr('href');
          $("#call").attr('href','tel:+'+data.get_offere_detail.offere_master.vendor.primary_phone_number+'');
          $('#selerDet').modal("show"); 
          $('#callModal').modal("hide");
          toastr['error'](''+data.errMsg+'');
        }
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  });
  function modalHide(){
    $('#selerDet').modal("hide");
  }
</script>
    
@endpush