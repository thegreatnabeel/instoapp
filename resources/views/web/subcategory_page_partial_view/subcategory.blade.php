@if(!empty($sub_category))
      <div class="boxHeader">
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-12">
            <h2>{{$sub_category->sub_catagory_name}}</h2>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12 text-right">
            <a href="javascript:void(0)" id="showDropdown"> Filters <img src="{{ asset("images/web-images/specsIcon.png")}}" alt=""> </a>
          </div>
        </div>
      </div>
      <div class="boxBody" id="loadnewproduct">
        @include('web.subcategory_page_partial_view.product')			
      </div>    
@else   
@endif

