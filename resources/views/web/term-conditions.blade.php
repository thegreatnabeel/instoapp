@extends('web.layout')
@section('title', 'Terms & Conditions')

@section('keywords',$get_termcondition_page_seo_description?$get_termcondition_page_seo_description->meta_description:'')

@section('description',$get_termcondition_page_seo_description?$get_termcondition_page_seo_description->meta_keyword:'')
    
@section('content')

{!!$get_termcondition_page_seo_description?$get_termcondition_page_seo_description->page_content:''!!}

<div class="clearfix"></div>
<div class="information">
  <div class="container">
  {!!$get_termcondition_page_seo_description?$get_termcondition_page_seo_description->seo_description:''!!}
  </div>
</div>
@endsection