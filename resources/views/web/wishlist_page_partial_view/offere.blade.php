@if(count($get_wishlist)>0)
@foreach ($get_wishlist as $wishlist)
<div class="venderInfo">
<div class="row">
  <div class="col-md-2 col-sm-2 col-xs-6 vendorNameCenter">
      @if($wishlist->get_all_offere_detail->offeremaster->product->productimage)
        <img src="{{$wishlist->get_all_offere_detail->offeremaster->product->productimage[0]->productresizeimage[0]->product_resize_image_path}}" alt="" width="90px" height="50px">
        @else
        <img src="{{ asset("images/web-images/qaziSm.png")}}" alt="">
      @endif
      <h5>{{$wishlist->get_all_offere_detail->offeremaster->product->product_name}}  <span>{{$wishlist->get_all_offere_detail->offeremaster->vendor->company_name??''}}</span></h5>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 hidden-lg hidden-md hidden-sm paddTop wishMob">
  <label style="display: block;"></label>
  @if(Auth::check()==true && Auth::user()->user_type == "client")
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="product_wishlist('{{$wishlist->get_all_offere_detail->id}}')"><i class="{{$wishlist!=null ? 'fas' : 'far' }} fa-heart wishlist{{$wishlist->get_all_offere_detail->id}}"></i></a>
  <a href="javascript:void(0)" class="btn btnCall hidden-sm hidden-xs" title="Call Now" onclick="product_order('{{$wishlist->get_all_offere_detail->id}}')">Call Now</a>
  <a href="javascript:void(0)" class="btn btnCall hidden-lg hidden-md" title="Call Now" onclick="product_order('{{$wishlist->get_all_offere_detail->id}}')"><i class="fa fa-phone"></i></a>
  @else
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="login()"><i class="far fa-heart"></i></a>
  <a href="javascript:void(0)" class="btn btnCall hidden-sm hidden-xs" title="Call Now" onclick="login()">Call Now</a>
  <a href="javascript:void(0)" class="btn btnCall hidden-lg hidden-md" title="Call Now" onclick="login()"><i class="fa fa-phone"></i></a>
  @endif
  </div>
  <div class="col-md-3 col-sm-3 col-xs-6 paddTop">
    <h4>Monthly Installment <span>Rs.{{number_format($wishlist->get_all_offere_detail->installment??0)}}/-</span></h4>
  </div>
  <div class="col-md-1 col-sm-1 col-xs-6 paddTop">
    <h4>Month <span>{{number_format($wishlist->get_all_offere_detail->month??0)}}</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 paddTop">
    <h4>Advance <span>Rs.{{number_format($wishlist->get_all_offere_detail->Advance??0)}}/-</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-6 paddTop">
    <h4>Total Amount  <span>Rs.{{number_format($wishlist->get_all_offere_detail->total_price??0)}}/-</span></h4>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-12 hidden-xs paddTop">
  <label style="display: block;"></label>
  @if(Auth::check()==true && Auth::user()->user_type == "client")
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="product_wishlist('{{$wishlist->get_all_offere_detail->id}}')"><i class="{{$wishlist!=null ? 'fas' : 'far' }} fa-heart wishlist{{$wishlist->get_all_offere_detail->id}}"></i></a>
  <a href="javascript:void(0)" class="btn btnCall hidden-sm hidden-xs" title="Call Now" onclick="product_order('{{$wishlist->get_all_offere_detail->id}}')">Call Now</a>
  <a href="javascript:void(0)" class="btn btnCall hidden-lg hidden-md" title="Call Now" onclick="product_order('{{$wishlist->get_all_offere_detail->id}}')"><i class="fa fa-phone"></i></a>
  @else
  <a href="javascript:void(0)" class="btn wishlistBtn" title="Add to Wishlist" onclick="login()"><i class="far fa-heart"></i></a>
  <a href="javascript:void(0)" class="btn btnCall hidden-sm hidden-xs" title="Call Now" onclick="login()">Call Now</a>
  <a href="javascript:void(0)" class="btn btnCall hidden-lg hidden-md" title="Call Now" onclick="login()"><i class="fa fa-phone"></i></a>
  @endif
  </div>
  <div class="clearfix"></div>
</div>
</div>
@endforeach
@else
@endif
