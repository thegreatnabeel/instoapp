<div class="brandInformation">
    <div class="brandLogo">
        @if($brand->brand_img_path==null)
        <img src="{{ asset('images/web-images/qaziSm.png') }}" alt="">
        @else
        <img src="{{$brand->brand_img_path}}" alt="">
        @endif
    </div>
    <div class="brandDesc">
        <h3>{{$brand->brand_name}}</h3>
        <h2>Description</h2>
        <p>{{$brand->brand_description}}</p>
    </div>
</div>