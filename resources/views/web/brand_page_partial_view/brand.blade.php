@if(!empty($brand))
      <div class="boxHeader">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <h2>{{$brand->brand_name}}</h2>
          </div>
        </div>
      </div>
      <div class="boxBody">
      <div class="row">
        <div class="col-md-3">
          @include('web.brand_page_partial_view.brand_detail')
        </div> 
      <div class="col-md-9" id="loadnewproduct">
        @include('web.brand_page_partial_view.product')			
      </div>
    </div>
    </div>    
@else   
@endif

