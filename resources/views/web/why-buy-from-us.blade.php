@extends('web.layout')
@section('title', 'Why Buy From Us')

@section('keywords',$get_whybuy_page_seo_description?$get_whybuy_page_seo_description->meta_description:'')

@section('description',$get_whybuy_page_seo_description?$get_whybuy_page_seo_description->meta_keyword:'')
    
@section('content')

{!!$get_whybuy_page_seo_description?$get_whybuy_page_seo_description->page_content:''!!}

<div class="clearfix"></div>
<div class="information">
  <div class="container">
  {!!$get_whybuy_page_seo_description?$get_whybuy_page_seo_description->seo_description:''!!}
  </div>
</div>
@endsection