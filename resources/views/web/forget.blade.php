@extends('web.layout')
@section('title', 'Forget Password')

@section('keywords',$forget_cms?$forget_cms->meta_description:'')

@section('description',$forget_cms?$forget_cms->meta_keyword:'')
    
@section('content')
<div class="aboutInner">
  <div class="container">
    <div class="boxMain">
      <div class="boxHeader faqHeader">
      <h2>Forget Password</h2>
    </div>
      <div class="boxBody">
         <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">			
                  <div id="phoneField" style="{{Session::get('forget')['divname']=='phoneField' ? '':'display:none;'}}">
                    <div class="form-group ">
                    <label>Phone Number</label>
                      <input type="tel" class="form-control" placeholder="Enter Phone Number" id="txtphonenumber">
                    <span class="help-block text-danger" id="phonerror"></span>
                    </div>
                    <div class="form-group">
                      <button type="button" class="btn btn-primary btnMain" id="goToOtp">Send OTP</button>
                    </div>
                  </div>
                  <div id="otpFields" style="{{Session::get('forget')['divname']=='otpFields' ? '':'display:none;'}}">
                    <div class="form-group">
                    <label>OTP</label>
                    <div class="col-12 jpa">
                      <input type="text" class="form-control" name="otp_code" id="otp_code1"  maxlength="1">
                      <input type="text" class="form-control" name="otp_code" id="otp_code2"  maxlength="1">
                      <input type="text" class="form-control" name="otp_code" id="otp_code3"  maxlength="1">
                      <input type="text" class="form-control" name="otp_code" id="otp_code4"  maxlength="1">
                    </div>
                    </div>
                    <div class="form-group">
                      <button type="button" class="btn btn-primary btnMain" id="goToForget">Check OTP</button>
                    </div>
                  </div>
                  <div id="forgetField" style="{{Session::get('forget')['divname']=='forgetField' ? '':'display:none;'}}">
                    <form action="{{ route('web.forget.customerforgetPassword') }}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" id="txthiddenphonenumber" name="hiddenphone" value="{{Session::get('forget')['phone_number']}}"/>
                      <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label>Password</label>
                         <input type="password" class="form-control" placeholder="Enter Password" name="password" value={{ old('password') }}>
                         @if($errors->has('password'))
                         <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                         @endif
                       </div>
                       <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <label>Confirmed Password</label>
                         <input type="password" class="form-control" placeholder="Enter Confirmed Password" name="password_confirmation" value={{ old('password_confirmation') }}>
                         @if($errors->has('password_confirmation'))
                         <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                         @endif
                       </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btnMain">Forget Now</button>
                    </div>
                    </form>
                  </div>
            </div>
          </div>
          </div>
      </div>
    </div>	
  </div>
</div>
@endsection
@push('js')
<script src="{{ asset('js/jquery.autotab.js')}}"></script>
<script>
  $(document).ready(function () {
    $('#otp_code1').autotab({ target: '#otp_code2', format: 'numeric' });
    $('#otp_code2').autotab({ target: '#otp_code3', format: 'numeric', previous: '#otp_code1' });
    $('#otp_code3').autotab({ target: '#otp_code4', format: 'numeric', previous: '#otp_code2' });
    $('#otp_code4').autotab({ previous: '#otp_code3', format: 'numeric' });
  });
$('#goToOtp').click( () => {
  $.ajax({
    url: "{{route('web.forget.sendotp')}}",
    data: {phone_number : $("#txtphonenumber").val()},
    type: "POST",
    success: function (json) {
    if(json.status==1)
      {
        toastr['success']("OTP Send Successfully");
        $("#phonerror").html('');
        $('#phoneField').hide();
        $('#otpFields').show();
      }
      else if(json.status==2){
        if(json.errMsg.phone_number[0] == 'The phone number must be between 11 and 12 digits.'){
          $("#phonerror").html('Please enter number in 03xxxxxxxxx');
        }
        else{
          $("#phonerror").html(json.errMsg.phone_number[0]);
        }
       
      }
      else
       {
        toastr['error']("OTP Not Send"); 
        $("#phonerror").html('');
      }
    }
  });
});
$('#goToForget').click( () => {
 var otp = $('input[name^="otp_code"]').map(function(){return $(this).val();}).get();
 if(otp.length == 4 && otp[0]!="" && otp[1]!="" && otp[2]!="" && otp[3]!=""){
   var otp_number = otp[0]+''+otp[1]+''+otp[2]+''+otp[3];
  $.ajax({
    url: "{{route('web.forget.checkotp')}}",
    data: {phone_number : $("#txtphonenumber").val(), otp:otp_number },
    type: "POST",
    success: function (json) {
    if(json.status==true)
      {
        toastr['success']("OTP verify successfully");
        $("#txthiddenphonenumber").val($("#txtphonenumber").val());
        $("#txthiddenotp").val(otp_number);
        $('#otpFields').hide(); 
        $('#forgetField').show();
      }
      else
       {
        toastr['error']("Please Enter Correct Otp"); 
      }
    }
  });
}
else{
 toastr['error']("4 digits otp required");
}
});

//$(".city_id").select2();
$(".city_id").change( function(){
 var id = $(this).val();
 if(id==null){

 }
 else{
  $.ajax({
    url: "{{ route('web.subarea') }}",
   type: 'POST',
   data : { data: id},
   success: function(response){
        if(response.status == 'success'){
          $(".sub_area_id").empty();
            html = '<option value="">Select Sub Area</option>';
            $(response.sub_areas).each( function (i,d){
                html += '<option value="'+ d.id +'">'+ d.name +'</option>';
            });
            $(".sub_area_id").html(html);
            //$(".sub_area_id").select2();
        }else{
            toastr['error']("Something went wrong.");
        }
   },
   error: function(error){
       toastr['error']("Something went wrong.");
   }

});
 }
});
</script>
    
@endpush