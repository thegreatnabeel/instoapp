@extends('web.layout')
@section('title', 'Easy Monthly Installment Plans in Lahore  - Bikes - AC - Kitchen Appliances - Electronics -instalment.pk | Instalment Pk')

@section('keywords',$get_home_page_seo_description?$get_home_page_seo_description->meta_description:'')

@section('description',$get_home_page_seo_description?$get_home_page_seo_description->meta_keyword:'')
    
@section('content')
<div class="banner">
  <div class="container">
    <div class="img1"> <img src="{{ asset("images/web-images/bg1.png")}}" alt="bg"> </div>
    <div class="img2"> <img src="{{ asset("images/web-images/bg2.png")}}" alt="bg"> </div>
    <div class="owl-carousel" id="bannerCarousel">
      <div class="item">
        <img src="{{ asset("images/web-images/slider.png")}}" alt="">
      </div>
    </div>
  </div>
</div>
<!-- include feature deal  -->
<div id="feature">
  @include('web.home_page_partial_view.feature_deal')
</div>
<!-- end include feature deal  -->
<!-- include category banner  -->
@include('web.home_page_partial_view.category_banner')
<!-- end include category banner  -->

<div class="category">
  <!-- include all category -->
@include('web.home_page_partial_view.category')
<!-- end include all category  -->
</div>
<div class="col-md-12">
  <div class="text-center col-md-4 col-sm-4 col-xs-4 col-md-offset-4 col-sm-offset-4 col-xs-offset-4 loadMoreBtn">
      <a href="javascript:void(0)" class="btn btn-primary btnMain" onclick="loadcategory()">Load More</a>
    </div>
</div>
<div class="clearfix"></div>
<div class="works">
  <div class="container">
    <h3>How It Works?</h3>
    <div class="howContent">
      <img src="{{ asset("images/web-images/how.png")}}" alt="">
    </div>
  </div>
</div>
<div class="information">
  <div class="container">
  {!!$get_home_page_seo_description?$get_home_page_seo_description->seo_description:''!!}
  </div>
</div>
<input type="hidden" id="txthiddencategoryid" value="{{$last_category_id}}"/>

{{--<form action="{{ route('store') }}" method="POST">
  @csrf
<button type="submit">Send Notification</button>    
</form>--}}

@endsection
@push('js')
<script>
  function datachangelocationwise(city_name){
    $("#feature").empty();
    $(".category").empty();
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('load.featureandcategory') }}",
      data: { city_name : city_name},
      success: function(data){
        $("#feature").append(data.featurehtml);
        $(".category").append(data.categoryhtml);
        $("#location_change").html('');
        $("#location_change").html(city_name);
        if(data.last_category_id != 0 )
        {
        $("#txthiddencategoryid").val(data.last_category_id);
        }
        $("#txthiddenlocation").val(data.location_id);
        $("#featuredDeals").owlCarousel({
          loop: true,
          margin: 0,
          responsiveClass: true,
          autoplay: true,
          nav: true,
          navText: [
              "<i class='glyphicon glyphicon-chevron-left'></i>",
              "<i class='glyphicon glyphicon-chevron-right'></i>"
          ],
          autoplayTimeout: 3000,
          responsive: {
              0: {
                  items: 1
              },
              360: {
                  items: 1
              },
              400: {
                  items: 2
              },
              500: {
                  items: 2
              },
              600: {
                  items: 3
              },
              1000: {
                  items: 4
              }
          }
      });
      
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }
  function loadcategory(){
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
      }
  });
  $.ajax({
      type: "POST",
      url: "{{ route('load.category') }}",
      data: { category_id : $("#txthiddencategoryid").val(),city_id:$("#txthiddenlocation").val()},
      success: function(data){
        $(".category").append(data.categoryhtml);
        if(data.last_category_id!=0)
        {
        $("#txthiddencategoryid").val(data.last_category_id);
        }
      
      },
      error: function(error){
          toastr['error']('something went wrong');
      }
  });
  }


</script>
    
@endpush