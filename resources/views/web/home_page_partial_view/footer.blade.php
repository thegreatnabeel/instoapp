<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-4 col-xs-12 footerLinks capsLinks">
        <h3>top brands</h3>
        <ul class="list-unstyled">
          @foreach ($get_top_product_brand as $product)
          <li><a href="{{route('brand', $product->brand->brand_slug)}}"><i class="fas fa-chevron-right"></i>{{$product->brand->brand_name}}</a></li>    
          @endforeach
        </ul>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-12 footerLinks capsLinks">
        <h3>Top Categories</h3>
        <ul class="list-unstyled">
          @foreach ($get_top_product_category as $product)
          <li><a href="{{route('category', $product->category->catagory_slug)}}"><i class="fas fa-chevron-right"></i>{{$product->category->catagory_name}}</a></li>    
          @endforeach
        </ul>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-12 footerLinks">
        <h3>GET TO KNOW Us</h3>
        <ul class="list-unstyled">
          <li><a href="{{route("web.aboutus")}}" ><i class="fas fa-chevron-right"></i> About Us</a></li>
          <li><a href="{{route("web.term-conditions")}}" ><i class="fas fa-chevron-right"></i> Terms & Conditions</a></li>
          <li><a href="{{route("web.privacy-policy")}}" ><i class="fas fa-chevron-right"></i> Privacy Policy</a></li>
          <li><a href="{{route("web.why-buy-from-us")}}" ><i class="fas fa-chevron-right"></i> Why buy from Us</a></li>
          <li><a href="javascript:void(0)">&nbsp</a></li>
        </ul>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-12 footerLinks">
        <h3>LET US HELP YOU</h3>
        <ul class="list-unstyled">
          <li><a href="{{route("web.contactus")}}" ><i class="fas fa-chevron-right"></i> Contact Us</a></li>
          <li><a href="{{route("web.faq")}}" ><i class="fas fa-chevron-right"></i> FAQs</a></li>
        </ul>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-12 footerLinks">
        <h3>MY ACCOUNT</h3>
        <ul class="list-unstyled">
          @if(Auth::check()==true && Auth::user()->user_type == "client")
          <li><a href="javascript:void(0)" onclick="dashboard_link('account')"><i class="fas fa-chevron-right"></i> My Account</a></li>
          <li><a href="javascript:void(0)" onclick="dashboard_link('orders')" ><i class="fas fa-chevron-right"></i> Order History</a></li>
          @else
          <li><a href="javascript:void(0)" onclick="login()"><i class="fas fa-chevron-right"></i> My Account</a></li>
          <li><a href="javascript:void(0)" onclick="login()"><i class="fas fa-chevron-right"></i> Order History</a></li>
          @endif
        </ul>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-12 footerLinks">
        <h3>Contact Info</h3>
        <ul class="list-unstyled">
          <li><a href="#"><i class="fas fa-chevron-right"></i> support@installment.pk</a></li>
          <li><a href="#"><i class="fas fa-chevron-right"></i> Call Us: 0306-2033366</a></li>
        </ul>
        <ul class="list-unstyled list-inline socialIcons">
          <li><a href="https://web.facebook.com"><i class="fab fa-facebook"></i></a></li>
          <li><a href="https://www.youtube.com"><i class="fab fa-youtube"></i></a></li>
          <li><a href="https://twitter.com"><i class="fab fa-twitter"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>