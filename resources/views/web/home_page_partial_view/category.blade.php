@if(count($get_category_product) > 0 )
@foreach ($get_category_product as $category)
<div class="featured">
  <div class="container">
    <div class="boxMain">
      <div class="boxHeader">
        <h2 class="capitalize">{{$category->catagory_name}}</h2>
      </div>
      <div class="boxBody">
        @foreach ($category->gethotproduct as $key => $product)
        <input type="hidden" name="product_id" value="{{$product->id}}">
        <div class="col-md-3 col-sm-3 col-xs-12 noPadd">
          <div class="productMain">
            <div class="productImage">
              @if( !$product->productimage->isEmpty())
              <img src="{{$product->productimage[0]->productresizeimage[3]->product_resize_image_path}}" alt="{{$product->productimage[0]->productresizeimage[3]->alt}}">
              @else
              <img src="{{ asset("images/web-images/product.png")}}" alt="">
              @endif
              <div class="overlay">
                <a href="{{route('product', $product->product_slug)}}"  class="btn btnDetails">View Details</a>
              </div>
            </div>
            <div class="productDesc">
              <h3 class="uppercase">
                <span>{{$product->product_name}}</span><br>
                <span>{{$product->info}}</span>
               </h3>
              @if( $product->getminoffere!=null)
              <h5>Starting from</h5>
              <h4><span>Rs.</span> {{number_format($product->getminoffere)}} <span>/ month</span></h4>
              @else
              <h5>&nbsp;</h5>
              <h4>&nbsp; <span></span></h4>
              @endif
            </div>
          </div>
        </div>
        @endforeach	
          <div class="col-md-12">
          <div class="viewMore">
            <a href="{{route('category', $category->catagory_slug)}}" >View More</a>
          </div>
          </div>		
      </div>
    </div>
  </div>
</div>    
@endforeach
@else
@endif

