@if(!$get_bannar_category->isEmpty())
<div class="productSlider">
  <div class="container-fluid">
    <div class="row">
      <div class="owl-carousel" id="proSlider">
        @foreach ($get_bannar_category as $category)
        <div class="item">
        <a href="{{route('category', $category->catagory_slug)}}" target="_blank"><img src="{{$category->catagory_img_path}}" alt=""></a>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>

@endif