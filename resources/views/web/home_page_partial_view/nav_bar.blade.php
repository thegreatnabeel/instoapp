<nav class="navbar navbar-default ">
  <div class="container-fluid"> 
   <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
   </div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="col-xs-10 hidden-lg hidden-md hidden-sm">
      <div class="input-group headerSearch hidden-lg hidden-md hidden-sm">
    <input type="text" class="form-control" placeholder="Search for products..." id="txtsearchformobile">
    <ul class="addproduct" style="display:none;">

    </ul>
    <div class="input-group-btn">
      <button class="btn" type="button" onclick="searchallproductformobile()">
      <span class="glyphicon glyphicon-search"></span>
      </button>
    </div>
    </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="collapse navbar-collapse" id="defaultNavbar1">
      <ul class="nav navbar-nav">
        @foreach ($get_category as $category)
        <li class="dropdown"><a href="javascript:void(0)"  class="dropdown-toggle"  role="button" aria-expanded="false">{{$category->catagory_name}}</a>
          <ul class="dropdown-menu" role="menu">
            @foreach ($category->subcatagory as $sub_category)
            <li><a href="{{route('subcategory', $sub_category->sub_catagory_slug)}}" >{{$sub_category->sub_catagory_name}}</a></li>    
            @endforeach
          </ul>
        </li>     
        @endforeach
       
      </ul>
      
    </div>
    </div>
    
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>