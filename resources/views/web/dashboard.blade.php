@extends('web.layout')
@section('title', 'Dashboard')
 
@section('keywords',$get_dashboard_cms?$get_dashboard_cms->meta_description:'')

@section('description',$get_dashboard_cms?$get_dashboard_cms->meta_keyword:'')

@section('content')
<div class="allContent">
<div class="featured">
<div class="container">
<div class="boxMain">
<div class="boxHeader faqHeader">
      <h2>My Account</h2>
    </div>
</div>
<div class="dashboardItems">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12">
    <div class="tabsLeft">
    <ul class="nav nav-tabs">
    <li class="{{Session::get('checkdashboard') == 'dashboard' ? 'active' : ''}}"><a data-toggle="tab" href="#dashboard">Dashboard</a></li>
    <li class="{{Session::get('checkdashboard') == 'orders' ? 'active' : ''}}"><a data-toggle="tab" href="#orders">Orders</a></li>
    <li class="{{Session::get('checkdashboard') == 'account' ? 'active' : ''}}"><a data-toggle="tab" href="#account">Account Details</a></li>
    <li><form action="{{ route('web.login.logout') }}" method="POST">
        @csrf  
        <button type="submit" class="btn btnMain btnLogout">Logout<button></form>
    </li>
    </ul>
    </div>
</div>
<div class="col-md-10 col-sm-9 col-xs-12">
    <div class="tab-content">
    <div id="dashboard" class="tab-pane fade {{Session::get('checkdashboard') == 'dashboard' ? 'active' : ''}} in">
        <h3>Dashboard</h3>
    </div>
    <div id="orders" class="tab-pane fade {{Session::get('checkdashboard') == 'orders' ? 'active' : ''}} in">
        <h3>Orders</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>Seller</th>
                    <th>Product</th>
                    <th>Total Instalments</th>
                    <th>Down Payment</th>
                    <th>Amount Per Month</th>
                    <th>Seller Comment</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                    @foreach ($get_order_history as $order)
                    <tr>
                    <td>{{$order->offeredetail->offeremaster->vendor->first_name.' '.$order->offeredetail->offeremaster->vendor->last_name}}</td>    
                    <td>{{$order->offeredetail->offeremaster->product->product_name}}</td>
                    <td>{{number_format($order->offeredetail->month)??''}}</td>
                    <td>{{number_format($order->offeredetail->Advance)??''}}</td>
                    <td>{{number_format($order->offeredetail->installment)??''}}</td>
                    <td></td>
                    <td>{{$order->status??''}}</td>
                    <td>{{$order->order_date??''}}</td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
    <div id="account" class="tab-pane fade {{Session::get('checkdashboard') == 'account' ? 'active' : ''}} in">
        <form action="{{ route('web.customerprofileupdate') }}" method="POST" enctype="multipart/form-data">
            @csrf
           <div class="row">
              <div class="col-md-4">
                <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                <label>First Name</label>
                <input type="text" class="form-control" placeholder="Enter First Name" name="first_name" value={{ $customer_detail->first_name }}>
                @if($errors->has('first_name'))
                <span class="help-block text-danger">{{ $errors->first('first_name') }}</span>
                @endif
              </div>
              </div>
              <div class="col-md-4">
            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
              <label>Last Name</label>
               <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" value={{ $customer_detail->last_name }}>
               @if($errors->has('last_name'))
               <span class="help-block text-danger">{{ $errors->first('last_name') }}</span>
               @endif
             </div>
            </div>
            <div class="col-md-4">
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
             <label>Email</label>
              <input type="email" class="form-control" placeholder="Enter Email" name="email" value={{ $customer_detail->user->email??'' }}>
              @if($errors->has('email'))
              <span class="help-block text-danger">{{ $errors->first('email') }}</span>
              @endif
            </div>
            </div>
           </div>
            <div class="row">
              <div class="col-md-4">
              <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
              <label>Password</label>
               <input type="password" class="form-control" placeholder="Enter Password" name="password" value={{ old('password') }}>
               @if($errors->has('password'))
               <span class="help-block text-danger">{{ $errors->first('password') }}</span>
               @endif
             </div></div>
              <div class="col-md-4">
              <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
              <label>Confirmed Password</label>
               <input type="password" class="form-control" placeholder="Enter Confirmed Password" name="password_confirmation" value={{ old('password_confirmation') }}>
               @if($errors->has('password_confirmation'))
               <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
               @endif
             </div>
             </div>
            </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                <label>City <span>*</span></label>
                <select class="form-control city_id" name="city_id" >
                <option value="">Select City</option>
                @foreach($cities as $city)
                <option value="{{$city->id}}" {{$getcity->id==$city->id?'selected':''}}>{{$city->name}}</option>
                @endforeach
                </select>
                @if($errors->has('city_id'))
                    <span class="help-block text-danger">{{ $errors->first('city_id') }}</span>
                @endif
            </div>
            </div>
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('sub_area_id') ? 'has-error' : '' }}">
              <label> Sub Area <span>*</span></label>
              <select class="form-control sub_area_id" name="sub_area_id" >
              <option value="">Select Sub Area</option>
              @foreach ($sub_areas as $sub_area)
              <option value="{{$sub_area->id}}" {{$customer_detail->sub_area_id==$sub_area->id?'selected':''}}>{{$sub_area->name}}</option>
              @endforeach
              </select>
              @if($errors->has('sub_area_id'))
                  <span class="help-block text-danger">{{ $errors->first('sub_area_id') }}</span>
              @endif
          </div>            
            </div>
            <div class="col-md-4">
                <div class="form-group {{ $errors->has('base_location') ? 'has-error' : '' }}">
              <label>Base Location</label>
              <input type="text" class="form-control" placeholder="Enter Base Location" name="base_location" value={{ $customer_detail->base_address }}>
              @if($errors->has('base_location'))
              <span class="help-block text-danger">{{ $errors->first('base_location') }}</span>
              @endif
            </div>
            </div>
          </div>
           <div class="row">
           <div class="col-md-4">
            <div class="form-group">
              <label>Profile Image</label>
              <input type="file" class="form-control" accept="image/png, image/jpeg, image/gif, image/jpg" name="picture_path" required=""/>
              </div>
            </div>
             <div class="col-md-4">
             <div class="form-group">
                <button type="submit" class="btn btn-primary btnMain btnUpdate">Update Profile</button>
              </div>
            </div>
           </div>
           <div class="row">
              <div class="col-md-4">
                <img src="{{ asset('images/web-images/proPic.jpg') }}" alt="">
              </div>
           </div>
          </form>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="clearfix"></div>
<div class="information">
  <div class="container">
  {!!$get_dashboard_cms?$get_dashboard_cms->seo_description:''!!}
  </div>
</div>
@endsection
@push('js')
<script>
    $(".city_id").select2();
$(".city_id").change( function(){
 var id = $(this).val();
 if(id==null){

 }
 else{
  $.ajax({
    url: "{{ route('web.subarea') }}",
   type: 'POST',
   data : { data: id},
   success: function(response){
        if(response.status == 'success'){
          $(".sub_area_id").empty();
            html = '<option value="">Select Sub Area</option>';
            $(response.sub_areas).each( function (i,d){
                html += '<option value="'+ d.id +'">'+ d.name +'</option>';
            });
            $(".sub_area_id").html(html);
            $(".sub_area_id").select2();
        }else{
            toastr['error']("Something went wrong.");
        }
   },
   error: function(error){
       toastr['error']("Something went wrong.");
   }

});
 }
});
</script>
    
@endpush