
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content=@yield('description')>
<meta name="keywords" content="@yield('keywords')">
<title>@yield('title')</title>

<!-- Bootstrap -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700|Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/web-custom.css')}}">
<link rel="stylesheet" href="{{ asset('css/media.css')}}">
<link rel="stylesheet" href="{{ asset('css/jquery-pincode-autotab.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css') }}"> 
@stack('css')
@yield('css')
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]> v
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar-fixed-top">
  <div class="topBar">
    <div class="container-fluid">
    	<div class="col-md-6 col-sm-4 col-xs-12 topLeft">
    		<p><i class="fas fa-phone-square"></i> For Inquiries: <b>0306-2033366</b></p>
    	</div>
    	<div class="col-md-6 col-sm-8 col-xs-12 topRight">
    		<ul class="list-unstyled list-inline">
          @if(auth()->check() && auth()->user()->user_type == "client")
          <li><a href="javascript:void(0)">Welcome {{auth()->user()->name}}</a> /
            <form action="{{ route('web.login.logout') }}" method="POST">
              @csrf  
              <button type="submit" class="btn btnLogout">Logout<button></form>|
              </li>
              @elseif(auth()->check() && auth()->user()->user_type == "admin")
              <li><a href="{{route("home")}}">Dashboard</a></li>
          @else
          <li><a href="javascript:void(0)" onclick="login()">Login</a>  / <a href="javascript:void(0)" onclick="register()">Register</a> |</li>
          @endif
          <li><a href="{{route("web.contactus")}}">Contact</a></li>
          @if(auth()->check() && auth()->user()->user_type == "client")
          <li><a href="javascript:void(0)" onclick="wishlist()"><i class="{{Session::get("wishlist_count") == 0 ? "far" : "fas"}} fa-heart"></i></a> <span id="wishlistcount">{{Session::get("wishlist_count") == 0 ? 0 : Session::get("wishlist_count")}}</span></li>
          @else
          <li><a href="javascript:void(0)" onclick="login()"><i class="far fa-heart"></i></a></li>
          @endif
				<li><a href="https://web.facebook.com"><i class="fab fa-facebook"></i></a></li>
				<li><a href="https://www.youtube.com"><i class="fab fa-youtube"></i></a></li>
				<li><a href="https://twitter.com"><i class="fab fa-twitter"></i></a></li>
    		</ul>
    	</div>
    </div>
  </div>
  <div class="header">
  	<div class="container-fluid">
  		<div class="col-md-4 col-sm-3 col-xs-4 logo">
        <a href="{{route('web.home')}}"><img src="{{ asset("images/web-images/logo.png")}}" alt=""></a>
  		</div>
  		<div class="col-md-4 col-sm-4 col-xs-4  hidden-xs">
  			<div class="row">
				<div class="col-xs-12 col-md-12 headerSearch">
				  <div class="input-group">
          <input type="text" class="form-control" id="txtsearch" placeholder="Search for products...">
          <ul class="addproduct" style="display:none;">

          </ul>
					<div class="input-group-btn">(
					  <button class="btn" type="button" onclick="searchallproduct()">
						<span class="glyphicon glyphicon-search"></span>
					  </button>
					</div>
				  </div>
				</div>
			  </div>
  		</div>
  		<div class="col-md-2 col-sm-2 col-xs-4 headerBtns">
        @if(auth()->check() && auth()->user()->user_type == "client")
        <a href="#" class="btn btn-primary btnMain" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fas fa-map-marker-alt"></i>
          <span id="location_change">{{Session::get("city") == null ? "Lahore" : Session::get("city")["city_name"]}}</span>
          <span class="caret"></span> </a>
        @else
        <a href="#" class="btn btn-primary btnMain" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fas fa-map-marker-alt"></i>
          <span id="location_change">{{Session::get("city") == null ? "Lahore" : Session::get("city")["city_name"]}}</span>
          <span class="caret"></span> </a>
        <ul class="dropdown-menu">
         <input type="hidden" id="txthiddenlocation" value={{Session::get("city") == null ? "1" :Session::get("city")["city_id"] }}>
         <li><a href="javascript:void(0)" onclick="datachangelocationwise('Lahore')">Lahore</a></li>
         <li><a href="javascript:void(0)" onclick="datachangelocationwise('Multan')">Multan</a></li>
         <li><a href="javascript:void(0)" onclick="datachangelocationwise('Islamabad')">Islamabad</a></li>
         </ul>
        @endif
  		</div>
  		<div class="col-md-2 col-sm-3 col-xs-4 headerBtns">
  			<a href="#" class="btn btn-primary btnMain">Download App</a>
  		</div>
  	</div>
  </div>

   <!-- include navbar -->
   @include('web.home_page_partial_view.nav_bar')
   <!-- end include navbar -->
</div>

<div class="allContent">
    @yield('content')
    <!-- include navbar -->
   @include('web.home_page_partial_view.footer')
   <!-- end include navbar -->
    <footer>
      <div class="container">
        <p>Copyright © 2019 Installment.pk. All Rights Reserved <span>(Designed &amp; Developed by: <a href="https://www.creative-dots.com/"  style="    text-decoration: none;
          color: #1e9fbc;">Creative Dots</a>)</span> </p>
      </div>
    </footer> 
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="{{ asset('js/jquery-1.11.3.min.js')}}"></script> 
<script src="{{ asset('js/jquery-ui.js')}}"></script> 
<script src="{{ asset('js/owl.carousel.min.js')}}"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="{{ asset('js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
<script href="{{ asset('js/jquery-pincode-autotab.min.js')}}"></script>
<script href="{{ asset('js/select2.min.js')}}"></script>
<script src="{{ asset('js/custom.js')}}"></script>
<script>
$(document).ready(function(){

  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };
  @if(session()->has('status') && session()->has('message'))
      toastr['{{ session()->get('status') }}']("{{ session()->get('message') }}");
  @endif


  var count=0;
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $("#txtsearch").keyup(function(){
    var value =$("#txtsearch").val();
    var search = value.split('');
    if(search.length>=3){
      $.ajax({
        url: "{{route('websearchproduct')}}",
        data: {search:$("#txtsearch").val()},
        type: "POST",
        success: function (json) {
        if(json.status==true)
          {
            $(".addproduct").empty();
            var html ='';
            $(json.get_product).each( function (i,d){
              html += '<li onclick="productpage(\'' + d.product_slug + '\')">'+ d.product_name +' '+ d.info+' '+ d.info1+'</li>';
              });
              $(".addproduct").html(html);
              $(".addproduct").show();
          }
          else
           {
            toastr['error'](""+json.errMsg+""); 
          }
        }
      });
    }
    else{
      $(".addproduct").empty();
      $(".addproduct").hide();
    }
  });
  $('#txtsearch').parents().click(function(){
    $(".addproduct").hide();
  });

  $("#txtsearchformobile").keyup(function(){
    var value =$("#txtsearchformobile").val();
    var search = value.split('');
    if(search.length>=3){
      $.ajax({
        url: "{{route('websearchproduct')}}",
        data: {search:$("#txtsearchformobile").val()},
        type: "POST",
        success: function (json) {
        if(json.status==true)
          {
            $(".addproduct").empty();
            var html ='';
            $(json.get_product).each( function (i,d){
              html += '<li onclick="productpage(\'' + d.product_slug + '\')">'+ d.product_info + ' </li>';
              });
              $(".addproduct").html(html);
              $(".addproduct").show();
          }
          else
           {
            toastr['error'](""+json.errMsg+""); 
          }
        }
      });
    }
    else{
      $(".addproduct").empty();
      $(".addproduct").hide();
    }
  });
});
function showproductdetailpage(product_slug){
}
function searchallproduct(){
  var url = '{{route("web.search", ":search_slug")}}';
  url = url.replace(':search_slug',$("#txtsearch").val());
  window.location.href=url;
  $("#txtsearch").val('');
  $(".addproduct").empty();
  $(".addproduct").hide();
}

function searchallproductformobile(){
  var url = '{{route("web.search", ":search_slug")}}';
  url = url.replace(':search_slug',$("#txtsearchformobile").val());
  window.location.href=url;
  $("#txtsearchformobile").val('');
  $(".addproduct").empty();
  $(".addproduct").hide();
}
function productpage(product_slug){
  var url = '{{route("product", ":product_slug")}}';
  url = url.replace(':product_slug', product_slug);
  window.location.href=url;
}

function register(){
  var url = "{{Request::url()}}";
  $.ajax({
    url: "{{route('getroute')}}",
    data: {url : url,link:'loginregister'},
    type: "POST",
    success: function (json) {
    if(json.status==true)
      {
        var url = '{{route("signup")}}';
        window.location.href=url;
      }
      else
       {
        toastr['error'](""+json.errMsg+""); 
      }
    }
  });
}

function login(){
  var url = "{{Request::url()}}";
  $.ajax({
    url: "{{route('getroute')}}",
    data: {url : url,link:'loginregister'},
    type: "POST",
    success: function (json) {
    if(json.status==true)
      {
        var url = '{{route("web.login")}}';
        window.location.href=url;
      }
      else
       {
        toastr['error'](""+json.errMsg+""); 
      }
    }
  });
}

function dashboard_link(value){
  $.ajax({
    url: "{{route('getroute')}}",
    data: {url : value,link:'dasboard'},
    type: "POST",
    success: function (json) {
    if(json.status==true)
      {
        var url = '{{route("web.dashboard")}}';
        window.location.href=url;
      }
      else
       {
        toastr['error'](""+json.errMsg+""); 
      }
    }
  });
}

function wishlist(){
  var url = '{{route("web.wishlist")}}';
  window.location.href=url;
}

function product_wishlist(offere_id){
  $.ajax({
    url: "{{route('adddeletewishlist')}}",
    data: {offere_id:offere_id },
    type: "POST",
    success: function (json) {
    if(json.status==1)
      {
        $(".wishlist"+offere_id+"").removeClass('fas');
        $(".wishlist"+offere_id+"").addClass('far');
        $("#wishlistcount").html('');
        $("#wishlistcount").html(json.getwishlistcount);
        toastr['success'](""+json.errMsg+"");
      }
      else if(json.status==2){
        $(".wishlist"+offere_id+"").removeClass('far');
        $(".wishlist"+offere_id+"").addClass('fas');
        $("#wishlistcount").html('');
        $("#wishlistcount").html(json.getwishlistcount);
        toastr['success'](""+json.errMsg+"");
      }
      else
       {
        toastr['error'](""+json.errMsg+""); 
      }
    }
  });
}

</script>
@stack('js')
@yield('js')
</body>
</html>