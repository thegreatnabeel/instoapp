@extends('web.layout')
@section('title', 'Contact Us')

@section('keywords',$get_contact_page_seo_description?$get_contact_page_seo_description->meta_description:'')

@section('description',$get_contact_page_seo_description?$get_contact_page_seo_description->meta_keyword:'')
    
@section('content')

<div class="aboutInner">
  <div class="container">
    <div class="boxMain">
      <div class="boxHeader faqHeader">
      <h2>Contact Us</h2>
    </div>
      <div class="boxBody">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 contactLeft">
            <h4>Customer Care</h4>
            <div class="row">
              <div class="col-md-1 col-sm-2 col-xs-3">
                <i class="far fa-envelope"></i>
              </div>
              <div class="col-md-11 col-sm-10 col-xs-9">
                <address><strong>Email:</strong> <span>info@instalment.pk</span></address>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1 col-sm-2 col-xs-3">
                <i class="fas fa-phone-square"></i>
              </div>
              <div class="col-md-11 col-sm-10 col-xs-9">
                <address><strong>For Queries :</strong> <span>0306-2033366</span></address>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1 col-sm-2 col-xs-3">
                <i class="fas fa-headphones-alt"></i>
              </div>
              <div class="col-md-11 col-sm-10 col-xs-9">
                <address><strong>Support Timings:</strong> <span>9:00 AM - 7:00 PM ( Monday to Saturday)</span></address>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 contactRight">
            <h4>Email Us</h4>
            <form action="{{route('web.contactus.send')}}" method="post">
              @csrf
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                          <input type="text" name="name" class="form-control" value="{{ old('name') }}" required="" placeholder="Your Name">
                           @if($errors->has('name'))
                              <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                          @endif
                      </div>
                  </div>
             </div>
             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group {{ $errors->has('contact_number') ? 'has-error' : '' }}">
                        <input type="text" name="contact_number" class="form-control" value="{{ old('contact_number') }}" required="" placeholder="Contact Number">
                         @if($errors->has('contact_number'))
                            <span class="help-block text-danger">{{ $errors->first('contact_number')=="The contact number must be between 11 and 12 digits."?'Please enter number in 03xxxxxxxxx':$errors->first('contact_number') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}" required="" placeholder="Email">
                         @if($errors->has('email'))
                            <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
           </div>
           <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                    <textarea class="form-control" rows="5" name="message" placeholder="Enter Your Message Here" required="">{{old('message')}}</textarea>
                       @if($errors->has('message'))
                          <span class="help-block text-danger">{{ $errors->first('message') }}</span>
                      @endif
                  </div>
              </div>
         </div>
         <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="form-group {{ $errors->has('captch') ? 'has-error' : '' }}">
                    <input type="text" name="captch_confirmation" class="form-control" value="{{ old('captch_confirmation') }}" required="" placeholder="Enter the code in the box on right" >
                     @if($errors->has('captch'))
                        <span class="help-block text-danger">{{ $errors->first('captch') }}</span>
                    @endif
                </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
                <input type="text" name="captch" class="form-control" value="2135" readonly id="captchaVal" >
            </div>
      </div>
     </div>
              
              <div class="form-group">
                <button type="submit" class="btn btn-primary btnMain">Submit</button>
              </div>
            </form>
          </div>
        </div>				
      </div>
    </div>	
  </div>
</div>
<div class="clearfix"></div>
<div class="information">
  <div class="container">
  {!!$get_contact_page_seo_description?$get_contact_page_seo_description->seo_description:''!!}
  </div>
</div>
@endsection
