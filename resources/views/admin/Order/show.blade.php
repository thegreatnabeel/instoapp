@extends('adminlte::page')
@section('title', 'Vendor')
@section('content_header')
    <h1>Vendor</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
            <div class="box-body">
              <div class="row">
                  <div col-md-12>
                      <input type="hidden" id="txtvendorid" value="{{$vendor->id}}">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>First Name</th>
                                <td>{{$vendor->first_name}}</td>
                            </tr>
                            <tr>
                                <th>Last Name</th>
                                <td>{{$vendor->last_name}}</td>
                            </tr>
                            <tr>
                                <th>Company Name</th>
                                <td>{{$vendor->company_name}}</td>
                            </tr>
                            <tr>
                                <th>Designation</th>
                                <td>{{$vendor->designation}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$vendor->user->email}}</td>
                            </tr>
                            <tr>
                                <th>Secondary Phone Number</th>
                                <td>{{$vendor->secondary_phone_number}}</td>
                            </tr>
                            <tr>
                                <th>Primary Phone Number</th>
                                <td>{{$vendor->primary_phone_number}}</td>
                            </tr>
                            <tr>
                                <th>Business Address</th>
                                <td>{{$vendor->business_address}}</td>
                            </tr>
                            <tr>
                                <th>Cnic</th>
                                <td>{{$vendor->cnic}}</td>
                            </tr>
                            <tr>
                                <th>Base Address</th>
                                <td>{{$vendor->base_address}}</td>
                            </tr>
                            <tr>
                                <th>Picture Path</th>
                                <td><img src="{{$vendor->picture_path}}"></td>
                            </tr>
                            <tr>
                                <th>Cnic Picture Path</th>
                                <td>
                                    <img src="{{$vendor->cnic_front_img_path}}">
                                    @if($vendor->cnic_back_img_path!=null)
                                    <img src="{{$vendor->cnic_back_img_path}}">
                                    @endif
                                    </td>
                            </tr>
                            <tr>
                                <th>Verfication Status</th>
                                <td><select id="SelectVerifyStatusChange"><option value="Verfied" {{$vendor->verfication_status=="Verfied"?'selected':''}}>Verfied</option><option value="Unverfied" {{$vendor->verfication_status=="Unverfied"?'selected':''}}>Unverfied</option></select>&nbsp;<button type="button" class="btn btn-primary btn-xs" id="btnVerificationStatusChange">Change Status</button></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td><select id="SelectStatusChange"><option value="true" {{$vendor->status==1?'selected':''}}>Active</option><option value="false" {{$vendor->status==0?'selected':''}}>Deactive</option></select>&nbsp;<button type="button" class="btn btn-primary btn-xs" id="btnStatusChange">Change Status</button></td>
                            </tr>
                        </tbody>
                    </table>
                  </div>
              </div>
      </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
   $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    $("#btnStatusChange").click(function (){
        $.ajax({
                url: "{{ route('changestatus') }}",
               type: 'POST',
               data : { id : $("#txtvendorid").val() ,status: $("#SelectStatusChange").val()},
               success: function(response){
                    if(response.status == 'success'){
                        toastr['success']("Vendor Status Change Successfully.");
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
         });

         $("#btnVerificationStatusChange").click(function (){
        $.ajax({
                url: "{{ route('changevarification') }}",
               type: 'POST',
               data : { id : $("#txtvendorid").val() ,verfication_status: $("#SelectVerifyStatusChange").val()},
               success: function(response){
                    if(response.status == 'success'){
                        toastr['success']("Vendor Verfication Status Change Successfully.");
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
         });
</script>
@endpush

