@extends('adminlte::page')
@section('title', 'Orders')

@section('content_header')
    <h1>Orders</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="clearfix"></div>
            <table id="orders" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Customer</th>
                        <th>Seller</th>
                        <th>Product</th>
                        <th>Total Instalments</th>
                        <th>Down Payment</th>
                        <th>Amount Per Month</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Place Order</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($orders as $order)
                        <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->customer->first_name.' '.$order->customer->last_name}}</td>
                        <td>{{$order->offeredetail->offeremaster->vendor->first_name.' '.$order->offeredetail->offeremaster->vendor->last_name}}</td>    
                        <td>{{$order->offeredetail->offeremaster->product->product_name}}</td>
                        <td>{{number_format($order->offeredetail->month)??''}}</td>
                        <td>{{number_format($order->offeredetail->Advance)??''}}</td>
                        <td>{{number_format($order->offeredetail->installment)??''}}</td>
                        <td>
                            <select onchange="selectstatus('{{$order->id}}')" id="selectstatus{{$order->id}}">
                                <option value="pending" {{$order->status=="pending"?'selected':''}}>Pending</option>
                                <option value="approved" {{$order->status=="approved"?'selected':''}}>Approved</option>
                                <option value="completed" {{$order->status=="completed"?'selected':''}}>Completed</option>
                            </select>
                        </td>
                        <td>{{$order->order_date??''}}</td>
                        <td>{{$order->place_order??''}}</td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
        $(function () {
            $('#orders').DataTable();
        });
        function selectstatus(id){
            $.ajax({
                url: "{{route('changeorderstatus')}}",
                data: {id : id, value:$("#selectstatus"+id+"").val()},
                type: "POST",
                success: function (json) {
                if(json.status==true)
                  {
                    toastr['success']("Status Successfully Changed");
                  }
                  else
                   {
                    toastr['error']("Order Not Found"); 
                  }
                }
              });
        }
    </script>
@endpush
