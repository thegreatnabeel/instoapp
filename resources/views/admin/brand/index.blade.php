@extends('adminlte::page')
@section('title', 'Brands')

@section('content_header')
    <h1>Brand</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.brand.create') }}" class="btn btn-primary btnSubmit">Add New Brand</a>
            </div>
            <div class="clearfix"></div>
            <table id="airlines" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($brands as $brand)
                    <tr>
                        <td>{{ $brand->id }}</td>
                        <td>{{ $brand->brand_name }}</td>
                        <td>{{ $brand->brand_slug }}</td>
                        <td>{{ $brand->brand_description }}</td>
                        <td>
                            <a href="{{ route('admin.brand.edit', $brand->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.brand.destroy', $brand->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#airlines').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 4},
                 { "width": "60%", "targets": 3 }
                 ],
            });
        });
    </script>
@endpush
