@extends('adminlte::page')
@section('title', 'Add Brand')
@section('content_header')
    <h1>Add New Brand</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>

@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('css/code.css') }}"> 
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">   
@endpush
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.brand.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('brand_name') ? 'has-error' : '' }}">
                            <label>Brand Name <span>*</span></label>
                            <input type="text" name="brand_name" class="form-control" value="{{ old('brand_name') }}">
                            @if($errors->has('brand_name'))
                                <span class="help-block text-danger">{{ $errors->first('brand_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('brand_slug') ? 'has-error' : '' }}">
                            <label>Brand Slug <span>*</span></label>
                            <input type="text" name="brand_slug" class="form-control" value="{{ old('brand_slug') }}">
                            @if($errors->has('brand_slug'))
                                <span class="help-block text-danger">{{ $errors->first('brand_slug') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                         <label for="title">Logo <span>*</span></label>
                           <div class="input-group image-preview">
                             <span class="input-group-btn">
                              <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                              <span class="glyphicon glyphicon-remove"></span> Clear
                              </button>
                              <div class="btn btn-default image-preview-input">
                               <span class="glyphicon glyphicon-folder-open"></span>
                               <span class="image-preview-input-title">Browse</span>
                               <input type="file" class="form-control" accept="image/png, image/jpeg, image/gif, image/jpg" name="logo" required=""/>
                            </div>
                             </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('brand_description') ? 'has-error' : '' }}">
                            <label>Brand Description </label>
                            <input type="text" name="brand_description" class="form-control" value="{{ old('brand_description') }}">
                            @if($errors->has('brand_description'))
                                <span class="help-block text-danger">{{ $errors->first('brand_description') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('seo_description') ? 'has-error' : '' }}">
                              <label>Seo Description </label>
                              <textarea class="textarea form-control" id="seo_description" name="seo_description" placeholder="Seo Description"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('seo_description') }}</textarea>
                               @if($errors->has('seo_description'))
                                  <span class="help-block text-danger">{{ $errors->first('seo_description') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                              <label>Meta Description </label>
                              <textarea class="textarea form-control" id="meta_description" name="meta_description" placeholder="Meta Description"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('meta_description') }}</textarea>
                               @if($errors->has('meta_description'))
                                  <span class="help-block text-danger">{{ $errors->first('meta_description') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('meta_keyword') ? 'has-error' : '' }}">
                              <label>Meta Keywords </label>
                              <textarea class="textarea form-control" id="meta_keyword" name="meta_keyword" placeholder="Meta Keywords"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('meta_keyword') }}</textarea>
                               @if($errors->has('meta_keyword'))
                                  <span class="help-block text-danger">{{ $errors->first('meta_keyword') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create</button>
            </div>
        </form>
    </div>
@endsection

@push('js')
<script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script src="{{ asset('js/image-uploader.min.js') }}"></script>
<script type="text/javascript">
$(function () {
    CKEDITOR.replace('seo_description',{
        allowedContent:true
    });
  })
  $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      }, 
       function () {
         $('.image-preview').popover('hide');
      }
    );    
  });

  $(function() {
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      // Clear event
      $('.image-preview-clear').click(function(){
          $('.image-preview').attr("data-content","").popover('hide');
          $('.image-preview-filename').val("");
          $('.image-preview-clear').hide();
          $('.image-preview-input input:file').val("");
          $(".image-preview-input-title").text("Browse"); 
      }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
          var img = $('<img/>', {
              id: 'dynamic',
              width:250,
              height:200
          });      
          var file = this.files[0];
          var reader = new FileReader();
          // Set preview image into the popover data-content
          reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
          reader.readAsDataURL(file);
      });  
  });
</script>
@endpush

