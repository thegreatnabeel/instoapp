@extends('adminlte::page')
@section('title', 'Categories')

@section('content_header')
    <h1>Categories</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.categories.create') }}" class="btn btn-primary btnSubmit">Add New category</a>
            </div>
            <div class="clearfix"></div>
            <table id="categories" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Category Name</th>
                    <th>Category Slug</th>
                    <th>Category Description</th>
                    <th>Show Nav</th>
                    <th>Show Home Page</th>
                    <th>Nav Order</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->catagory_name }}</td>
                        <td>{{ $category->catagory_slug??"--" }}</td>
                        <td>{{ $category->catagory_description??"--" }}</td>
                        <td>{{ $category->show_nav==0?'false':'true' }}</td>
                        <td>{{ $category->show_home_page==0?'false':'true' }}</td>
                        <td>{{ $category->nav_order }}</td>
                        <td>
                            <a href="{{ route('admin.categories.edit', $category->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.categories.destroy', $category->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#categories').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 7}
                 ],
            });
        });
    </script>
@endpush
