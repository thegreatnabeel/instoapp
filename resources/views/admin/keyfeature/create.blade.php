@extends('adminlte::page')
@section('title', 'Add Key Feature')
@section('content_header')
    <h1>Add New Key Feature</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>

@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.keyfeature.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('sub_catagory_id') ? 'has-error' : '' }}">
                            <label>Sub Category Name <span>*</span></label>
                             <select class="form-control sub_catagory_id" name="sub_catagory_id" >
                            <option value="">Select Sub Category</option>
                            @foreach($subcategories as $subcategory)
                            <option value="{{$subcategory->id}}" {{ ( old('sub_catagory_id') == $subcategory->id) ? 'selected' : '' }}>{{$subcategory->sub_catagory_name}}</option>
                            @endforeach
                            </select>
                             @if($errors->has('sub_catagory_id'))
                                <span class="help-block text-danger">{{ $errors->first('sub_catagory_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Title <span>*</span></label>
                            <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                             @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('show_in_filter') ? 'has-error' : '' }}">
                            <label>Show Nav </label>
                            <input type="checkbox" name="show_in_filter" value="true" {{ ( old('show_in_filter') == true) ? 'checked' : '' }}>
                             @if($errors->has('show_in_filter'))
                                <span class="help-block text-danger">{{ $errors->first('show_in_filter') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create</button>
            </div>
        </form>
    </div>
@endsection

@push('js')

<script type="text/javascript">
    $(".sub_catagory_id").select2();
</script>
@endpush

