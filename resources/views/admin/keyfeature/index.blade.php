@extends('adminlte::page')
@section('title', 'Key Feature')

@section('content_header')
    <h1>Key Features</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.keyfeature.create') }}" class="btn btn-primary btnSubmit">Add New Key Feature</a>
            </div>
            <div class="clearfix"></div>
            <table id="keyfeatures" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Sub Category Name</th>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($keyfeatures as $keyfeature)
                    <tr>
                        <td>{{ $keyfeature->id }}</td>
                        <td>{{ $keyfeature->subcatagory?$keyfeature->subcatagory->sub_catagory_name:"--" }}</td>
                        <td>{{ $keyfeature->title }}</td>
                        <td>
                            <a href="{{ route('admin.keyfeature.edit', $keyfeature->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.keyfeature.destroy', $keyfeature->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#keyfeatures').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 3}
                 ],
            });
        });
    </script>
@endpush