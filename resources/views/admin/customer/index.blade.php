@extends('adminlte::page')
@section('title', 'Customers')

@section('content_header')
    <h1>Customers</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="clearfix"></div>
            <table id="customers" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>First Name</th>
                    <th>Email</th>
                    <th>Primary Number</th>
                    <th>Secondary Number</th>
                    <th>Base Address</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($customers as $customer)
                    <tr>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->first_name.''.$customer->last_name }}</td>
                        <td>{{ $customer->user->email }}</td>
                        <td>{{ $customer->primary_phone_number }}</td>
                        <td>{{ $customer->secondary_phone_number }}</td>
                        <td>{{ $customer->base_address }}</td>
                        <td>
                            <a href="{{ route('admin.customer.show', $customer->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="view"><i class="fa fa-eye"></i></button></a>
                            <form action="{{ route('admin.customer.destroy', $customer->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#customers').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 6}
                 ],
            });
        });
    </script>
@endpush
