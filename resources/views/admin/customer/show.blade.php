@extends('adminlte::page')
@section('title', 'Customer Detail')
@section('content_header')
    <h1>Customer Detail</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
            <div class="box-body">
              <div class="row">
                  <div col-md-12>
                      <input type="hidden" id="txtcustomerid" value="{{$customer->id}}">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>First Name</th>
                                <td>{{$customer->first_name}}</td>
                            </tr>
                            <tr>
                                <th>Last Name</th>
                                <td>{{$customer->last_name}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$customer->user->email}}</td>
                            </tr>
                            <tr>
                                <th>Secondary Phone Number</th>
                                <td>{{$customer->secondary_phone_number}}</td>
                            </tr>
                            <tr>
                                <th>Primary Phone Number</th>
                                <td>{{$customer->primary_phone_number}}</td>
                            </tr>
                            <tr>
                                <th>Base Address</th>
                                <td>{{$customer->base_address}}</td>
                            </tr>
                            <tr>
                                <th>Picture Path</th>
                                <td><img src="{{$customer->picture_path}}"></td>
                            </tr>
                            <tr>
                                <th>Verfication Status</th>
                                <td><select id="SelectVerifyStatusChange"><option value="Verfied" {{$customer->verfication_status=="Verfied"?'selected':''}}>Verfied</option><option value="Unverfied" {{$customer->verfication_status=="Unverfied"?'selected':''}}>Unverfied</option></select>&nbsp;<button type="button" class="btn btn-primary btn-xs" id="btnVerificationStatusChange">Change Status</button></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td><select id="SelectStatusChange"><option value="true" {{$customer->status==1?'selected':''}}>Active</option><option value="false" {{$customer->status==0?'selected':''}}>Deactive</option></select>&nbsp;<button type="button" class="btn btn-primary btn-xs" id="btnStatusChange">Change Status</button></td>
                            </tr>
                        </tbody>
                    </table>
                  </div>
              </div>
      </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
   $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    $("#btnStatusChange").click(function (){
        $.ajax({
                url: "{{ route('customerchangestatus') }}",
               type: 'POST',
               data : { id : $("#txtcustomerid").val() ,status: $("#SelectStatusChange").val()},
               success: function(response){
                    if(response.status == 'success'){
                        toastr['success']("Customer Status Change Successfully.");
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
         });

         $("#btnVerificationStatusChange").click(function (){
        $.ajax({
                url: "{{ route('cutomerchangevarification') }}",
               type: 'POST',
               data : { id : $("#txtcustomerid").val() ,verfication_status: $("#SelectVerifyStatusChange").val()},
               success: function(response){
                    if(response.status == 'success'){
                        toastr['success']("Customer Verfication Status Change Successfully.");
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
         });
</script>
@endpush

