@extends('adminlte::page')
@section('title', 'offers')

@section('content_header')
    <h1>Offers</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="clearfix"></div>
            <table id="offer" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Seller</th>
                        <th>Product</th>
                        <th>Total Offers</th>
                        <th>View Detail</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($get_offere_master as $offere_master)
                        <tr>
                        <td>{{$offere_master->id}}</td>
                        <td>{{$offere_master->vendor->first_name.' '.$offere_master->vendor->last_name}}</td>    
                        <td>{{$offere_master->product->product_name}}</td>
                        <td>{{number_format($offere_master->total_offere)??''}}</td>
                        <td>
                            <a href="{{ route('admin.offer.show', $offere_master->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="view"><i class="fa fa-eye"></i></button></a>
                        </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#offer').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 4}
                 ],
            });
        });
    </script>
@endpush
