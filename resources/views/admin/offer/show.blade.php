@extends('adminlte::page')
@section('title', 'Offers Details')

@section('content_header')
    <h1>Offers Details</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="clearfix"></div>
            <table id="offer" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Offer Valid Date</th>
                        <th>Advance</th>
                        <th>Installment Per Month</th>
                        <th>Month</th>
                        <th>Total Price</th>
                        <th>Cash Price</th>
                        <th>Featured</th>
                        <th>Post On Instalment</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($get_offere as $offere)
                        <tr>
                        <td>{{$offere->id}}</td>
                        <td>{{$offere->offere_valid_date}}</td>
                        <td>{{number_format($offere->Advance)??''}}</td>
                        <td>{{number_format($offere->installment)??''}}</td>
                        <td>{{number_format($offere->month)??''}}</td>
                        <td>{{number_format($offere->total_price)??''}}</td>
                        <td>{{number_format($offere->cash_price)??''}}</td>
                        <td>{{$offere->featured == true ? 'Yes' : 'No'}}</td>
                        <td>{{$offere->post_on_instalment == true ? 'Yes' : 'No'}}</td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#offer').DataTable();
        });
    </script>
@endpush
