@extends('adminlte::page')

@section('title', 'AdminLTE')


@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{$total_orders}}</h3>
          <p>Total Orders</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="javascript:void(0)" class="small-box-footer" onclick="order_link('total_orders')">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
         <h3>{{$total_new_orders}}</h3>

          <p>New Orders</p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-cart-outline"></i>
        </div>
        <a href="javascript:void(0)" class="small-box-footer" onclick="order_link('total_new_orders')">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$total_customers}}</h3>
          <p>Registered Customers</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="{{route('admin.customer.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$total_vendors}}</h3>
          <p>Registered Vendors</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-stalker"></i>
        </div>
        <a href="{{route('admin.vendor.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
</div>
<div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{$total_brands}}</h3>
          <p>Total Brands</p>
        </div>
        <div class="icon">
          <i class="fa fa-bold"></i>
        </div>
        <a href="{{route('admin.brand.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$total_products}}</h3>
          <p>Total Products</p>
        </div>
        <div class="icon">
          <i class="fa fa-barcode"></i>
        </div>
        <a href="{{route('admin.product.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$total_category}}</h3>

          <p>Total Categories</p>
        </div>
        <div class="icon">
          <i class="fa fa-braille"></i>
        </div>
        <a href="{{route('admin.categories.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$total_offeres}}</h3>

          <p>Total Offers</p>
        </div>
        <div class="icon">
          <i class="fa fa-barcode"></i>
        </div>
        <a href="{{route('admin.offer.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->
@endsection
@push('js')
    <script>
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      function order_link(value){
        $.ajax({
          url: "{{route('showorder')}}",
          data: {value : value},
          type: "POST",
          success: function (json) {
          if(json.status==true)
            {
              var url = '{{route("admin.Order.index")}}';
              window.location.href=url;
            }
            else
             {
              toastr['error'](""+json.errMsg+""); 
            }
          }
        });
      }
    </script>
@endpush