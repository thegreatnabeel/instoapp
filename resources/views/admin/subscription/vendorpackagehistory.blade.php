@extends('adminlte::page')
@section('title', 'Vendor Subscription History')

@section('content_header')
    <h1>Vendor Subscription History</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right">
            </div>
            <div class="clearfix"></div>
            <table id="packages" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Vedor Name</th>
                    <th>Package Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Expiry Date</th>
                    <th>Total Feature</th>
                    <th>Total Offers</th>
                    <th>Remain Feature</th>
                    <th>Remain Offers</th>
                    <th>Offer validity (Days)</th>
                </tr>
                </thead>
                <tbody>
                @foreach($packagehistories as $packagehistory)
                    <tr>
                        <td>{{ $packagehistory->id }}</td>
                        <td>{{ $packagehistory->vendor->first_name . ' '. $packagehistory->vendor->last_name }}</td>
                        <td>{{ $packagehistory->package->name }}</td>
                        <td>{{ $packagehistory->start_date }}</td>
                        <td>{{ $packagehistory->end_date }}</td>
                        <td>{{ $packagehistory->expiry_date }} </td>
                        <td>{{ $packagehistory->no_of_feature }}</td>
                        <td>{{ $packagehistory->no_of_post }}</td>
                        <td>{{ $packagehistory->remain_no_of_feature }}</td>
                        <td>{{ $packagehistory->remain_no_of_post }}</td>
                        <td>{{ $packagehistory->offere_validity }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#packages').DataTable({
                'bFilter': false,
                "columnDefs": [
                 { orderable: false, targets: 2}
                 ],
            });
        });
    </script>
@endpush
