@extends('adminlte::page')
@section('title', 'Edit Subscriptions')
@section('content_header')
    <h1>Edit Subscriptions</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.subscription.update', $vendorpackage->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('vendor_id') ? 'has-error' : '' }}">
                            <label>Vendor Name <span>*</span></label>
                            <select class="form-control vendor_id" name="vendor_id" disabled>
                                <option value="">Select Vendor</option>
                                @foreach($vendors as $vendor)
                                <option value="{{$vendor->id}}" {{ ( $vendorpackage->vendor_id == $vendor->id) ? 'selected' : '' }}>{{$vendor->first_name.' '.$vendor->last_name }}</option>
                                @endforeach
                                </select>
                             @if($errors->has('vendor_id'))
                                <span class="help-block text-danger">{{ $errors->first('vendor_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('package_id') ? 'has-error' : '' }}">
                            <label>Package Name <span>*</span></label>
                            <select class="form-control package_id" name="package_id" >
                                <option value="">Select Package</option>
                                @foreach($packages as $package)
                                <option value="{{$package->id}}" {{ ( $vendorpackage->package_id == $package->id) ? 'selected' : '' }}>{{$package->name}}</option>
                                @endforeach
                                </select>
                             @if($errors->has('package_id'))
                                <span class="help-block text-danger">{{ $errors->first('package_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('remian_no_of_feature') ? 'has-error' : '' }}">
                            <label>Remain Feature</label>
                            <input type="number" name="remian_no_of_feature" class="form-control remian_no_of_feature" value="{{ $getPackagehistoy ? $getPackagehistoy->remain_no_of_feature : 0 }}" min="0" readonly>
                            @if($errors->has('remian_no_of_feature'))
                                <span class="help-block text-danger">{{ $errors->first('remian_no_of_feature') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('remian_no_of_post') ? 'has-error' : '' }}">
                            <label>Remain Offers </label>
                            <input type="number" name="remian_no_of_post" class="form-control remian_no_of_post" value="{{ $getPackagehistoy ? $getPackagehistoy->remain_no_of_post : 0 }}" min="0" readonly>
                             @if($errors->has('remian_no_of_post'))
                                <span class="help-block text-danger">{{ $errors->first('remian_no_of_post') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('no_of_feature') ? 'has-error' : '' }}">
                            <label>Total Feature <span>*</span></label>
                            <input type="number" name="no_of_feature" class="form-control no_of_feature" value="{{ $vendorpackage->no_of_feature - ($getPackagehistoy ? $getPackagehistoy->remain_no_of_feature : 0) }}" min="1">
                            @if($errors->has('no_of_feature'))
                                <span class="help-block text-danger">{{ $errors->first('no_of_feature') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('no_of_post') ? 'has-error' : '' }}">
                            <label>Total Offers <span>*</span></label>
                            <input type="number" name="no_of_post" class="form-control no_of_post" value="{{ $vendorpackage->no_of_post - ($getPackagehistoy ? $getPackagehistoy->remain_no_of_post : 0 )}}" min="1">
                             @if($errors->has('no_of_post'))
                                <span class="help-block text-danger">{{ $errors->first('no_of_post') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('offere_validity') ? 'has-error' : '' }}">
                            <label>Offer valid <span>*</span></label>
                            <input type="number" name="offere_validity" class="form-control offere_validity" value="{{ $vendorpackage->offere_validity }}" min="1">
                             @if($errors->has('offere_validity'))
                                <span class="help-block text-danger">{{ $errors->first('offere_validity') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update</button>
            </div>
        </form>
    </div>
@endsection
@push('js')
<script type="text/javascript">
   $(".package_id").select2();
    $(".vendor_id").select2();
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $(".package_id").change( function(){
           var id = $(this).val();
           $.ajax({
                url: "{{ route('getpackagefeatureandpost') }}",
               type: 'POST',
               data : { Id: id},
               success: function(response){
                $(".no_of_feature").val(response.getPackage['no_of_feature']);
                $(".no_of_post").val(response.getPackage['no_of_post']);
                $(".offere_validity").val(response.getPackage['offere_validity']);
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
        });
</script>
@endpush

