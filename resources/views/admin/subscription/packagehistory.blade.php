@extends('adminlte::page')
@section('title', 'Subscription History')

@section('content_header')
    <h1>Subscription History</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right">
            </div>
            <div class="clearfix"></div>
            <table id="packages" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Vendor Name</th>
                    <th>Company Name</th>
                    <th>Contact Number</th>
                    <th>Total Expired Packages</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($packagehistories as $packagehistory)
                    <tr>
                        <td>{{ $packagehistory->vendor->first_name . ' '. $packagehistory->vendor->last_name }}</td>
                        <td>{{ $packagehistory->vendor->company_name ?? '' }}</td>
                        <td>{{ $packagehistory->vendor->primary_phone_number }}</td>
                        <td>{{ $packagehistory->total_package }}</td>
                        <td>
                            <a href="{{ route('vendorpackagehistory', $packagehistory->vendor->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="view"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#packages').DataTable({
                'bFilter': false,
                "columnDefs": [
                 { orderable: false, targets: 4}
                 ],
            });
        });
    </script>
@endpush
