@extends('adminlte::page')
@section('title', 'Subscription')

@section('content_header')
    <h1>Subscription</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.subscription.create') }}" class="btn btn-primary btnSubmit">Subscribe</a>
            </div>
            <div class="clearfix"></div>
            <table id="packages" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Vedor Name</th>
                    <th>Package Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Day Left Expired(days)</th>
                    <th>Total Feature</th>
                    <th>Total Offers</th>
                    <th>Offer validity (Days)</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($vendorpackages as $vendorpackage)
                    <tr>
                        <td>{{ $vendorpackage->id }}</td>
                        <td>{{ $vendorpackage->vendor?$vendorpackage->vendor->first_name:'' .' '. $vendorpackage->vendor?$vendorpackage->vendor->last_name:'' }}</td>
                        <td>{{ $vendorpackage->package->name }}</td>
                        <td>{{ $vendorpackage->start_date }}</td>
                        <td>{{ $vendorpackage->end_date }}</td>
                        <td> 
                            <!-- {{ $endDate = Carbon\Carbon::parse($vendorpackage->end_date) }} {{ $now = Carbon\Carbon::now() }}--> 
                            @if($endDate < $now)
                            <span style="color:red;">{{ $diff = $endDate->diffInDays($now) }} Days</span>
                            @else
                            <span style="color:green;">{{ $diff = $endDate->diffInDays($now) }} Days</span>
                            @endif
                             
                        </td>
                        <td>{{ $vendorpackage->no_of_feature }}</td>
                        <td>{{ $vendorpackage->no_of_post }}</td>
                        <td>{{ $vendorpackage->offere_validity }}</td>
                        <td>
                            <a href="{{ route('admin.subscription.edit', $vendorpackage->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.subscription.destroy', $vendorpackage->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#packages').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 9}
                 ],
            });
        });
    </script>
@endpush
