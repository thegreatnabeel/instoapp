@extends('adminlte::page')
@section('title', 'Edit Page')
@section('content_header')
    <h1>Edit Page</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.cms.update', $cm->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('page_title') ? 'has-error' : '' }}">
                            <label>Page Title <span>*</span></label>
                            <input type="text" name="page_title" class="form-control" value="{{ $cm->page_title }}">
                             @if($errors->has('page_title'))
                                <span class="help-block text-danger">{{ $errors->first('page_title') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                         <div class="form-group {{ $errors->has('page_content') ? 'has-error' : '' }}">
                            <label>Page Content <span>*</span></label>
                            <textarea class="textarea form-control" id="page_content" name="page_content" placeholder="Page Content"
                             style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="">{{$cm->page_content}}</textarea>
                             @if($errors->has('page_content'))
                                <span class="help-block text-danger">{{ $errors->first('page_content') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('seo_description') ? 'has-error' : '' }}">
                              <label>Seo Description </label>
                              <textarea class="textarea form-control" id="seo_description" name="seo_description" placeholder="Seo Description"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $cm->seo_description }}</textarea>
                               @if($errors->has('seo_description'))
                                  <span class="help-block text-danger">{{ $errors->first('seo_description') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>

                <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                              <label>Meta Description </label>
                              <textarea class="textarea form-control" id="meta_description" name="meta_description" placeholder="Meta Description"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$cm->meta_description}}</textarea>
                               @if($errors->has('meta_description'))
                                  <span class="help-block text-danger">{{ $errors->first('meta_description') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>
  
                  <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('meta_keyword') ? 'has-error' : '' }}">
                              <label>Meta Keywords </label>
                              <textarea class="textarea form-control" id="meta_keyword" name="meta_keyword" placeholder="Meta Keywords"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$cm->meta_keyword}}</textarea>
                               @if($errors->has('meta_keyword'))
                                  <span class="help-block text-danger">{{ $errors->first('meta_keyword') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>

            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update</button>
            </div>
        </form>
    </div>
@endsection
@push('js')
<script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(function () {
    CKEDITOR.replace('page_content',{
        allowedContent:true
    });
    CKEDITOR.replace('seo_description',{
        allowedContent:true
    });
  })
</script>
@endpush

