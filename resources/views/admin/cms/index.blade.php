@extends('adminlte::page')
@section('title', 'CMS')

@section('content_header')
    <h1>CMS</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.cms.create') }}" class="btn btn-primary btnSubmit">Add New Page</a>
            </div>
            <div class="clearfix"></div>
            <table id="cms" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Page Title</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cms as $cm)
                    <tr>
                        <td>{{ $cm->id }}</td>
                        <td>{{ $cm->page_title }}</td>
                        <td>
                            <a href="{{ route('admin.cms.edit', $cm->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.cms.destroy', $cm->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#cms').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 2}
                 ],
            });
        });
    </script>
@endpush
