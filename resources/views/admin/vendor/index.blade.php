@extends('adminlte::page')
@section('title', 'Vendors')

@section('content_header')
    <h1>Vendors</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="clearfix"></div>
            <table id="vendors" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>First Name</th>
                    <th>Business Name</th>
                    <th>Email</th>
                    <th>Primary Number</th>
                    <th>Secondary Number</th>
                    <th>Base Address</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($vendors as $vendor)
                    <tr>
                        <td>{{ $vendor->id }}</td>
                        <td>{{ $vendor->first_name.''.$vendor->last_name }}</td>
                        <td>{{ $vendor->company_name }}</td>
                        <td>{{ $vendor->user?$vendor->user->email:'' }}</td>
                        <td>{{ $vendor->primary_phone_number }}</td>
                        <td>{{ $vendor->secondary_phone_number }}</td>
                        <td>{{ $vendor->base_address }}</td>
                        <td>
                            <a href="{{ route('admin.vendor.show', $vendor->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-eye"></i></button></a>
                            <form action="{{ route('admin.vendor.destroy', $vendor->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#vendors').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 7}
                 ],
            });
        });
    </script>
@endpush
