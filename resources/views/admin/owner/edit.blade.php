@extends('adminlte::page')
@section('title', 'Edit Owner')
@section('content_header')
    <h1>Edit Owner</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.owner.update', $owner->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                          <label>First Name <span>*</span></label>
                          <input type="text" name="first_name" class="form-control" value="{{ $owner->first_name }}">
                           @if($errors->has('first_name'))
                              <span class="help-block text-danger">{{ $errors->first('first_name') }}</span>
                          @endif
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                          <label>Last Name <span>*</span></label>
                          <input type="text" name="last_name" class="form-control" value="{{ $owner->last_name }}">
                           @if($errors->has('last_name'))
                              <span class="help-block text-danger">{{ $errors->first('last_name') }}</span>
                          @endif
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group">
                       <label for="title">Profile Image <span>*</span></label>
                         <div class="input-group image-preview">
                           <span class="input-group-btn">
                            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                            <span class="glyphicon glyphicon-remove"></span> Clear
                            </button>
                            <div class="btn btn-default image-preview-input">
                             <span class="glyphicon glyphicon-folder-open"></span>
                             <span class="image-preview-input-title">Browse</span>
                             <input type="file" accept="image/png, image/jpeg, image/gif, image/jpg" name="picture_path" id="owner_image" {{ $owner->picture_path==null?'required':''}} />
                          </div>
                           </span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>Email <span>*</span></label>
                        <input type="text" name="email" class="form-control" value="{{ $owner->user->email }}" disabled="disabled">
                         @if($errors->has('email'))
                            <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label>Password </label>
                        <input type="password" name="password" class="form-control" value="">
                         @if($errors->has('password'))
                            <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <label>Confirm Password </label>
                        <input type="password" name="password_confirmation" class="form-control" value="">
                         @if($errors->has('password_confirmation'))
                            <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>
                </div>
            </div>
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group {{ $errors->has('primary_phone_number') ? 'has-error' : '' }}">
                          <label>Primary Phone Number <span>*</span></label>
                          <input type="text" name="primary_phone_number" class="form-control" value="{{ $owner->primary_phone_number }}">
                           @if($errors->has('primary_phone_number'))
                              <span class="help-block text-danger">{{ $errors->first('primary_phone_number') }}</span>
                          @endif
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group {{ $errors->has('secondary_phone_number') ? 'has-error' : '' }}">
                          <label>Secondary Phone Number </label>
                          <input type="text" name="secondary_phone_number" class="form-control" value="{{ $owner->secondary_phone_number==null?"":$owner->secondary_phone_number }}">
                           @if($errors->has('secondary_phone_number'))
                              <span class="help-block text-danger">{{ $errors->first('secondary_phone_number') }}</span>
                          @endif
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group {{ $errors->has('postal_address') ? 'has-error' : '' }}">
                          <label>Postal Address </label>
                          <input type="text" name="postal_address" class="form-control" value="{{ $owner->postal_address==null?"":$owner->postal_address }}">
                           @if($errors->has('postal_address'))
                              <span class="help-block text-danger">{{ $errors->first('postal_address') }}</span>
                          @endif
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <div class="form-group {{ $errors->has('base_address') ? 'has-error' : '' }}">
                          <label>Base Address <span>*</span></label>
                          <input type="text" name="base_address" class="form-control" value="{{ $owner->base_address }}">
                           @if($errors->has('base_address'))
                              <span class="help-block text-danger">{{ $errors->first('base_address') }}</span>
                          @endif
                      </div>
                  </div>
              </div>
          </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update</button>
            </div>
        </form>
    </div>
@endsection
@push('js')
<script type="text/javascript">
  $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      }, 
       function () {
         $('.image-preview').popover('hide');
      }
    );    
  });

  $(function() {
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      // Clear event
      $('.image-preview-clear').click(function(){
        $("#owner_image").attr('required','required');
          $('.image-preview').attr("data-content","").popover('hide');
          $('.image-preview-filename').val("");
          $('.image-preview-clear').hide();
          $('.image-preview-input input:file').val("");
          $(".image-preview-input-title").text("Browse"); 
      }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
          var img = $('<img/>', {
              id: 'dynamic',
              width:250,
              height:200
          }); 
          var file = this.files[0];
          var reader = new FileReader();
          // Set preview image into the popover data-content
          reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
          reader.readAsDataURL(file);
      });  
  });
  firstTime   = true;
  function firstTimeLoad()
  {
    if(firstTime)
    {
      firstTime = false;
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      var img   = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
              });  
      $(".image-preview-input-title").text("Change");
      $(".image-preview-clear").show();
      $(".image-preview-filename").val("Test");            
      img.attr('src', "{{$owner->picture_path}}");
      $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
    }
  }
  @if(!empty($owner->picture_path))
    firstTimeLoad();
  @endif
</script>
@endpush

