@extends('adminlte::page')
@section('title', 'Owner')

@section('content_header')
    <h1>Owner Profile</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body travelAgencyTableBtn">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.owner.create') }}" class="btn btn-primary btnSubmit">Add New Owner</a>
            </div>
            <div class="clearfix"></div>
            <table id="owner" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Primary Number</th>
                    <th>Secondary Number</th>
                    <th>Base Address</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($owners as $owner)
                    <tr>
                        <td>{{ $owner->id }}</td>
                        <td>{{ $owner->first_name }}</td>
                        <td>{{ $owner->last_name }}</td>
                        <td>{{ $owner->primary_phone_number }}</td>
                        <td>{{ $owner->secondary_phone_number }}</td>
                        <td>{{ $owner->base_address }}</td>
                        <td>
                            <a href="{{ route('admin.owner.edit', $owner->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.owner.destroy', $owner->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#owner').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 6}
                 ],
            });
        });
    </script>
@endpush
