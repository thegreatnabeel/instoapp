@extends('adminlte::page')
@section('title', 'Info Graphic')

@section('content_header')
    <h1>Info Graphic</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.InfoGraphic.create') }}" class="btn btn-primary btnSubmit">Add New Info Graphic</a>
            </div>
            <div class="clearfix"></div>
            <table id="InfoGraphic" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Image</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($InfoGraphics as $InfoGraphic)
                    <tr>
                        <td>{{ $InfoGraphic->id }}</td>
                        <td><img src="{{$InfoGraphic->info_graphic_path}}" style="height:30px;" /></td>
                        <td>
                            <a href="{{ route('admin.InfoGraphic.edit', $InfoGraphic->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.InfoGraphic.destroy', $InfoGraphic->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#InfoGraphic').DataTable({
                'bFilter': false,
                "columnDefs": [
                 { orderable: false, targets: 2},
                 { "width": "30%", "targets": 1 }
                 ],
            });
        });
    </script>
@endpush
