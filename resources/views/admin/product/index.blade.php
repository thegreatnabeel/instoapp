@extends('adminlte::page')
@section('title', 'Product')

@section('content_header')
    <h1>Products</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.product.create') }}" class="btn btn-primary btnSubmit">Add New Product</a>
            </div>
            <div class="clearfix"></div>
            <table id="products" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Product Name</th>
                    <th>Brand</th>
                    <th>Category Name</th>
                    <th>Sub Category Name</th>
                    <th>Product Slug</th>
                    <th>Total Offeres</th>
                    <th>Total Request</th>
                    <th>New Arrival</th>
                    <th>Hot Product</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>

                        <td>{{ $product->product_name }}</td>

                        <td>{{ $product->brand?$product->brand->brand_name:"--" }}</td>

                        <td>{{ $product->subcatagory?$product->subcatagory->category->catagory_name:"--" }}</td>

                        <td>{{ $product->subcatagory?$product->subcatagory->sub_catagory_name:"--" }}</td>
                        
                        <td>{{ $product->product_slug??"--" }}</td>

                        <td>{{ $product->get_offere_number??"0" }}</td>

                        <td>{{ $product->request_offere_number??"0" }}</td>
                        <td id ="newarrival{{$product->id}}">{{ $product->hot_product ? "New Arrival" : "" }}</td>
                        <td id ="hotproduct{{$product->id}}">{{ $product->hot_product ? "Hot Product" : "" }}</td>
                        <td>
                            <a href="{{ route('admin.product.edit', $product->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.product.destroy', $product->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                            <a href="#"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="New Arrival" onclick="ChangeNewArrival('{{$product->id}}')"><i class="fa fa-bars"></i></button></a>
                            <a href="#"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Hot Product" onclick="ChangeHotProduct('{{$product->id}}')"><i class="fa fa-barcode"></i></button></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#products').DataTable({
                'bFilter': true,
                "columnDefs": [
                 { orderable: false, targets: 10}
                 ],
            });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        function ChangeNewArrival(Id){
            $.ajax({
                url: "{{ route('changenewarrival') }}",
               type: 'POST',
               data : { Id: Id},
               success: function(response){
                    if(response.status == true){
                        $("#newarrival"+Id+"").html(response.value);
                        toastr['success']("Change Successfully.");
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
        }

        function ChangeHotProduct(Id){
            $.ajax({
                url: "{{ route('changehotproduct') }}",
               type: 'POST',
               data : { Id: Id},
               success: function(response){
                    if(response.status == true){
                        $("#hotproduct"+Id+"").html(response.value);
                        toastr['success']("Change Successfully.");
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
        }
    </script>
@endpush