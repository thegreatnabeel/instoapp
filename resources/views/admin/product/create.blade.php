@extends('adminlte::page')

@section('title', 'Add Product')
@section('content_header')
    <h1>Add New Product</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>

@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('css/code.css') }}"> 
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">   
@endpush
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.product.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="row">
                  <div class="col-md-3">
                        <div class="form-group {{ $errors->has('product_name') ? 'has-error' : '' }}">
                            <label>Product Name <span>*</span></label>
                            <input type="text" name="product_name" class="form-control" value="{{ old('product_name') }}" maxlength="15">
                             @if($errors->has('product_name'))
                                <span class="help-block text-danger">{{ $errors->first('product_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('info') ? 'has-error' : '' }}">
                            <label>Product Info <span>*</span></label>
                            <input type="text" name="info" class="form-control" value="{{ old('info') }}" maxlength="15">
                             @if($errors->has('info'))
                                <span class="help-block text-danger">{{ $errors->first('info') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('info1') ? 'has-error' : '' }}">
                            <label>Product Info1 <span>*</span></label>
                            <input type="text" name="info1" class="form-control" value="{{ old('info1') }}" maxlength="15">
                             @if($errors->has('info1'))
                                <span class="help-block text-danger">{{ $errors->first('info1') }}</span>
                            @endif
                        </div>
                    </div>

                    
                    <div class="col-md-3">
                        <div class="form-group">
                         <label for="title">Product Image *</label>
                           <div class="input-group image-preview">
                             <span class="input-group-btn">
                              <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                              <span class="glyphicon glyphicon-remove"></span> Clear
                              </button>
                              <div class="btn btn-default image-preview-input">
                               <span class="glyphicon glyphicon-folder-open"></span>
                               <span class="image-preview-input-title">Browse</span>
                               <input type="file" class="form-control" accept="image/png, image/jpeg, image/gif, image/jpg" name="product_image" required=""/>
                            </div>
                             </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                        <div class="form-group {{ $errors->has('product_slug') ? 'has-error' : '' }}">
                            <label>Product Slug <span>*</span></label>
                            <input type="text" name="product_slug" class="form-control" value="{{ old('product_slug') }}">
                             @if($errors->has('product_slug'))
                                <span class="help-block text-danger">{{ $errors->first('product_slug') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                        <div class="form-group {{ $errors->has('catagory_id') ? 'has-error' : '' }}">
                            <label>Category Name <span>*</span></label>
                             <select class="form-control catagory_id" name="catagory_id" >
                            <option value="">Select Category</option>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->catagory_name}}</option>
                            @endforeach
                            </select>
                             @if($errors->has('catagory_id'))
                                <span class="help-block text-danger">{{ $errors->first('catagory_id') }}</span>
                            @endif
                        </div>
                    </div>
                  <div class="col-md-3">
                        <div class="form-group {{ $errors->has('sub_catagory_id') ? 'has-error' : '' }}">
                            <label>Sub Category Name <span>*</span></label>
                             <select class="form-control sub_catagory_id" name="sub_catagory_id" >
                            </select>
                             @if($errors->has('sub_catagory_id'))
                                <span class="help-block text-danger">{{ $errors->first('sub_catagory_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('brand_id') ? 'has-error' : '' }}">
                            <label>Brand <span>*</span></label>
                             <select class="form-control brand_id" name="brand_id" >
                            <option value="">Select Brand</option>
                            @foreach($brands as $brand)
                            <option value="{{$brand->id}}" {{ ( old('brand_id') == $brand->id) ? 'selected' : '' }}>{{$brand->brand_name}}</option>
                            @endforeach
                            </select>
                             @if($errors->has('brand_id'))
                                <span class="help-block text-danger">{{ $errors->first('brand_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group {{ $errors->has('new_arrival') ? 'has-error' : '' }}">
                            <label>New Arrival </label>
                            <input type="checkbox" name="new_arrival" value="true" {{ ( old('new_arrival') == true) ? 'checked' : '' }}>
                             @if($errors->has('new_arrival'))
                                <span class="help-block text-danger">{{ $errors->first('new_arrival') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group {{ $errors->has('hot_product') ? 'has-error' : '' }}">
                            <label>Hot Product </label>
                            <input type="checkbox" name="hot_product" value="true" {{ ( old('hot_product') == true) ? 'checked' : '' }}>
                             @if($errors->has('hot_product'))
                                <span class="help-block text-danger">{{ $errors->first('hot_product') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                        <div class="form-group">
                         <label for="title">Gallery  Image</label>
                         <div class="input-images-1" style="padding-top: .5rem;"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12 meta_row nl-padding">
                  <div class="col-md-4 nl-padding">
                    <div class="form-group {{ $errors->has('meta_key') ? 'has-error' : '' }}">
                      <label >Feature Key</label>
                      <select name="meta_key[]" class="form-control meta_key select2">
                        <option>Select Key Feature</option>
                      </select>
                      @if($errors->has('meta_key'))
                        <span class="help-block text-danger">{{ $errors->first('meta_key') }}</span>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group {{ $errors->has('meta_value') ? 'has-error' : '' }}">
                      <label >Feature Value</label>
                      <input type="text" name="meta_value[]" class="form-control meta_value">
                       @if($errors->has('meta_value'))
                        <span class="help-block text-danger">{{ $errors->first('meta_value') }}</span>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-1 add_row">
                    <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                  </div>
                  <div class="clearfix"></div>
                </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group {{ $errors->has('product_description') ? 'has-error' : '' }}">
                            <label>Product Description <span>*</span></label>
                            <textarea class="textarea form-control" id="add_products" name="product_description" placeholder="Product Description" id="product_description"
                             style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="">{{old('product_description')}}</textarea>
                             @if($errors->has('product_description'))
                                <span class="help-block text-danger">{{ $errors->first('product_description') }}</span>
                            @endif
                        </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group {{ $errors->has('product_feature') ? 'has-error' : '' }}">
                            <label>Product Feature <span>*</span></label>
                            <textarea class="textarea form-control" name="product_feature" placeholder="Product Description" id="product_feature"
                             style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="">{{old('product_feature')}}</textarea>
                             @if($errors->has('product_feature'))
                                <span class="help-block text-danger">{{ $errors->first('product_feature') }}</span>
                            @endif
                        </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                         <div class="form-group {{ $errors->has('seo_description') ? 'has-error' : '' }}">
                            <label>Seo Description </label>
                            <textarea class="textarea form-control" id="seo_description" name="seo_description" placeholder="Seo Description"
                             style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('seo_description') }}</textarea>
                             @if($errors->has('seo_description'))
                                <span class="help-block text-danger">{{ $errors->first('seo_description') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                         <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                            <label>Meta Description </label>
                            <textarea class="textarea form-control" id="meta_description" name="meta_description" placeholder="Meta Description"
                             style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('meta_description') }}</textarea>
                             @if($errors->has('meta_description'))
                                <span class="help-block text-danger">{{ $errors->first('meta_description') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                         <div class="form-group {{ $errors->has('meta_keyword') ? 'has-error' : '' }}">
                            <label>Meta Keywords </label>
                            <textarea class="textarea form-control" id="meta_keyword" name="meta_keyword" placeholder="Meta Keywords"
                             style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('meta_keyword') }}</textarea>
                             @if($errors->has('meta_keyword'))
                                <span class="help-block text-danger">{{ $errors->first('meta_keyword') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create</button>
            </div>
        </form>
    </div>
@endsection

@push('js')
<script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script src="{{ asset('js/image-uploader.min.js') }}"></script>
<script>
  $(function () {
    
    CKEDITOR.replace('product_description',{
        allowedContent:true
    });
    CKEDITOR.replace('seo_description',{
        allowedContent:true
    });
  })
</script>
<script type="text/javascript">
 $('.input-images-1').imageUploader();
    $(".catagory_id").select2();
    $(".brand_id").select2();
    $(".meta_key").select2();

  $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      }, 
       function () {
         $('.image-preview').popover('hide');
      }
    );    
  });

  $(function() {
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      // Clear event
      $('.image-preview-clear').click(function(){
          $('.image-preview').attr("data-content","").popover('hide');
          $('.image-preview-filename').val("");
          $('.image-preview-clear').hide();
          $('.image-preview-input input:file').val("");
          $(".image-preview-input-title").text("Browse"); 
      }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
          var img = $('<img/>', {
              id: 'dynamic',
              width:250,
              height:200
          });      
          var file = this.files[0];
          var reader = new FileReader();
          // Set preview image into the popover data-content
          reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
          reader.readAsDataURL(file);
      });  
  });

  $(document).on('click', '.add_row', function(){
    $(".meta_key").select2("destroy");
    var node  = $(this).closest('.meta_row');
    var clone = $(node).clone();
    $(this).removeClass('add_row');
    $(this).addClass('remove_row');
    $(node).find('button').html('<i class="fa fa-minus"></i>');
    $(node).find('button').removeClass('btn-primary');
    $(node).find('button').addClass('btn-danger');
    $(clone).find('input').val('');
    $(node).after(clone);
    $(".meta_key").select2();
  });

  $(document).on('click', '.remove_row', function(){
    var node      = $(this).closest('.meta_row');
    $(node).remove();
    var last_node = $('.meta_row').last();
    $(this).removeClass('remove_row');
    $(this).addClass('add_row');
    $(last_node).find('button').html('<i class="fa fa-plus"></i>');
    $(last_node).find('button').removeClass('btn-danger');
    $(last_node).find('button').addClass('btn-primary');
  });

  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        $(".catagory_id").change( function(){
           var id = $(this).val();
           $.ajax({
                url: "{{ route('subcategorybyid') }}",
               type: 'POST',
               data : { data: id},
               success: function(response){
                    if(response.status == 'success'){
                      $(".sub_catagory_id").empty();
                        html = '<option value="">Select Sub Category</option>';
                        toastr['success']("Sub categories has been loaded.");
                        $(response.subcategory).each( function (i,d){
                            html += '<option value="'+ d.id +'">'+ d.sub_catagory_name +'</option>';
                        });
                        $(".sub_catagory_id").html(html);
                        $(".sub_catagory_id").select2();
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
        });

        $(".sub_catagory_id").change( function(){
           var id = $(this).val();
           $.ajax({
                url: "{{ route('keyfeaturebysubid') }}",
               type: 'POST',
               data : { data: id},
               success: function(response){
                    if(response.status == 'success'){
                      $(".meta_key").empty();
                        html = '<option value="">Select Key Feature</option>';
                        toastr['success']("Key Feature has been loaded.");
                        $(response.featurekey).each( function (i,d){
                            html += '<option value="'+ d.id +'">'+ d.title +'</option>';
                        });
                        $(".meta_key").html(html);
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
        });


</script>
@endpush

