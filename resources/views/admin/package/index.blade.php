@extends('adminlte::page')
@section('title', 'Packages')

@section('content_header')
    <h1>Packages</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.package.create') }}" class="btn btn-primary btnSubmit">Add New Package</a>
            </div>
            <div class="clearfix"></div>
            <table id="packages" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Package Name</th>
                    <th>Total Feature</th>
                    <th>Total Offers</th>
                    <th>Offer validity (Days)</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($packages as $package)
                    <tr>
                        <td>{{ $package->id }}</td>
                        <td>{{ $package->name }}</td>
                        <td>{{ $package->no_of_feature }}</td>
                        <td>{{ $package->no_of_post }}</td>
                        <td>{{ $package->offere_validity }}</td>
                        <td>{{ $package->price }}</td>
                        <td>
                            <a href="{{ route('admin.package.edit', $package->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.package.destroy', $package->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#packages').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 6}
                 ],
            });
        });
    </script>
@endpush
