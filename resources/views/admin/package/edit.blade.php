@extends('adminlte::page')
@section('title', 'Edit Package')
@section('content_header')
    <h1>Edit Package</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.package.update', $package->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Package Name <span>*</span></label>
                            <input type="text" name="name" class="form-control" value="{{ $package->name}}">
                             @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group {{ $errors->has('no_of_featureno_of_feature') ? 'has-error' : '' }}">
                            <label>Total Feature <span>*</span></label>
                            <input type="number" name="no_of_feature" class="form-control" value="{{ $package->no_of_feature }}" min="1">
                            @if($errors->has('no_of_feature'))
                                <span class="help-block text-danger">{{ $errors->first('no_of_feature') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group {{ $errors->has('no_of_post') ? 'has-error' : '' }}">
                            <label>Total Offers <span>*</span></label>
                            <input type="number" name="no_of_post" class="form-control" value="{{ $package->no_of_post }}" min="1">
                             @if($errors->has('no_of_post'))
                                <span class="help-block text-danger">{{ $errors->first('no_of_post') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group {{ $errors->has('offere_validity') ? 'has-error' : '' }}">
                            <label>Offer valid <span>*</span></label>
                            <input type="number" name="offere_validity" class="form-control" value="{{ $package->offere_validity }}" min="1">
                             @if($errors->has('offere_validity'))
                                <span class="help-block text-danger">{{ $errors->first('offere_validity') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                            <label>Price <span>*</span></label>
                            <input type="number" name="price" class="form-control" value="{{ $package->price }}" min="0">
                             @if($errors->has('price'))
                                <span class="help-block text-danger">{{ $errors->first('price') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update</button>
            </div>
        </form>
    </div>
@endsection
@push('js')
<script type="text/javascript">
</script>
@endpush

