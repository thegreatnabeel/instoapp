@extends('adminlte::page')
@section('title', 'Edit Sub Category')
@section('content_header')
    <h1>Edit Sub Category</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.subcategories.update', $subcategory->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('catagory_id') ? 'has-error' : '' }}">
                            <label>Category Name <span>*</span></label>
                             <select class="form-control catagory_id" name="catagory_id" >
                            <option value="">Select Category</option>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}" {{ ( $category->id ==$subcategory->catagory_id ) ? 'selected' : '' }}>{{$category->catagory_name}}</option>
                            @endforeach
                            </select>
                             @if($errors->has('catagory_id'))
                                <span class="help-block text-danger">{{ $errors->first('catagory_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('sub_catagory_name') ? 'has-error' : '' }}">
                            <label>Sub Category Name <span>*</span></label>
                            <input type="text" name="sub_catagory_name" class="form-control" value="{{ $subcategory->sub_catagory_name }}">
                             @if($errors->has('sub_catagory_name'))
                                <span class="help-block text-danger">{{ $errors->first('sub_catagory_name') }}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('sub_catagory_slug') ? 'has-error' : '' }}">
                            <label>Sub Category Slug <span>*</span></label>
                            <input type="text" name="sub_catagory_slug" class="form-control" value="{{ $subcategory->sub_catagory_slug }}">
                            @if($errors->has('sub_catagory_slug'))
                                <span class="help-block text-danger">{{ $errors->first('sub_catagory_slug') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                         <label for="title">Sub Category Image</label>
                           <div class="input-group image-preview">
                             <span class="input-group-btn">
                              <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                              <span class="glyphicon glyphicon-remove"></span> Clear
                              </button>
                              <div class="btn btn-default image-preview-input">
                               <span class="glyphicon glyphicon-folder-open"></span>
                               <span class="image-preview-input-title">Browse</span>
                               <input type="file" class="form-control" accept="image/png, image/jpeg, image/gif, image/jpg" name="sub_catagory_img_path" />
                            </div>
                             </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group {{ $errors->has('show_home_page') ? 'has-error' : '' }}">
                            <label>Show Home </label>
                            <input type="checkbox" name="show_home_page" value="true" {{$subcategory->show_home_page==1?'checked':''}}>
                             @if($errors->has('show_home_page'))
                                <span class="help-block text-danger">{{ $errors->first('show_home_page') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                        <div class="form-group 
                        {{ $errors->has('sub_catagory_description') ? 'has-error' : '' }}">
                            <label>Sub Category Description </label>
                            <input type="text" name="sub_catagory_description" class="form-control" value="{{ $subcategory->sub_catagory_description }}">
                             @if($errors->has('sub_catagory_description'))
                                <span class="help-block text-danger">{{ $errors->first('sub_catagory_description') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('seo_description') ? 'has-error' : '' }}">
                              <label>Seo Description </label>
                              <textarea class="textarea form-control" id="seo_description" name="seo_description" placeholder="Seo Description"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $subcategory->seo_description }}</textarea>
                               @if($errors->has('seo_description'))
                                  <span class="help-block text-danger">{{ $errors->first('seo_description') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                              <label>Meta Description </label>
                              <textarea class="textarea form-control" id="meta_description" name="meta_description" placeholder="Meta Description"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $subcategory->meta_description }}</textarea>
                               @if($errors->has('meta_description'))
                                  <span class="help-block text-danger">{{ $errors->first('meta_description') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                           <div class="form-group {{ $errors->has('meta_keyword') ? 'has-error' : '' }}">
                              <label>Meta Keywords </label>
                              <textarea class="textarea form-control" id="meta_keyword" name="meta_keyword" placeholder="Meta Keywords"
                               style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $subcategory->meta_keyword }}</textarea>
                               @if($errors->has('meta_keyword'))
                                  <span class="help-block text-danger">{{ $errors->first('meta_keyword') }}</span>
                              @endif
                          </div>
                      </div>
                  </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update</button>
            </div>
        </form>
    </div>
@endsection
@push('js')
<script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script type="text/javascript">
$(function () {
    CKEDITOR.replace('seo_description',{
        allowedContent:true
    });
  });
   $(".catagory_id").select2();

  $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      }, 
       function () {
         $('.image-preview').popover('hide');
      }
    );    
  });

  $(function() {
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      // Clear event
      $('.image-preview-clear').click(function(){
          $('.image-preview').attr("data-content","").popover('hide');
          $('.image-preview-filename').val("");
          $('.image-preview-clear').hide();
          $('.image-preview-input input:file').val("");
          $(".image-preview-input-title").text("Browse"); 
      }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
          var img = $('<img/>', {
              id: 'dynamic',
              width:250,
              height:200
          }); 
          var file = this.files[0];
          var reader = new FileReader();
          // Set preview image into the popover data-content
          reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
          reader.readAsDataURL(file);
      });  
  });
  firstTime   = true;
  function firstTimeLoad()
  {
    if(firstTime)
    {
      firstTime = false;
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      var img   = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
              });  
      $(".image-preview-input-title").text("Change");
      $(".image-preview-clear").show();
      $(".image-preview-filename").val("Test");            
      img.attr('src', "{{$subcategory->sub_catagory_img_path}}");
      $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
    }
  }
  @if(!empty($subcategory->sub_catagory_img_path))
    firstTimeLoad();
  @endif
</script>
@endpush

