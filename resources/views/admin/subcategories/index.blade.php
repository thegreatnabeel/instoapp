@extends('adminlte::page')
@section('title', 'Sub Categories')

@section('content_header')
    <h1>Sub Categories</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.subcategories.create') }}" class="btn btn-primary btnSubmit">Add New Sub Category</a>
            </div>
            <div class="clearfix"></div>
            <table id="subcategories" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Category Name</th>
                    <th>Sub Category Name</th>
                    <th>Sub Category Slug</th>
                    <th>Sub Category Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($subcategories as $subcategory)
                    <tr>
                        <td>{{ $subcategory->id }}</td>
                        <td>{{ $subcategory->category?$subcategory->category->catagory_name:"--" }}</td>
                        <td>{{ $subcategory->sub_catagory_name }}</td>
                        <td>{{ $subcategory->sub_catagory_slug??"--" }}</td>
                        <td>{{ $subcategory->sub_catagory_description??"--" }}</td>
                        <td>
                            <a href="{{ route('admin.subcategories.edit', $subcategory->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.subcategories.destroy', $subcategory->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#subcategories').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 5}
                 ],
            });
        });
    </script>
@endpush