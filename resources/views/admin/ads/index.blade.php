@extends('adminlte::page')
@section('title', 'Ads')

@section('content_header')
    <h1>Ads</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body travelAgencyTableBtn">
            <div class="col-md-12 text-right btnSpace">
                <a href="{{ route('admin.ads.create') }}" class="btn btn-primary btnSubmit">Add New Ads</a>
            </div>
            <div class="clearfix"></div>
            <table id="ads" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Screen</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ads as $ad)
                    <tr>
                        <td>{{ $ad->id }}</td>
                        <td>{{ $ad->screen_id }}</td>
                        <td>{{ $ad->type }}</td>
                        <td>
                            <a href="{{ route('admin.ads.edit', $ad->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('admin.ads.destroy', $ad->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#ads').DataTable({
                "columnDefs": [
                 { orderable: false, targets: 3}
                 ],
            });
        });
    </script>
@endpush
