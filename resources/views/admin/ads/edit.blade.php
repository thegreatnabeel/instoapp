@extends('adminlte::page')
@section('title', 'Edit Ads')
@section('content_header')
    <h1>Edit Ads</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
        </div>
        <form action="{{ route('admin.ads.update', $ad->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('screen_id') ? 'has-error' : '' }}">
                            <label>Screen Number <span>*</span></label>
                            <select class="form-control screen_id" name="screen_id" >
                              <option value="" {{ ( $ad->screen_id == null) ? 'selected' : '' }} >Select Screen No#</option>

                              <option value="1" {{ ( $ad->screen_id == 1) ? 'selected' : '' }}>1</option>

                              <option value="2" {{ ( $ad->screen_id == 2) ? 'selected' : '' }}>2</option>

                              <option value="3" {{ ( $ad->screen_id == 3) ? 'selected' : '' }}>3</option>
                            </select>
                             @if($errors->has('screen_id'))
                                <span class="help-block text-danger">{{ $errors->first('screen_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                            <label>Type <span>*</span></label>
                            <select class="form-control type" name="type" >
                              <option value="" {{ ( $ad->type == null) ? 'selected' : '' }}>Select Type</option>
                              
                              <option value="product" {{ ( $ad->type == "product") ? 'selected' : '' }}>Product</option>
                              
                              <option value="sub_category" {{ ( $ad->type == "sub_category") ? 'selected' : '' }}>Sub Category</option>
                              
                              <option value="external_link" {{ ( $ad->type == "external_link") ? 'selected' : '' }}>External Link</option>
                            </select>
                            @if($errors->has('type'))
                                <span class="help-block text-danger">{{ $errors->first('type') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                      @if($ad->type == "external_link")
                      <div class="form-group {{ $errors->has('type_id') ? 'has-error' : '' }}">
                        <label>Type Nature <span>*</span></label>
                        <select class="form-control type_id" name="type_id" disabled>
                          @if($ad->type == "product")
                          <option value="" >Select Product</option>
                           @foreach($product as $pro){
                           <option value="{{$pro->id}}" {{ ( $pro->id == $ad->type_id) ? 'selected' : '' }} >{{$pro->product_name}}</option>
                           @endforeach
                           @elseif($ad->type == "sub_category")
                          <option value="" >Select Sub Category</option>
                           @foreach($subcatagory as $subcate){
                           <option value="{{$subcate->id}}" {{ ( $subcate->id == $ad->type_id) ? 'selected' : '' }} >{{$subcate->sub_catagory_name}}</option>
                           @endforeach
                           @else
                           
                           @endif
                        </select>
                        @if($errors->has('type_id'))
                            <span class="help-block text-danger">{{ $errors->first('type_id') }}</span>
                        @endif
                    </div>
                      @else
                      <div class="form-group {{ $errors->has('type_id') ? 'has-error' : '' }}">
                        <label>Type Nature <span>*</span></label>
                        <select class="form-control type_id" name="type_id">
                          @if($ad->type == "product")
                          <option value="" >Select Product</option>
                           @foreach($product as $pro){
                           <option value="{{$pro->id}}" {{ ( $pro->id == $ad->type_id) ? 'selected' : '' }} >{{$pro->product_name}}</option>
                           @endforeach
                           @elseif($ad->type == "sub_category")
                          <option value="" >Select Sub Category</option>
                           @foreach($subcatagory as $subcate){
                           <option value="{{$subcate->id}}" {{ ( $subcate->id == $ad->type_id) ? 'selected' : '' }} >{{$subcate->sub_catagory_name}}</option>
                           @endforeach
                           @else
                           
                           @endif
                        </select>
                        @if($errors->has('type_id'))
                            <span class="help-block text-danger">{{ $errors->first('type_id') }}</span>
                        @endif
                    </div>
                      @endif
                       
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                         <label for="title">Ad Image <span>*</span></label>
                           <div class="input-group image-preview">
                             <span class="input-group-btn">
                              <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                              <span class="glyphicon glyphicon-remove"></span> Clear
                              </button>
                              <div class="btn btn-default image-preview-input">
                               <span class="glyphicon glyphicon-folder-open"></span>
                               <span class="image-preview-input-title">Browse</span>
                               <input type="file" class="form-control" accept="image/png, image/jpeg, image/gif, image/jpg" name="ad_image" id="ad_image" {{$ad->ad_image_path==null?'required':''}} />
                            </div>
                             </span>
                            </div>
                        </div>
                    </div>
                </div>
                @if($ad->type == "external_link")
                <div class="row">
                  <div class="col-md-12" id="linkdiv">
                        <div class="form-group {{ $errors->has('external_link') ? 'has-error' : '' }}">
                            <label>External Link <span>*</span></label>
                            <input type="text" name="external_link" class="form-control" value="{{ $ad->external_link }}">
                             @if($errors->has('external_link'))
                                <span class="help-block text-danger">{{ $errors->first('external_link') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                  <div class="col-md-12 hidden" id="linkdiv">
                        <div class="form-group {{ $errors->has('external_link') ? 'has-error' : '' }}">
                            <label>External Link <span>*</span></label>
                            <input type="text" name="external_link" class="form-control" value="{{ old('external_link') }}">
                             @if($errors->has('external_link'))
                                <span class="help-block text-danger">{{ $errors->first('external_link') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update</button>
            </div>
        </form>
    </div>
@endsection
@push('js')
<script type="text/javascript">

    $(".screen_id").select2();
  $(".type").select2();

  var product = {!! str_replace("'", "\'", json_encode($product)) !!};
  var sub_catagory = {!! str_replace("'", "\'", json_encode($subcatagory)) !!};

  $('.type').on('select2:select', function (e) {
    var data = e.params.data;

    if(data.id=="product"){
      $(".type_id").empty();
      $(".type_id").removeAttr('disabled');
      $("#linkdiv").addClass('hidden');
      html='<option value="">Select Product</option>';
      $(product).each( function (i,d){
        html += '<option value="'+ d.id +'">'+ d.product_name +'</option>';
        });
       $(".type_id").html(html);
    }
    else if(data.id=="sub_category"){
      $(".type_id").empty();
      $(".type_id").removeAttr('disabled');
      $("#linkdiv").addClass('hidden');
      html='<option value="">Select Sub Category</option>';
      $(sub_catagory).each( function (i,d){
        html += '<option value="'+ d.id +'">'+ d.sub_catagory_name +'</option>';
        });
       $(".type_id").html(html);
    }
    else if(data.id=="external_link"){
      $(".type_id").empty();
      $(".type_id").attr('disabled','disabled');
      $("#linkdiv").removeClass('hidden');
    }
    else{
      $(".type_id").removeAttr('disabled');
      $("#linkdiv").addClass('hidden');
      $(".type_id").empty();
    }
});
$(".type_id").select2();

  $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
      function () {
         $('.image-preview').popover('show');
      }, 
       function () {
         $('.image-preview').popover('hide');
      }
    );    
  });

  $(function() {
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      // Clear event
      $('.image-preview-clear').click(function(){
        $("#ad_image").attr('required','required');
          $('.image-preview').attr("data-content","").popover('hide');
          $('.image-preview-filename').val("");
          $('.image-preview-clear').hide();
          $('.image-preview-input input:file').val("");
          $(".image-preview-input-title").text("Browse"); 
      }); 
      // Create the preview image
      $(".image-preview-input input:file").change(function (){     
          var img = $('<img/>', {
              id: 'dynamic',
              width:250,
              height:200
          }); 
          var file = this.files[0];
          var reader = new FileReader();
          // Set preview image into the popover data-content
          reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
          reader.readAsDataURL(file);
      });  
  });
  firstTime   = true;
  function firstTimeLoad()
  {
    if(firstTime)
    {
      firstTime = false;
      // Create the close button
      var closebtn = $('<button/>', {
          type:"button",
          text: 'x',
          id: 'close-preview',
          style: 'font-size: initial;',
      });
      closebtn.attr("class","close pull-right");
      // Set the popover default content
      $('.image-preview').popover({
          trigger:'manual',
          html:true,
          title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
          content: "There's no image",
          placement:'bottom'
      });
      var img   = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
              });  
      $(".image-preview-input-title").text("Change");
      $(".image-preview-clear").show();
      $(".image-preview-filename").val("Test");            
      img.attr('src', "{{$ad->ad_image_path}}");
      $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
    }
  }
  @if(!empty($ad->ad_image_path))
    firstTimeLoad();
  @endif
</script>
@endpush

